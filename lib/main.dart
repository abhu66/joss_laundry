import 'package:flutter/material.dart';
import 'package:joss_laundry/src/splash/splash_page.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizerUtil().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'JOSS Laundry ',
              theme: ThemeData.light(),
              home: SplashPage(),
            );
          },
        );
      },
    );
  }
}
