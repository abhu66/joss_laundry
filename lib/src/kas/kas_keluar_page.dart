import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/kas/bottom_sheet_kas_keluar.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:sizer/sizer.dart';

class KasKeluarPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<KasKeluarPage> implements KasContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  KasPresenter _kasPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;
  TextEditingController _jenisKas = TextEditingController();
  TextEditingController _jumlah = TextEditingController();
  TextEditingController _keterangan = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<KasKeluar> listKasKeluar = [];


  @override
  void initState() {
    super.initState();
    _kasPresenter = KasPresenter(this);
    _kasPresenter.loadSharedPrefs();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body : this.isLoading ? Center(child: CircularProgressIndicator(strokeWidth: 1)) : this.listKasKeluar.length == 0 ?
    Center(
      child : Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.info_outline_rounded,size: 50,),
          SizedBox(height: 20,),
          Text("Belum ada data kas keluar !"),
        ],
      ),
     )

    : Container(
    child: Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: listKasKeluar.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  KasKeluar kasKeluar = this.listKasKeluar[index];
                  return _buildListView(kasKeluar,index);
                },
              ),
            ),
          ),
        ]
    ),
    ),

     floatingActionButton: FloatingActionButton(
       onPressed: (){
         showBottomSheetTambahKas();
       },
       child: Icon(Icons.add),
       backgroundColor: Colors.blue,
     ),
    );
  }

  Widget _buildListView(KasKeluar kasKeluar, int index) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                (index+1).toString(),
                style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
              ),
              SizedBox(width: 16,),
              Text(
                "Transaksi : "+kasKeluar.cashOut,
                style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
              ),

              Spacer(),
              Text(
                "Rp.${kasKeluar.amount}",
                style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
              ),
            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              Container(
                height: SizerUtil.deviceType == DeviceType.Tablet ? 40 : 25,
                width: SizerUtil.deviceType == DeviceType.Tablet ? 140 : 100,
                decoration: BoxDecoration(
                    color: kasKeluar.approvalStatus.toUpperCase() == "APPROVED" ? Colors.green : kasKeluar.approvalStatus.toUpperCase() == "REJECTED" ? Colors.red : Colors.yellow,
                    borderRadius: BorderRadius.all(
                        Radius.circular(50)
                    )
                ),
                child: Center(
                  child: Text(kasKeluar.approvalStatus.toUpperCase(),
                    style: TextStyle(
                        color: kasKeluar.approvalStatus.toUpperCase() == "APPROVED" ? Colors.white : kasKeluar.approvalStatus.toUpperCase() == "REJECTED" ? Colors.red : Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 8.0.sp
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  showBottomSheetTambahKas({String title}) async{
    return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      isDismissible: true,
      backgroundColor: Colors.transparent,
      builder: (context){
        return Container(
          margin: EdgeInsets.only(top: 100),
        padding: EdgeInsets.only(top: 30.0),
         decoration: BoxDecoration(
           borderRadius: BorderRadius.all(Radius.circular(20)),
           border: Border.all(color: Colors.grey.withOpacity(0.6)),
           color: Colors.white
         ),
          child: TambahKasKeluar(),
        );
      }).then((value) {
        setState(() {
          _kasPresenter.loadSharedPrefs();
        });
    });
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    setState(() {
      this.listKasKeluar = responseKasKeluar.data;
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetListKasKeluarByOutletId(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    // TODO: implement onSuccessSetorKas
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
    // TODO: implement onSuccessListCashOutItem
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    // TODO: implement onSuccessCreateCashOut
  }
}
