// To parse this JSON data, do
//
//     final responseListKasMasuk = responseListKasMasukFromJson(jsonString);

import 'dart:convert';

ResponseListKasMasuk responseListKasMasukFromJson(String str) => ResponseListKasMasuk.fromJson(json.decode(str));

String responseListKasMasukToJson(ResponseListKasMasuk data) => json.encode(data.toJson());

class ResponseListKasMasuk {
  ResponseListKasMasuk({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<KasMasuk> data;

  factory ResponseListKasMasuk.fromJson(Map<String, dynamic> json) => ResponseListKasMasuk(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<KasMasuk>.from(json["data"].map((x) => KasMasuk.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class KasMasuk {
  KasMasuk({
    this.id,
    this.orderNumber,
    this.orderDate,
    this.total,
    this.paymentMethod,
  });

  int id;
  String orderNumber;
  DateTime orderDate;
  dynamic total;
  String paymentMethod;

  factory KasMasuk.fromJson(Map<String, dynamic> json) => KasMasuk(
    id: json["id"],
    orderNumber: json["order_number"],
    orderDate: DateTime.parse(json["order_date"]),
    total: json["total"],
    paymentMethod: json["payment_method"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "order_number": orderNumber,
    "order_date": orderDate.toIso8601String(),
    "total": total,
    "payment_method": paymentMethod,
  };
}
