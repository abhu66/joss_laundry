// To parse this JSON data, do
//
//     final responseListCashOutItem = responseListCashOutItemFromJson(jsonString);

import 'dart:convert';

ResponseListCashOutItem responseListCashOutItemFromJson(String str) => ResponseListCashOutItem.fromJson(json.decode(str));

String responseListCashOutItemToJson(ResponseListCashOutItem data) => json.encode(data.toJson());

class ResponseListCashOutItem {
  ResponseListCashOutItem({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<CashOutItem> data;

  factory ResponseListCashOutItem.fromJson(Map<String, dynamic> json) => ResponseListCashOutItem(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<CashOutItem>.from(json["data"].map((x) => CashOutItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class CashOutItem {
  CashOutItem({
    this.id,
    this.organizationId,
    this.name,
    this.description,
  });

  int id;
  int organizationId;
  String name;
  String description;

  factory CashOutItem.fromJson(Map<String, dynamic> json) => CashOutItem(
    id: json["id"],
    organizationId: json["organization_id"],
    name: json["name"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organization_id": organizationId,
    "name": name,
    "description": description,
  };
}
