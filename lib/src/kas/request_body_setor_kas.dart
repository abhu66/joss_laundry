// To parse this JSON data, do
//
//     final requestBodySetorKas = requestBodySetorKasFromJson(jsonString);

import 'dart:convert';

RequestBodySetorKas requestBodySetorKasFromJson(String str) => RequestBodySetorKas.fromJson(json.decode(str));

String requestBodySetorKasToJson(RequestBodySetorKas data) => json.encode(data.toJson());

class RequestBodySetorKas {
  RequestBodySetorKas({
    this.outletId,
    this.userId,
    this.amount,
    this.description,
    this.image,
    this.cashIn,
    this.cashOut,
  });

  int outletId;
  int userId;
  int amount;
  String description;
  String image;
  List<CashIn> cashIn;
  List<CashOut> cashOut;

  factory RequestBodySetorKas.fromJson(Map<String, dynamic> json) => RequestBodySetorKas(
    outletId: json["outlet_id"],
    userId: json["user_id"],
    amount: json["amount"],
    description: json["description"],
    image: json["image"],
    cashIn: List<CashIn>.from(json["cash_in"].map((x) => CashIn.fromJson(x))),
    cashOut: List<CashOut>.from(json["cash_out"].map((x) => CashOut.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "user_id": userId,
    "amount": amount,
    "description": description,
    "image": image,
    "cash_in": List<dynamic>.from(cashIn.map((x) => x.toJson())),
    "cash_out": List<dynamic>.from(cashOut.map((x) => x.toJson())),
  };
}

class CashIn {
  CashIn({
    this.id,
    this.name,
    this.amount,
  });

  String name;
  int amount;
  int id;

  factory CashIn.fromJson(Map<String, dynamic> json) => CashIn(
    id: json["id"],
    name: json["name"],
    amount: json["amount"],
  );

  Map<String, dynamic> toJson() => {
    "id":id,
    "name": name,
    "amount": amount,
  };

  @override
  String toString() {
    return 'CashIn{id: $id, name: $name, amount: $amount}';
  }
}


class CashOut {
  CashOut({
    this.idTemp,
    this.name,
    this.amount,
  });

  int idTemp;
  String name;
  int amount;

  factory CashOut.fromJson(Map<String, dynamic> json) => CashOut(
    idTemp: json["id_temp"],
    name: json["name"],
    amount: json["amount"],
  );

  Map<String, dynamic> toJson() => {
    "id_temp": idTemp,
    "name": name,
    "amount": amount,
  };
}
