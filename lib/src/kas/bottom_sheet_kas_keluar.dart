import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/request_body_create_cash_out.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:sizer/sizer.dart';

class TambahKasKeluar extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<TambahKasKeluar> implements KasContract{
  String _dropDownValue = "Pilih Kas Keluar";
  TextEditingController _jenisKasKeluarController = TextEditingController();
  TextEditingController _jumlahController = TextEditingController();
  TextEditingController _keteranganController = TextEditingController();
  TextEditingController _gasController = TextEditingController();
  String jenisKasKeluar = "";
  bool _isSelectedGender = false;
  bool _isSelectedTokenListrik = false;
  bool _isSelectedGasController = false;
  bool _isLoading = false;
  LoginResponse _loginResponse;
  KasPresenter kasPresenter;
  List<CashOutItem> listItem = [];
  bool isLoading;
  CashOutItem itemSelected;

  @override
  void initState(){
    kasPresenter = KasPresenter(this);
    kasPresenter.loadSharedPrefs();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Text("Tambah Kas Keluar", textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),),
            Divider(height: 20.0, color: Colors.grey,),
            Container(
              height: 50,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.grey.withOpacity(0.5)),
            ),
              child: TextFormField(
                readOnly: true,
                controller: _jenisKasKeluarController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  // icon: Icon(Icons.vpn_key,
                  //   color: Color(0xff6bceff),
                  // ),
                  hintText: 'Jenis Kas Keluar',
                  hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                ),
                onTap: () {
                  showBottomSheetKas();
                },
              ),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Colors.grey.withOpacity(0.5)),
              ),
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: _jumlahController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  // icon: Icon(Icons.vpn_key,
                  //   color: Color(0xff6bceff),
                  // ),
                  hintText: 'Jumlah',
                  hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  child: Container(
                    width: 150,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    ),
                    child: Center(
                        child: Text(
                          "Simpan",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w700),
                        )),
                  ),
                  onTap: () {
                    if (_jenisKasKeluarController.text.length == 0) {
                      showAalert(
                          "Perhatian !", "Item tidak boleh kosong !", context);
                    }
                    else if (_jumlahController.text.length == 0) {
                      showAalert("Perhatian !", "Jumlah tidak boleh kosong !",
                          context);
                    }
                    else {
                      RequestBodyCashOut body = RequestBodyCashOut(
                          amount: int.parse(this._jumlahController.value.text),
                          outletId: _loginResponse.data.outletId,
                          cashOutItemId: itemSelected.id,
                          userSubmissionId: _loginResponse.data.id
                      );
                      var bytesUsername = utf8.encode(
                          _loginResponse.data.email); // data being hashed
                      var digestUsername = sha1.convert(bytesUsername);
                      kasPresenter.doPostCreateCashOut(
                          digestUsername.toString(),
                          _loginResponse.data.accessSecret,
                          _loginResponse.data.outletId.toString(), body);
                    }
                  }
                ),
                InkWell(
                  child: Container(
                    width: 150,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    ),
                    child: Center(
                        child: Text(
                          "Batal",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w700),
                        )),
                  ),
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                ),
                ],
            )
          ],
        ),
      ),
    );
  }

  showBottomSheetKas() {
    return showModalBottomSheet<void>(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Container(
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(color: Colors.grey.withOpacity(0.5))
                  ),
                  child: ListView.builder(
                    itemCount: this.listItem.length,
                    itemBuilder: (BuildContext context, int index){
                      return InkWell(
                        onTap: () {
                          setState(() {
                            itemSelected = this.listItem[index];
                            _jenisKasKeluarController.text = itemSelected.name;
                            Navigator.pop(context);
                          });
                        },
                        child: Container(
                          height: 40,
                          margin: EdgeInsets.only(left: 16,top: 20,right: 16),
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          child: Text(this.listItem[index].name, style: TextStyle(color: this._isSelectedTokenListrik ? Colors.white : Colors.black),),
                          decoration: BoxDecoration(
                              color: this._isSelectedTokenListrik ? Colors.blue: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              border:
                              Border.all(color: Colors.grey.withOpacity(0.5))),
                        ),
                      );
                    },
                  ),
                );
              }
          );
        });
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    // TODO: implement onSuccessListKeluar
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    // TODO: implement onSuccessSetorKas
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      kasPresenter.doGetListCashOutItem(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
   setState(() {
     this.listItem = responseListCashOutItem.data;
   });
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    setState(() {
      showAalert(responseBodyCashOut.status,"message : "+responseBodyCashOut.message.replaceAll("Exception:", ""),context);
      Future.delayed(Duration(seconds: 1)).then((_){
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    });
  }
}
