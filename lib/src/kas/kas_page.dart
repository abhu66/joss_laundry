import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/kas/kas_keluar_page.dart';
import 'package:joss_laundry/src/kas/kas_masuk_page.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/kas/setoran_page.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:sizer/sizer.dart';

class KasPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<KasPage> with SingleTickerProviderStateMixin implements KasContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  TabController controller; //create controller untuk tabBar
  String paramDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
  LoginResponse _loginResponse;
  KasPresenter _kasPresenter;
  ResponseKasBalance responseKasBalance;
  bool isLoading = true;

  @override
  void initState(){
    controller    = new TabController(vsync: this, length: 2);
    _kasPresenter = KasPresenter(this);
    _kasPresenter.loadSharedPrefs();

    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Riwayat Kas".toUpperCase(),
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.6,
        actions: [
          IconButton(icon: Icon(Icons.date_range), onPressed: (){
            String _date = "";
            DatePicker.showDatePicker(context,
                showTitleActions: true,
                minTime: DateTime(1880, 1, 1),
                maxTime: DateTime(
                    DateTime.now().year,
                    DateTime.now().month,
                    DateTime.now().day), onChanged: (date) {
                  print('change $date');
                  print(DateTime.now().toString());
                }, onConfirm: (date) {
                  print('confirm $date');
                  _date = '$date'.substring(0, 10);
                  setState(() {
                    paramDate = _date;
                    _kasPresenter.loadSharedPrefs();
                  });

                }, currentTime: DateTime.now(), locale: LocaleType.en);
          }),
        ],
        iconTheme: IconThemeData(color: Colors.black),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: Container(
            child: Column(
              children: [
                Text("Kas saat ini"),
                Text("Rp. ${responseKasBalance?.data.toString()},-"),
                InkWell(
                  child: Text("Setorkan", style: TextStyle(color: Colors.blue),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) =>
                        SetoranPage(kasSaatIni: responseKasBalance?.data.toString(),currentDate: paramDate,)));
                  },
                ),
                ButtonBar(
                  children: [

                  ],
                ),
                new TabBar(
                  controller: controller,
                  labelColor: Colors.black,
                  tabs: <Widget>[
                    new Tab(text: "Kas Masuk"),
                    new Tab(text: "Kas Keluar",),
                  ],
                ),
              ],
            ),
          )
        )
      ),
      body: TabBarView(
        //controller untuk tab bar
        controller: controller,
        children: <Widget>[
          //kemudian panggil halaman sesuai tab yang sudah dibuat
          KasMasukPage(currentDate:this.paramDate,),
          KasKeluarPage()
        ],
      ),

    );
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
    setState(() {
      this.responseKasBalance = responseKasBalance;
    });
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetKasBalanceByOutledIdAndDate(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString(), paramDate);
    });
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    // TODO: implement onSuccessListKeluar
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    // TODO: implement onSuccessSetorKas
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
    // TODO: implement onSuccessListCashOutItem
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    // TODO: implement onSuccessCreateCashOut
  }
}
