// To parse this JSON data, do
//
//     final responseBodyCashJournal = responseBodyCashJournalFromJson(jsonString);

import 'dart:convert';

ResponseBodyCashJournal responseBodyCashJournalFromJson(String str) => ResponseBodyCashJournal.fromJson(json.decode(str));

String responseBodyCashJournalToJson(ResponseBodyCashJournal data) => json.encode(data.toJson());

class ResponseBodyCashJournal {
  ResponseBodyCashJournal({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory ResponseBodyCashJournal.fromJson(Map<String, dynamic> json) => ResponseBodyCashJournal(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.outletId,
    this.userId,
    this.date,
    this.amount,
    this.description,
    this.image,
    this.id,
    this.user,
    this.outlet,
  });

  int outletId;
  int userId;
  DateTime date;
  int amount;
  String description;
  String image;
  int id;
  String user;
  String outlet;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    outletId: json["outlet_id"],
    userId: json["user_id"],
    date: DateTime.parse(json["date"]),
    amount: json["amount"],
    description: json["description"],
    image: json["image"],
    id: json["id"],
    user: json["user"],
    outlet: json["outlet"],
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "user_id": userId,
    "date": date.toIso8601String(),
    "amount": amount,
    "description": description,
    "image": image,
    "id": id,
    "user": user,
    "outlet": outlet,
  };
}
