// To parse this JSON data, do
//
//     final responseKasKeluar = responseKasKeluarFromJson(jsonString);

import 'dart:convert';

ResponseKasKeluar responseKasKeluarFromJson(String str) => ResponseKasKeluar.fromJson(json.decode(str));

String responseKasKeluarToJson(ResponseKasKeluar data) => json.encode(data.toJson());

class ResponseKasKeluar {
  ResponseKasKeluar({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<KasKeluar> data;

  factory ResponseKasKeluar.fromJson(Map<String, dynamic> json) => ResponseKasKeluar(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<KasKeluar>.from(json["data"].map((x) => KasKeluar.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class KasKeluar {
  KasKeluar({
    this.id,
    this.outletId,
    this.cashOutItemId,
    this.amount,
    this.userSubmissionId,
    this.userApprovalId,
    this.approvalStatus,
    this.approvalDate,
    this.deposited,
    this.cashOut,
    this.cashier,
    this.staff,
  });

  int id;
  int outletId;
  int cashOutItemId;
  String amount;
  int userSubmissionId;
  int userApprovalId;
  String approvalStatus;
  DateTime approvalDate;
  bool deposited;
  String cashOut;
  String cashier;
  String staff;

  factory KasKeluar.fromJson(Map<String, dynamic> json) => KasKeluar(
    id: json["id"],
    outletId: json["outlet_id"],
    cashOutItemId: json["cash_out_item_id"],
    amount: json["amount"],
    userSubmissionId: json["user_submission_id"],
    userApprovalId: json["user_approval_id"] == null ? null : json["user_approval_id"],
    approvalStatus: json["approval_status"],
    approvalDate: json["approval_date"] == null ? null : DateTime.parse(json["approval_date"]),
    deposited: json["deposited"],
    cashOut: json["cash_out"],
    cashier: json["cashier"],
    staff: json["staff"] == null ? null : json["staff"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "outlet_id": outletId,
    "cash_out_item_id": cashOutItemId,
    "amount": amount,
    "user_submission_id": userSubmissionId,
    "user_approval_id": userApprovalId == null ? null : userApprovalId,
    "approval_status": approvalStatus,
    "approval_date": approvalDate == null ? null : approvalDate.toIso8601String(),
    "deposited": deposited,
    "cash_out": cashOut,
    "cashier": cashier,
    "staff": staff == null ? null : staff,
  };
}
