import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:sizer/sizer.dart';

class KasMasukPage extends StatefulWidget {

  final String currentDate;

  KasMasukPage({this.currentDate});
  @override
  _State createState() => _State();
}

class _State extends State<KasMasukPage> implements KasContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  KasPresenter _kasPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;
  List<KasMasuk> listKasMasuk = [];
  String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
  String paramDate = "";

  @override
  void initState() {
    super.initState();
    _kasPresenter = KasPresenter(this);
    _kasPresenter.loadSharedPrefs();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body : this.isLoading ? Center(child: CircularProgressIndicator(strokeWidth: 1))
        : this.listKasMasuk.length == 0 ?
       Center(
          child : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.info_outline_rounded,size: 50,),
              SizedBox(height: 20,),
              Text("Belum ada kas masuk untuk saat ini !"),
            ],
          ),
        )
           : Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: this.listKasMasuk.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      KasMasuk kasMasuk = this.listKasMasuk[index];
                      return _buildListView(kasMasuk, index);
                    },
                  ),
                ),
              ),
            ]
          ),
        ),
    );
  }

  Widget _buildListView(KasMasuk kasMasuk, int index) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Text(
            (index+1).toString(),
            style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
          ),
          SizedBox(width: 16,),
          Text(
            "Transaksi : "+kasMasuk.orderNumber,
            style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
          ),
          Spacer(),
          Text(
            "Rp.${kasMasuk.total}",
            style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
          ),
        ],
      ),
    );
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
    setState(() {
      this.listKasMasuk = responseListKasMasuk.data;
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this.paramDate == "" ? this.paramDate = formattedDate : this.paramDate = this.paramDate;
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetListKasMasukByOutletIdAndDate(digestUsername.toString(),
          response.data.accessSecret, response.data.outletId.toString(), widget.currentDate);
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    // TODO: implement onSuccessListKeluar
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    // TODO: implement onSuccessSetorKas
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
    // TODO: implement onSuccessListCashOutItem
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    // TODO: implement onSuccessCreateCashOut
  }
}

