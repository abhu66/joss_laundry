// To parse this JSON data, do
//
//     final requestBodyCashOut = requestBodyCashOutFromJson(jsonString);

import 'dart:convert';

RequestBodyCashOut requestBodyCashOutFromJson(String str) => RequestBodyCashOut.fromJson(json.decode(str));

String requestBodyCashOutToJson(RequestBodyCashOut data) => json.encode(data.toJson());

class RequestBodyCashOut {
  RequestBodyCashOut({
    this.outletId,
    this.cashOutItemId,
    this.amount,
    this.userSubmissionId,
  });

  int outletId;
  int cashOutItemId;
  int amount;
  int userSubmissionId;

  factory RequestBodyCashOut.fromJson(Map<String, dynamic> json) => RequestBodyCashOut(
    outletId: json["outlet_id"],
    cashOutItemId: json["cash_out_item_id"],
    amount: json["amount"],
    userSubmissionId: json["user_submission_id"],
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "cash_out_item_id": cashOutItemId,
    "amount": amount,
    "user_submission_id": userSubmissionId,
  };
}
