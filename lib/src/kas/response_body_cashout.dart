// To parse this JSON data, do
//
//     final responseBodyCashOut = responseBodyCashOutFromJson(jsonString);

import 'dart:convert';

ResponseBodyCashOut responseBodyCashOutFromJson(String str) => ResponseBodyCashOut.fromJson(json.decode(str));

String responseBodyCashOutToJson(ResponseBodyCashOut data) => json.encode(data.toJson());

class ResponseBodyCashOut {
  ResponseBodyCashOut({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory ResponseBodyCashOut.fromJson(Map<String, dynamic> json) => ResponseBodyCashOut(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.outletId,
    this.cashOutItemId,
    this.amount,
    this.userSubmissionId,
    this.approvalStatus,
    this.deposited,
    this.id,
    this.cashOut,
    this.cashier,
    this.staff,
  });

  int outletId;
  int cashOutItemId;
  int amount;
  int userSubmissionId;
  String approvalStatus;
  bool deposited;
  int id;
  String cashOut;
  String cashier;
  dynamic staff;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    outletId: json["outlet_id"],
    cashOutItemId: json["cash_out_item_id"],
    amount: json["amount"],
    userSubmissionId: json["user_submission_id"],
    approvalStatus: json["approval_status"],
    deposited: json["deposited"],
    id: json["id"],
    cashOut: json["cash_out"],
    cashier: json["cashier"],
    staff: json["staff"],
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "cash_out_item_id": cashOutItemId,
    "amount": amount,
    "user_submission_id": userSubmissionId,
    "approval_status": approvalStatus,
    "deposited": deposited,
    "id": id,
    "cash_out": cashOut,
    "cashier": cashier,
    "staff": staff,
  };
}
