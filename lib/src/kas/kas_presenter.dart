import 'package:dio/dio.dart';
import 'package:joss_laundry/src/kas/kas_api.dart';
import 'package:joss_laundry/src/kas/request_body_create_cash_out.dart';
import 'package:joss_laundry/src/kas/request_body_setor_kas.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';

abstract class KasContract{
  void onLoading(bool isLoading);
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk);
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar);
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal);
  void onSuccessSharedPref(LoginResponse response);
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem);
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance);
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut);
  void onFailure(String error);
}

class KasPresenter{
  KasApi api = KasApi();
  SharedPref sharedPref = SharedPref();
  KasContract contract;
  KasPresenter(this.contract);

  loadSharedPrefs() async{
    contract.onLoading(true);
    LoginResponse data = await sharedPref.getDataLogin();
    if(data != null){
      contract.onLoading(false);
      contract.onSuccessSharedPref(data);
    }
    else {
      contract.onLoading(false);
      contract.onFailure("Terjadi kesalahan !");
    }
  }

  doGetListKasMasukByOutletIdAndDate(String key,String secretKey,dynamic outletId,String date) async {
    contract.onLoading(true);
    api.getListKasMasukByOutletIdAndDate(key: key,secretKey: secretKey,outletId: outletId,date: date).then((ResponseListKasMasuk responseListKasMasuk){
      contract.onLoading(false);
      contract.onSuccessListKasMasuk(responseListKasMasuk);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetListKasKeluarByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListKasKeluarByOutletId(key: key,secretKey: secretKey,outletId: outletId).then((ResponseKasKeluar responseKasKeluar){
      contract.onLoading(false);
      contract.onSuccessListKeluar(responseKasKeluar);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetKasBalanceByOutledIdAndDate(String key,String secretKey,dynamic outletId,String date) async {
    contract.onLoading(true);
    api.getKasBalance(key: key,secretKey: secretKey,outletId: outletId,date: date).then((ResponseKasBalance responseKasBalance){
      contract.onLoading(false);
      contract.onSuccessKasBalanace(responseKasBalance);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doPostCreateCash(String key,String secretKey,dynamic outletId,RequestBodySetorKas requestBodySetorKas) async {
    contract.onLoading(true);
    api.setoranKas(key: key,secretKey: secretKey,outletId: outletId,requestBodySetorKas: requestBodySetorKas).then((ResponseBodyCashJournal responseBodyCashJournal){
      contract.onLoading(false);
      contract.onSuccessSetorKas(responseBodyCashJournal);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetListCashOutItem(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListKasKeluarItem(key: key,secretKey: secretKey,outletId: outletId).then((ResponseListCashOutItem responseListCashOutItem){
      contract.onLoading(false);
      contract.onSuccessListCashOutItem(responseListCashOutItem);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doPostCreateCashOut(String key,String secretKey,dynamic outletId,RequestBodyCashOut requestBodyCashOut) async {
    contract.onLoading(true);
    api.crateCashOut(key: key,secretKey: secretKey,outletId: outletId,requestBodyCashOut: requestBodyCashOut).then((ResponseBodyCashOut responseBodyCashOut){
      contract.onLoading(false);
      contract.onSuccessCreateCashOut(responseBodyCashOut);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

}