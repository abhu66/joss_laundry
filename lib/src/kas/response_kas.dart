// To parse this JSON data, do
//
//     final responseKasBalance = responseKasBalanceFromJson(jsonString);

import 'dart:convert';

ResponseKasBalance responseKasBalanceFromJson(String str) => ResponseKasBalance.fromJson(json.decode(str));

String responseKasBalanceToJson(ResponseKasBalance data) => json.encode(data.toJson());

class ResponseKasBalance {
  ResponseKasBalance({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  int data;

  factory ResponseKasBalance.fromJson(Map<String, dynamic> json) => ResponseKasBalance(
    status: json["status"],
    message: json["message"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}
