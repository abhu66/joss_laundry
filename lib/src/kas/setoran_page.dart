import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/request_body_setor_kas.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:sizer/sizer.dart';
import 'dart:developer';

class SetoranPage extends StatefulWidget {
  final String kasSaatIni;
  final String currentDate;
  SetoranPage({this.kasSaatIni,this.currentDate});
  @override
  _State createState() => _State();
}

class _State extends State<SetoranPage> implements KasContract {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  TextEditingController _tanggalSetoranController = new TextEditingController();
  TextEditingController _jumlahSetoranController = new TextEditingController();
  TextEditingController _keteranganController = new TextEditingController();

  KasPresenter _kasPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;
  List<KasMasuk> listKasMasuk = [];
  List<CashIn>   listCashIn   = [];

  List<KasKeluar> listKasKeluar = [];
  List<CashOut>   listCashOut   = [];
  String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
  String tanggalSetoran;
  String paramDate = "";
  File _image;
  File _imageFromCamera;
  final picker = ImagePicker();
  double totalKasMasuk = 0;
  double totalKasKeluar = 0;

 @override
  void initState() {
    super.initState();
    paramDate = widget.currentDate;
    _kasPresenter = KasPresenter(this);
    _kasPresenter.loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Setor Kas",
            style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
          ),
          backgroundColor: Colors.white,
          elevation: 0.6,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: this.isLoading
            ? Center(child: CircularProgressIndicator(strokeWidth: 1))
            : SingleChildScrollView(
              child: Column(children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 1)
                        ]),
                    margin: EdgeInsets.all(10.0),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 180 : 100,
                    child: Column(
                      children: [
                        Text("Kas Saat Ini"),
                        Text(
                          "Rp. ${widget.kasSaatIni.toString()},-",
                          style: TextStyle(fontSize: 30.0),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 1)
                        ]),
                    margin: EdgeInsets.all(10.0),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text("Total Kas Masuk"),
                            Spacer(),
                            Text("Rp ${this.totalKasMasuk},-"),
                          ],
                        ),
                        Row(
                          children: [
                            Text("Total Kas Keluar"),
                            Spacer(),
                            Text("Rp ${this.totalKasKeluar},-"),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 1)
                        ]),
                    margin: EdgeInsets.all(5.0),
                    padding: EdgeInsets.all(5),
                    child: ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                          ),
                          child: Text("Informasi Setoran", textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 20.0),),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(
                              top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                          ),
                          child: TextFormField(
                            readOnly: true,
                            controller: _tanggalSetoranController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // icon: Icon(Icons.vpn_key,
                              //   color: Color(0xff6bceff),
                              // ),
                              hintText: 'Tanggal Setoran',
                              hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                            ),
                            onTap: () {
                              String _date = "";
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(1880, 1, 1),
                                  maxTime: DateTime(
                                      DateTime.now().year,
                                      DateTime.now().month,
                                      DateTime.now().day), onChanged: (date) {
                                    print('change $date');
                                    print(DateTime.now().toString());
                                  }, onConfirm: (date) {
                                    print('confirm $date');
                                    _date = '$date'.substring(0, 10);
                                    _tanggalSetoranController.text = DateFormat("yyyy-MM-dd").format(DateTime.parse(_date)); //1994-03-10
                                    tanggalSetoran = date.toString();
                                    print('sss ' + _tanggalSetoranController.value.text);
                                  }, currentTime: DateTime.now(), locale: LocaleType.en);
                            },
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(
                              top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                          ),
                          child: TextFormField(
                            controller: _jumlahSetoranController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // icon: Icon(Icons.vpn_key,
                              //   color: Color(0xff6bceff),
                              // ),
                              hintText: 'Jumlah Setoran',
                              hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 150 : 100,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(
                              top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.grey),
                            color: Colors.white,
                          ),
                          child: TextFormField(
                            maxLengthEnforced: true,
                            expands: true,
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _keteranganController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // icon: Icon(Icons.vpn_key,
                              //   color: Color(0xff6bceff),
                              // ),
                              hintText: 'Keterangan',
                              hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(
                              top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                          ),
                          child: TextFormField(
                            readOnly: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Upload Bukti Setoran',
                              hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                            ),
                            onTap: (){
                              myAlert();
                            },
                          ),
                        ),
                        SizedBox(height: 20,),
                        Visibility(
                          visible: _image != null ? true : false,
                          child:Container(
                            margin: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(16.9)),
                            ),
                            child: _image != null ? Image.file(_image,width: 200,height: 200,) : Text(""),
                          ),
                        ),
                      ],
                    ),
                  ),
                SizedBox(height: 10.0,),
                Container(
                  height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                  width: MediaQuery.of(context).size.width/1.2,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff6bceff),
                          Color(0xFF00abff),
                        ],
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(50)
                      )
                  ),
                  child: Center(
                    child: this.isLoading ? CircularProgressIndicator(strokeWidth: 1,) :
                    InkWell(

                      child: Text('Tambah Setoran'.toUpperCase(),
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                      ),
                      onTap: () {
                        if (_tanggalSetoranController.text.length == 0) {
                          showAalert("Perhatian !", "Tanggal tidak boleh kosong !", context);
                        }
                        else if (_jumlahSetoranController.text.length == 0) {
                          showAalert("Perhatian !", "Jumlah setoran tidak boleh kosong !", context);
                        }
                        else if (_keteranganController.text.length == 0) {
                          showAalert("Perhatian !", "Keternagan tidak boleh kosong !", context);
                        }
                        else if (_image == null) {
                          showAalert("Perhatian !", "Harap Upload bukti!", context);
                        }
                        else {
                          RequestBodySetorKas body = RequestBodySetorKas(
                            image: base64Encode(_image.readAsBytesSync()).toString(),
                            description: _keteranganController.value.text,
                            outletId: _loginResponse.data.outletId,
                            userId: _loginResponse.data.id,
                            amount: int.parse(_jumlahSetoranController.value.text),
                            cashIn: this.listCashIn,
                            cashOut: this.listCashOut,
                          );
                          var bytesUsername = utf8.encode(_loginResponse.data
                              .email); // data being hashed
                          var digestUsername = sha1.convert(bytesUsername);
                          _kasPresenter.doPostCreateCash(
                              digestUsername.toString(),
                              _loginResponse.data.accessSecret, _loginResponse
                              .data.outletId.toString(), body);
                          print("Body : ${body.cashIn.toList().toString()}");
                        }
                      }
                    ),
                  ),
                ),
                SizedBox(height: 50.0,),
                ]),
            ));
  }


  // Future getImage(ImageSource media) async{
  //   var img = await ImagePicker.pickImage(source: media);
  //   setState(() {
  //     _image = img;
  //   });
  // }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera,imageQuality:50);
    if(mounted)
      setState(() {
        if (pickedFile != null) {
          _imageFromCamera = File(pickedFile.path);
          _image = _imageFromCamera;
        } else {
          print('No image selected.');
        }
      });
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery,imageQuality:50);
    if(mounted)
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
        } else {
          print('No image selected.');
        }
      });
  }

  void myAlert(){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text("Silahkan Pilih Media"),
            content: Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Column(
                children: [
                  FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      getImageFromGallery();
                    },
                    child: Row(
                      children: [
                        Icon(Icons.image),
                        Text("From Gallery"),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      getImage();
                    },
                    child: Row(
                      children: [
                        Icon(Icons.camera),
                        Text('From Camera'),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }
    );
  }

  //override
  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.replaceAll("Exception:", ""), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
    setState(() {
      this.listKasMasuk = responseListKasMasuk.data;
      if(this.listKasMasuk.length != 0 || this.listKasMasuk != null){
        this.listKasMasuk.map((e){
          this.totalKasMasuk += double.parse(e.total.toString());
          this.listCashIn.add(CashIn(id: e.id,amount: e.total,name: "Transaksi ${e.orderNumber}"));
        }).toList();
      }
      print("dd : "+this.totalKasMasuk.toString());
      var bytesUsername = utf8.encode(_loginResponse.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetListKasKeluarByOutletId(digestUsername.toString(), _loginResponse.data.accessSecret, _loginResponse.data.outletId.toString());

    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this.paramDate == "" ? this.paramDate = formattedDate : this.paramDate = this.paramDate;
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetListKasMasukByOutletIdAndDate(
          digestUsername.toString(),
          response.data.accessSecret,
          response.data.outletId.toString(),
          this.paramDate);
    });
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    setState(() {
      this.listKasKeluar = responseKasKeluar.data;
      if(this.listKasKeluar.length != 0 || this.listKasKeluar != null){
        this.listKasKeluar.map((e){
          if(e.approvalStatus.toUpperCase() != "WAITING"){
            this.totalKasKeluar += double.parse(e.amount);
            print("STATUS : ${e.approvalStatus}");
            this.listCashOut.add(CashOut(idTemp: e.id,amount: double.parse(e.amount).toInt(),name: e.cashOut));
          }

        }).toList();
      }
      print("dd : "+this.totalKasKeluar.toString());

    });
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    setState(() {
      showAalert(responseBodyCashJournal.status,"message : "+responseBodyCashJournal.message.replaceAll("Exception:", ""),context);
      Future.delayed(Duration(seconds: 2)).then((_){
        Navigator.of(context).pop();
      });
    });
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
    // TODO: implement onSuccessListCashOutItem
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    // TODO: implement onSuccessCreateCashOut
  }

}
