import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/kas/request_body_create_cash_out.dart';
import 'package:joss_laundry/src/kas/request_body_setor_kas.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';


class KasApi{
  final listKasMasukUrl    = "cash/in/";
  final listKasKeluarUrl   = "cash/out/";
  //final listKasKeluarUrl = "cash/out/item/";
  final kasBalanceUrl      = "cash/balance/";
  final setoranUrl         = "cash/journal";
  final listCashOutItemUrl = "cash/out/item/";
  final cashOutuRL         = "cash/out/approval";
  Config config  = new Config();

  Future<ResponseListKasMasuk> getListKasMasukByOutletIdAndDate({String key,String secretKey,dynamic outletId,String date}) async {
    return ResponseListKasMasuk.fromJson(
        await config.doGets(endpoint:config.baseUrl + listKasMasukUrl+outletId+"/"+date,
            key: key,
            secretKey: secretKey));
  }

  Future<ResponseKasBalance> getKasBalance({String key,String secretKey,dynamic outletId,String date}) async {
    return ResponseKasBalance.fromJson(
        await config.doGets(endpoint:config.baseUrl + kasBalanceUrl+outletId+"/"+date,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseKasKeluar> getListKasKeluarByOutletId({String key,String secretKey,dynamic outletId}) async {
    return ResponseKasKeluar.fromJson(
        await config.doGets(endpoint:config.baseUrl + listKasKeluarUrl+outletId,
            key: key,
            secretKey: secretKey));
  }

  Future<ResponseBodyCashJournal> setoranKas({String key,String secretKey,dynamic outletId,RequestBodySetorKas requestBodySetorKas}) async {
    return ResponseBodyCashJournal.fromJson(
        await config.doPosts(endpoint:config.baseUrl + setoranUrl,dataBody: requestBodySetorKas,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseListCashOutItem> getListKasKeluarItem({String key,String secretKey,dynamic outletId}) async {
    return ResponseListCashOutItem.fromJson(
        await config.doGets(endpoint:config.baseUrl + listCashOutItemUrl+outletId,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseBodyCashOut> crateCashOut({String key,String secretKey,dynamic outletId,RequestBodyCashOut requestBodyCashOut}) async {
    return ResponseBodyCashOut.fromJson(
        await config.doPosts(endpoint:config.baseUrl + cashOutuRL,dataBody: requestBodyCashOut,
            key: key,
            secretKey: secretKey));
  }
}