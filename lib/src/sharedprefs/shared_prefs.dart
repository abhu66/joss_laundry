
import 'dart:convert';

import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref{
  static read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key));
  }
  static readToken(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
  static save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  static saveBool(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
    print("Set bool  saved" +value.toString());
  }
  static saveToken(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("Device Token saved" +value.toString());
  }

  static remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static saveUkuranPrinter(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("Ukuran Printer" +value.toString());
  }

  static setGunakanBarcode(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
    print("Gunakan barcode " +value.toString());
  }

  static readGunakanBarcode(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  static setModeTeks(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
    print("Mode teks " +value.toString());
  }

  static readBool(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }



  static readModeTeks(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }


  Future<AuthResponse> getDataAuth() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('auth')) {
      AuthResponse auth = AuthResponse.fromJson(await SharedPref.read("auth"));
      return auth;
    }
    else {
      return null;
    }
  }

  Future<LoginResponse> getDataLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('login')) {
      LoginResponse loginResponse = LoginResponse.fromJson(await SharedPref.read("login"));
      return loginResponse;
    }
    else {
      return null;
    }

  }

  Future<String> getFCMToken() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('deviceToken')) {
      String deviceToken = await SharedPref.readToken("deviceToken");
      return deviceToken;
    }
    else {
      return null;
    }
  }
}