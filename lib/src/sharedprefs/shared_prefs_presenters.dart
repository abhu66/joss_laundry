
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';

abstract class SharedPrefContract{
 void onLoadingShared(bool isLoading);
 void onLoadingFcmToken(bool isLoading);
 void onSuccessAuth(AuthResponse authResponse);
 void onSuccessDataLogin(LoginResponse loginResponse);
 void onSuccessFCMToken(String token);
 void onFailure(String error);
}

class SharedPrefPresenter{
  SharedPref sharedPref = SharedPref();
  SharedPrefContract view;
  SharedPrefPresenter(this.view);

  loadAuth() async{
    view.onLoadingShared(true);
    sharedPref.getDataAuth().then((AuthResponse authResponse){
      view.onLoadingShared(false);
      view.onSuccessAuth(authResponse);
    }).catchError((error){
      view.onLoadingShared(false);
      view.onFailure(error.toString());
    });
  }

  loadFCMToken() async{
    view.onLoadingFcmToken(true);
    sharedPref.getFCMToken().then((String deviceToken){
      view.onLoadingFcmToken(false);
      view.onSuccessFCMToken(deviceToken);
    }).catchError((error){
      view.onLoadingFcmToken(false);
      view.onFailure(error.toString());
    });
  }

  loadDataLogin() async{
    view.onLoadingShared(true);
    sharedPref.getDataLogin().then((LoginResponse response){
      view.onLoadingShared(false);
      view.onSuccessDataLogin(response);
    }).catchError((error){
      view.onLoadingShared(false);
      view.onFailure(error.toString());
    });
  }
}