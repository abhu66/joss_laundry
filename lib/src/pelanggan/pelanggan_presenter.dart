import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_api.dart';
import 'package:joss_laundry/src/pelanggan/request_body_pelanggan.dart';
import 'package:joss_laundry/src/pelanggan/response_body_pelanggan_baru.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';

abstract class PelangganContract{
  void onPelangganLoading(bool isLoading);
  void onSuccessListPelanggan(ListPelangganResponse listPelangganResponse);
  void onSuccessCreatePelanggan(ResonseBodyPelangganBaru resonseBodyPelangganBaru);
  void onSuccessSharedPref(LoginResponse response);
  void onFailurePelanggan(String error);
}

class PelangganPresenter{
  PelangganApi api = PelangganApi();
  SharedPref sharedPref = SharedPref();
  PelangganContract contract;
  PelangganPresenter(this.contract);

  loadSharedPrefs() async{
    contract.onPelangganLoading(true);
    LoginResponse data = await sharedPref.getDataLogin();
    if(data != null){
      contract.onPelangganLoading(false);
      contract.onSuccessSharedPref(data);
    }
    else {
      contract.onPelangganLoading(false);
      contract.onFailurePelanggan("Terjadi kesalahan !");
    }
  }

  doGetListPelangganByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onPelangganLoading(true);
    api.getListPelanggan(key: key,secretKey: secretKey,outletId: outletId).then((ListPelangganResponse listPelangganResponse){
      contract.onPelangganLoading(false);
      contract.onSuccessListPelanggan(listPelangganResponse);
    }).catchError((error){
      contract.onPelangganLoading(false);
      contract.onFailurePelanggan(error.toString());
    });
  }

  doCreateNewCustomer({String key,String secretKey,RequestBodyPelanggan requestBodyPelanggan})async {
    contract.onPelangganLoading(true);
    api.addNewCustomer(key: key, secretKey: secretKey, requestBodyPelanggan:requestBodyPelanggan)
        .then((ResonseBodyPelangganBaru resonseBodyPelangganBaru) {
      contract.onPelangganLoading(false);
      contract.onSuccessCreatePelanggan(resonseBodyPelangganBaru);
    }).catchError((error) {
      contract.onPelangganLoading(false);
      contract.onFailurePelanggan(error.toString());
    });
  }
}