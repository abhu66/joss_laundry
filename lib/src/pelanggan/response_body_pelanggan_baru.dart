// To parse this JSON data, do
//
//     final resonseBodyPelangganBaru = resonseBodyPelangganBaruFromJson(jsonString);

import 'dart:convert';

ResonseBodyPelangganBaru resonseBodyPelangganBaruFromJson(String str) => ResonseBodyPelangganBaru.fromJson(json.decode(str));

String resonseBodyPelangganBaruToJson(ResonseBodyPelangganBaru data) => json.encode(data.toJson());

class ResonseBodyPelangganBaru {
  ResonseBodyPelangganBaru({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory ResonseBodyPelangganBaru.fromJson(Map<String, dynamic> json) => ResonseBodyPelangganBaru(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.outletId,
    this.name,
    this.phone,
    this.email,
    this.dateOfBirth,
    this.gender,
    this.address,
    this.id,
  });

  int outletId;
  String name;
  String phone;
  String email;
  DateTime dateOfBirth;
  String gender;
  String address;
  int id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    outletId: json["outlet_id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    dateOfBirth: DateTime.parse(json["date_of_birth"]),
    gender: json["gender"],
    address: json["address"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "name": name,
    "phone": phone,
    "email": email,
    "date_of_birth": "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
    "gender": gender,
    "address": address,
    "id": id,
  };
}
