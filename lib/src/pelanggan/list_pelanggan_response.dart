// To parse this JSON data, do
//
//     final listPelangganResponse = listPelangganResponseFromJson(jsonString);

import 'dart:convert';

ListPelangganResponse listPelangganResponseFromJson(String str) => ListPelangganResponse.fromJson(json.decode(str));

String listPelangganResponseToJson(ListPelangganResponse data) => json.encode(data.toJson());

class ListPelangganResponse {
  ListPelangganResponse({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<Customer> data;

  factory ListPelangganResponse.fromJson(Map<String, dynamic> json) => ListPelangganResponse(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<Customer>.from(json["data"].map((x) => Customer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Customer {
  Customer({
    this.id,
    this.outletId,
    this.name,
    this.phone,
    this.email,
    this.dateOfBirth,
    this.gender,
    this.address,
  });

  int id;
  int outletId;
  String name;
  String phone;
  String email;
  DateTime dateOfBirth;
  String gender;
  String address;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    id: json["id"],
    outletId: json["outlet_id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    dateOfBirth: DateTime.parse(json["date_of_birth"]),
    gender: json["gender"],
    address: json["address"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "outlet_id": outletId,
    "name": name,
    "phone": phone,
    "email": email,
    "date_of_birth": "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
    "gender": gender,
    "address": address,
  };
}
