import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/request_body_pelanggan.dart';
import 'package:joss_laundry/src/pelanggan/response_body_pelanggan_baru.dart';

class PelangganApi{
  final listPelangganUrl = "/customer/list/";
  final addNewPelangganUrl = "/customer";
  Config config  = new Config();

  Future<ListPelangganResponse> getListPelanggan({String key,String secretKey,dynamic outletId}) async {
    return ListPelangganResponse.fromJson(
        await config.doGets(endpoint:config.baseUrl + listPelangganUrl+outletId,
        key: key,
        secretKey: secretKey));
  }


  Future<ResonseBodyPelangganBaru> addNewCustomer({String key,String secretKey,RequestBodyPelanggan requestBodyPelanggan}) async {
    return ResonseBodyPelangganBaru.fromJson(
        await config.doPosts(endpoint:config.baseUrl + addNewPelangganUrl,dataBody: requestBodyPelanggan,
            key: key,
            secretKey: secretKey));
  }
}