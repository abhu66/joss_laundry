import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:sizer/sizer.dart';

class DetailPelangganPage extends StatefulWidget {
  final Customer pelanggan;
  DetailPelangganPage({this.pelanggan});

  @override
  _State createState() => _State();
}

class _State extends State<DetailPelangganPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Pelanggan",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.0, // untuk membuat garis dibawah appBar
        iconTheme: IconThemeData(
            color: Colors.black
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              // Lebar
              height: SizerUtil.deviceType == DeviceType.Tablet ? 200 : 200,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  SizedBox(height: 10.0,),
                  CircleAvatar(
                    backgroundColor: Colors.blue,
                    // backgroundImage: NetworkImage(""),
                    radius: 50,
                  ),
                  SizedBox(height: 20.0,),
                  Center(
                    child: Text(
                      "${widget.pelanggan.name}",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0.sp),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.black12, blurRadius: 1)
                    ]),
                margin: EdgeInsets.all(10.0),
                padding: EdgeInsets.all(5),
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      title: Text(
                          "Nama :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(widget.pelanggan.name),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text(
                          "No. Telepon :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(widget.pelanggan.phone),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text(
                          "Alamat :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(widget.pelanggan.address),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Jenis Kelamin :",
                          style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(widget.pelanggan.gender),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text(
                          "Email :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(widget.pelanggan.email),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Tanggal Lahir :",
                          style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(DateFormat("dd MMMM yyyy").format(widget.pelanggan.dateOfBirth)),
                    ),
                  ],
                )),
            SizedBox(
              height: 50.0,
            )
          ],
        ),
      ),
    );
  }
}
