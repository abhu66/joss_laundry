import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/pelanggan/detail_pelanggan_page.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:sizer/sizer.dart';

class PelangganSearch extends StatefulWidget {
  final List<Customer> listCustomerWidget;
  PelangganSearch({this.listCustomerWidget});

  @override
  _State createState() => _State();
}

class _State extends State<PelangganSearch> {
  TextEditingController controller = new TextEditingController();
  List<Customer> listCustomer = [];
  List<Customer> listSearch = [];
  bool firstSearch = true;
  String query ="";


  @override
  void initState(){
    super.initState();
    this.listCustomer = widget.listCustomerWidget;
    controller.addListener((){
      //menambahkan method yang akan dipanggil ketika object ada berubah
      if(controller.text.isEmpty){
        setState(() {
          firstSearch = true;
          query ="";
        });
      } else{
        //data tidak kosong
        setState(() {
          firstSearch = false;
          query = controller.text;
          print("Query : "+query);
        });
      }
    });
  }

  //membuat widget untuk hasil search
Widget _performSearch(){
    listSearch = new List<Customer>();
    for(int i=0; i<listCustomer.length;i++){
      var item = listCustomer[i];

      if(item.name.toLowerCase().contains(query.toLowerCase())){
        listSearch.add(item);
      }
    }
    return Expanded(
      child: SingleChildScrollView(child :  ListView.builder(
        shrinkWrap: true,
        itemCount: this.listSearch.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index){
          Customer customer = this.listSearch[index];
          return _buildListView(customer,index);
        },
      ),),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: 70,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){
                Navigator.pop(context);
              }),
              Flexible(child: Container(
                margin: EdgeInsets.all(10.0.sp),
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45.0,
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    color: Colors.white,
                    boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.search,
                      color: Color(0xff6bceff),
                    ),
                    hintText: 'Cari pelanggan',
                  ),
                ),
              ),
              ),
            ],
          ),
          !firstSearch ?  _performSearch() : Expanded(
            child: SingleChildScrollView(child :  ListView.builder(
              shrinkWrap: true,
              itemCount: this.listCustomer.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index){
                Customer customer = this.listCustomer[index];
                return _buildListView(customer,index);
              },
            ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView(Customer customer,int index){
    return  Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child : ListTile(
        leading: Icon(Icons.account_circle_rounded,size: 45,),
        title: Text(customer.name,style: TextStyle(color: Colors.black,fontSize: 12.0.sp),),
        subtitle:  Text("${customer.phone}",style: TextStyle(color: Colors.black,fontSize: 10.0.sp),),
        trailing: IconButton(icon: Icon(Icons.arrow_forward_ios_rounded),onPressed: (){},),
        onTap: (){
          // panggil detail pelanggan screen
          // paramnya customer
          Navigator.push(context,  MaterialPageRoute(builder: (context) => DetailPelangganPage(pelanggan: customer)));
        },
      ),
    );
  }
}
