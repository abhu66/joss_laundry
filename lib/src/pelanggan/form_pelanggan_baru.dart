import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_presenter.dart';
import 'package:joss_laundry/src/pelanggan/request_body_pelanggan.dart';
import 'package:joss_laundry/src/pelanggan/response_body_pelanggan_baru.dart';
import 'package:sizer/sizer.dart';

class FormNewCustomer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<FormNewCustomer> implements PelangganContract {
  final GlobalKey<FormState> _formNewCustomer = GlobalKey<FormState>();
  TextEditingController _namaController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _tanggalLahirController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _jenisKelaminController = new TextEditingController();
  TextEditingController _alamatController = new TextEditingController();
  String tanggalLahir;
  int _value = 1;
  String genders = "";
  bool _isSelectedGender = false;
  bool _isSelectedGenderPria = false;
  bool _isSelectedGenderWanita = false;
  bool _isLoading = false;
  LoginResponse _loginResponse;
  PelangganPresenter _pelangganPresenter;

  @override
  void initState(){
    super.initState();
    _pelangganPresenter = PelangganPresenter(this);
    _pelangganPresenter.loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Pelanggan Baru",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: this._isLoading ? Center(child : CircularProgressIndicator(strokeWidth: 1)) : Padding(
        child: Form(
          key: _formNewCustomer,
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                margin: EdgeInsets.only(top: 0),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  controller: _namaController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'Nama',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  controller: _phoneController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'No. hp',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'Email',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  readOnly: true,
                  controller: _tanggalLahirController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'Tanggal Lahir',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                  onTap: () {
                    String _date = "";
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(1880, 1, 1),
                        maxTime: DateTime(
                            DateTime.now().year,
                            DateTime.now().month,
                            DateTime.now().day), onChanged: (date) {
                      print('change $date');
                      print(DateTime.now().toString());
                    }, onConfirm: (date) {
                      print('confirm $date');
                      _date = '$date'.substring(0, 10);
                      _tanggalLahirController.text = DateFormat("yyyy-MM-dd").format(DateTime.parse(_date)); //1994-03-10
                      tanggalLahir = date.toString();
                      print('sss ' + _tanggalLahirController.value.text);
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  readOnly: true,
                  controller: _jenisKelaminController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'Jenis Kelamin',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                  onTap: () {
                    showBottomSheetGender();
                  },
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.2,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 200 : 100,
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                child: TextFormField(
                  maxLengthEnforced: true,
                  expands: true,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  controller: _alamatController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    // icon: Icon(Icons.vpn_key,
                    //   color: Color(0xff6bceff),
                    // ),
                    hintText: 'Alamat',
                    hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    ),
                    child: Center(
                        child: Text(
                      "SUBMIT",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    )),
                  ),
                  onTap: () {
                    if(_namaController.text.length == 0){
                      showAalert("Perhatian !","Nama tidak boleh kosong !",context);
                    }
                    else if(_phoneController.text.length == 0){
                      showAalert("Perhatian !","Nomor telpon tidak boleh kosong !",context);
                    }
                    else if(_emailController.text.length == 0){
                      showAalert("Perhatian !","Email tidak boleh kosong !",context);
                    }
                    else if(_tanggalLahirController.text.length == 0){
                      showAalert("Perhatian !","Tanggal lahir tidak boleh kosong !",context);
                    }
                    else if(this.genders.length == 0){
                      showAalert("Perhatian !","Jenis kelamin tidak boleh kosong !",context);
                    }
                    else if(_alamatController.text.length == 0){
                      showAalert("Perhatian !","Alamat tidak boleh kosong !",context);
                    }
                    else {
                      setState(() {
                        RequestBodyPelanggan newPelanggan = RequestBodyPelanggan(
                          outletId: this._loginResponse.data.outletId,
                          name: _namaController.text,
                          email: _emailController.text,
                          address: _alamatController.text,
                          dateOfBirth: _tanggalLahirController.text,
                          gender:  this._jenisKelaminController.text,
                          phone: _phoneController.text,
                        );
                        var bytesUsername = utf8.encode(this._loginResponse.data.email); // data being hashed
                        var digestUsername = sha1.convert(bytesUsername);
                        _pelangganPresenter.doCreateNewCustomer(
                            key: digestUsername.toString(),
                            secretKey: this._loginResponse.data.accessSecret,
                            requestBodyPelanggan: newPelanggan);
                      });
                    }
                  }),
            ],
          ),
        ),
        padding: EdgeInsets.all(16),
      ),
    );
  }

  Widget gender() {
    return Row(
      children: [
        Container(
          child: Text("Pria"),
        ),
        Container(
          child: Text("Wanita"),
        ),
      ],
    );
  }

  //datePicker
  showDatePickerTanggalKejadian(BuildContext context) {
    String _date = "";
    return TextFormField(
      readOnly: true,
      enableInteractiveSelection: false,
      controller: _tanggalLahirController,
      validator: (value) {
        return value.isEmpty || value == null ? "Pilih tanggal lahir !" : null;
      },
      onTap: () {
        DatePicker.showDatePicker(context,
            showTitleActions: true,
            minTime: DateTime(1880, 1, 1),
            maxTime: DateTime(
                DateTime.now().year, DateTime.now().month, DateTime.now().day),
            onChanged: (date) {
          print('change $date');
          print(DateTime.now().toString());
        }, onConfirm: (date) {
          print('confirm $date');
          _date = '$date'.substring(0, 10);
          _tanggalLahirController.text = _date;
             // DateFormat("dd/MM/yyyy").format(DateTime.parse(_date));
          //tanggalLahir = date.toString();
          //print('sss ' + _tanggalLahirController.value.text);
        }, currentTime: DateTime.now(), locale: LocaleType.en);
      },
      decoration: InputDecoration(
        labelText: "Tanggal Lahir",
        labelStyle: TextStyle(
            fontFamily: 'Poppins-Regular',
            fontSize: 14,
            fontWeight: FontWeight.w300),
        hintText: "Tanggal Lahir",
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
          //  when the TextFormField in unfocused
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
          //  when the TextFormField in focused
        ),
        border: OutlineInputBorder(),
      ),
    );
  }

  showBottomSheetGender() {
    return showModalBottomSheet<void>(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Container(
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(children: [
                        Container(
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.only(left: 10, bottom: 5, top: 5, right: 10),
                            child: Text("Pilih Jenis Kelamin",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                              textAlign: TextAlign.left,
                            ),
                        )
                      ]),
                      InkWell(
                        onTap: () {
                          setState(() {
                            this.genders = "Laki - laki";
                            this._isSelectedGenderPria = true;
                            this._isSelectedGenderWanita = false;
                            this._jenisKelaminController.text = this.genders;
                            Navigator.pop(context);
                          });
                        },
                        child: Container(
                          height: 40,
                          margin: EdgeInsets.only(left: 16,top: 20,right: 16),
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          child: Text("Pria", style: TextStyle(color: this._isSelectedGenderPria ? Colors.white : Colors.black),),
                          decoration: BoxDecoration(
                              color: this._isSelectedGenderPria ? Colors.blue: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              border:
                              Border.all(color: Colors.grey.withOpacity(0.5))),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            this.genders = "Perempuan";
                            this._isSelectedGenderPria = false;
                            this._isSelectedGenderWanita = true;
                            this._jenisKelaminController.text = this.genders;
                            Navigator.pop(context);
                          });
                        },
                        child: Container(
                          height: 40,
                          margin: EdgeInsets.only(
                            left: 16,
                            top: 20,
                            right: 16,
                          ),
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          child: Text("Wanita", style: TextStyle(color: this._isSelectedGenderWanita ? Colors.white : Colors.black),),
                          decoration: BoxDecoration(
                              color: this._isSelectedGenderWanita ? Colors.blue: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              border:
                              Border.all(color: Colors.grey.withOpacity(0.5))),
                        ),
                      ),
                    ],
                  ),
                );
              }
          );
        });
  }

  @override
  void onFailurePelanggan(String error) {
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onPelangganLoading(bool isLoading) {
    setState(() {
      this._isLoading = isLoading;
    });
  }

  @override
  void onSuccessCreatePelanggan(ResonseBodyPelangganBaru resonseBodyPelangganBaru) {
    setState(() {
      showAalertSuccess("Success!", resonseBodyPelangganBaru.message, context);
    });
  }

  @override
  void onSuccessListPelanggan(ListPelangganResponse listPelangganResponse) {
    // TODO: implement onSuccessListPelanggan
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      this._loginResponse = response;
      //_pelangganPresenter.doGetListPelangganByOutletId(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }
}
