// To parse this JSON data, do
//
//     final requestBodyPelanggan = requestBodyPelangganFromJson(jsonString);

import 'dart:convert';

RequestBodyPelanggan requestBodyPelangganFromJson(String str) => RequestBodyPelanggan.fromJson(json.decode(str));

String requestBodyPelangganToJson(RequestBodyPelanggan data) => json.encode(data.toJson());

class RequestBodyPelanggan {
  RequestBodyPelanggan({
    this.outletId,
    this.name,
    this.phone,
    this.email,
    this.dateOfBirth,
    this.gender,
    this.address,
  });

  int outletId;
  String name;
  String phone;
  String email;
  String dateOfBirth;
  String gender;
  String address;

  factory RequestBodyPelanggan.fromJson(Map<String, dynamic> json) => RequestBodyPelanggan(
    outletId: json["outlet_id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    dateOfBirth: json["date_of_birth"],
    gender: json["gender"],
    address: json["address"],
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "name": name,
    "phone": phone,
    "email": email,
    "date_of_birth": dateOfBirth,
    "gender": gender,
    "address": address,
  };
}
