import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/pelanggan/detail_pelanggan_page.dart';
import 'package:joss_laundry/src/pelanggan/form_pelanggan_baru.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_presenter.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_search.dart';
import 'package:joss_laundry/src/pelanggan/response_body_pelanggan_baru.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class PelangganPage extends StatefulWidget{
  final bool isFromTransaction;

  PelangganPage({this.isFromTransaction});
  @override
  State<StatefulWidget> createState() =>  _State();
}
class _State extends State<PelangganPage> implements PelangganContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  PelangganPresenter _pelangganPresenter;
  LoginResponse      _loginResponse;
  bool isLoading      = true;
  List<Customer> listCustomer = [];

  @override
  void initState(){
    super.initState();
    _pelangganPresenter = PelangganPresenter(this);
    _pelangganPresenter.loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Data Pelanggan",style: TextStyle(color: Colors.black,fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PelangganSearch(listCustomerWidget:this.listCustomer,)),
            ).then((_){
              _pelangganPresenter.loadSharedPrefs();
            });
          })
        ],

      ),
      body: this.isLoading ? Center(child : CircularProgressIndicator(strokeWidth: 1)) : Container(
        child: this.listCustomer.length == 0 ? Center(
          child : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.info_outline_rounded,size: 50,),
              SizedBox(height: 20,),
              Text("Tidak ada data pelanggan !"),
            ],
          ),
        ) : Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(child :  ListView.builder(
                shrinkWrap: true,
                itemCount: this.listCustomer.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index){
                  Customer customer = this.listCustomer[index];
                  return _buildListView(customer,index);
                },
              ),),
              ),
            InkWell(
              onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FormNewCustomer()),
                  ).then((_) {
                    _pelangganPresenter.loadSharedPrefs();
                  });
              },
              child: Container(
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff6bceff),
                        Color(0xFF00abff),
                      ],
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(0)
                    )
                ),
                child: Center(
                  child: Text('Pelanggan Baru'.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListView(Customer customer,int index){
    return  Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(0.0)),
            color: Colors.white,
          ),
          child : ListTile(
            leading: Icon(Icons.account_circle_rounded,size: 45,),
            title: Text(customer.name,style: TextStyle(color: Colors.black,fontSize: 12.0.sp),),
            subtitle:  Text("${customer.phone}",style: TextStyle(color: Colors.black,fontSize: 10.0.sp),),
            trailing: IconButton(icon: Icon(Icons.arrow_forward_ios_rounded),onPressed: (){},),
            onTap: (){
              // panggil detail pelanggan screen
              // paramnya customer
              if(widget.isFromTransaction){
                Navigator.of(context).pop(customer);
              }
              else {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    DetailPelangganPage(pelanggan: customer)));
              }
            },
        ),
    );
  }

  @override
  void onFailurePelanggan(dynamic error) {
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onPelangganLoading(bool isLoading) {
   setState(() {
     this.isLoading = isLoading;
   });
  }

  @override
  void onSuccessListPelanggan(ListPelangganResponse listPelangganResponse) {
    setState(() {
      this.listCustomer = listPelangganResponse.data;
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _pelangganPresenter.doGetListPelangganByOutletId(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessCreatePelanggan(ResonseBodyPelangganBaru resonseBodyPelangganBaru) {
  }
}