import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:sizer/sizer.dart';

class TambahLayanan extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<TambahLayanan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Tambah Layanan",style: TextStyle(color: Colors.black,fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
      ),
      body: Container(
        child: Container(),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: (){
        },
      ),
    );
  }
  
}
