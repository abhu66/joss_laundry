import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/outlet/bottom_sheet_item.dart';
import 'package:joss_laundry/src/outlet/bottom_sheet_kondisi_barang.dart';
import 'package:joss_laundry/src/outlet/daftar_layanan_page.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_page.dart';
import 'package:joss_laundry/src/transaksi/form_tambah_layanan.dart';
import 'package:joss_laundry/src/transaksi/request_body_transaction.dart';
import 'package:joss_laundry/src/transaksi/request_detail_order.dart';
import 'package:joss_laundry/src/transaksi/rincian_transaksi_page.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class TransaksiPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<TransaksiPage> {
  TextEditingController _namaController = new TextEditingController();
  ScrollController _scrollController = new ScrollController();
  TextEditingController _keteranganController = new TextEditingController();

  Customer selectedCustomer;
  List<Layanan> listLayananSelected = [];
  List<Condition> listConditionSelected = [];
  List<int> listIndexCondition = [];
  List<List<Condition>> listConditionAll = [];
  List<DetailOrder> listDetailorder = [];
  bool isVisible =  false;


  @override
  void initState(){
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Transaksi Baru",
            style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
          ),
          backgroundColor: Colors.white,
          elevation: 0.6,
          iconTheme: IconThemeData(color: Colors.black),
          // actions: [IconButton(icon: Icon(Icons.search), onPressed: () {})],
        ),
        body: Container(
          child:
              ListView(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width / 1.2,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                      margin: EdgeInsets.only(top: 20, right: 16.0, left: 16.0),
                      padding: EdgeInsets.only(
                          top: 4.0.sp, left: 16.0.sp, right: 0.0.sp, bottom: 4.0.sp),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),
                      child: TextFormField(
                        onTap: (){
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PelangganPage(
                                    isFromTransaction: true,
                                  )
                              )
                          ).then((value) {
                            setState(() {
                              if(value != null) {
                                this.selectedCustomer = value;
                                _namaController.text = this.selectedCustomer.name + " - "+this.selectedCustomer.phone;
                              }
                            });
                          });
                        },
                        readOnly: true,
                        controller: _namaController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            // icon: Icon(Icons.vpn_key,
                            //   color: Color(0xff6bceff),
                            // ),
                            hintText: 'Pilih pelanggan',
                            hintStyle: TextStyle(color: Colors.black, fontSize: 12.0.sp),
                            suffixIcon: IconButton(
                              icon: Icon(Icons.arrow_forward_ios_rounded),
                              onPressed: () {
                              },
                            )),
                      ),
                  ),
                  InkWell(
                      child: Container(
                        margin: EdgeInsets.all(20),
                        width: MediaQuery.of(context).size.width,
                        height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 45,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Center(
                            child: Text(
                              "Tambah Layanan",
                              style: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.w700),
                            )),
                      ),
                      onTap: () async {
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DaftarLayananPage(
                                  isFromTransaction: true,
                                )
                            )
                        ).then((value) {
                          setState(() {
                            if(value != null) {
                              this.listDetailorder.add(
                                DetailOrder(
                                  amount: int.parse(value.price),
                                  total: int.parse(value.price),
                                  description: "",
                                  listKondisi: [],
                                  service: value,
                                  listItem: [],
                                  totalPcsOrKg: 1,
                                ),
                              );
                            }
                          });
                        });
                      }
                  ),
                  ListView.builder(
                      itemCount: this.listDetailorder.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context,int index){
                        DetailOrder detailOrder = this.listDetailorder[index];
                        return layananWidget(detailOrder,index);
                      }),
                  Visibility(child: InkWell(
                      child: Container(
                        margin: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 40,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        ),
                        child: Center(
                            child: Text(
                              "Lanjut",
                              style: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.w700),
                            )),
                      ),
                      onTap:  () {
                          if(selectedCustomer != null){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => RincianTranskasiPage(
                              detailOrders: this.listDetailorder,
                              customer: this.selectedCustomer,),
                            ));
                          }
                          else {
                            showAalert("Perhatian", "Harap pilih pelanggan !", context);
                          }

                      }
                  ),
                    visible: this.listDetailorder.length != 0 ? true : false,
                  ),
                ],
              ),
        ),
    );
  }

  Widget _buildListView(int index) {
    return Container(
      color: Colors.white,
      child: InkWell(
        child: ListTile(
          leading: Icon(
            Icons.account_circle_rounded,
            size: 45,
          ),
          title: Text(
            "Abu Khoerul Iskandar Ali",
            style: TextStyle(color: Colors.black, fontSize: 12.0.sp),
          ),
          subtitle: Text(
            "08121820958$index \nJl. KP.Gombol paya Kalideres...",
            style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
          ),
          trailing: IconButton(
            icon: Icon(Icons.more_vert_rounded),
            onPressed: () {},
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TambahLayanan()));
          },
        ),
      ),
    );
  }

  Widget layananWidget(DetailOrder detailOrder, int index){
    return Container(
      key: UniqueKey(),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(color: Colors.black12, blurRadius: 1)
          ]),
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(5),
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Row(
            children: [
              SizedBox(width: 10,),
              Text("${detailOrder.service.name} ${detailOrder.service.unit == "KG" ? "per Kg" : ""}"),
              Spacer(),
              IconButton(icon: Icon(Icons.remove_circle_outline_rounded,), onPressed: (){
                setState(() {
                  this.listDetailorder.remove(detailOrder);
                });
              }),
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.remove),
                onPressed: detailOrder.totalPcsOrKg == 1 ? null : (){
                  setState(() {
                    detailOrder.totalPcsOrKg = detailOrder.totalPcsOrKg -1;
                    int total = detailOrder.totalPcsOrKg * int.parse(detailOrder.service.price);
                    detailOrder.total = total;
                  });
                },
              ),
              Text(detailOrder.totalPcsOrKg.toString()),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: (){
                  setState(() {
                    detailOrder.totalPcsOrKg = detailOrder.totalPcsOrKg + 1;
                    int total = detailOrder.totalPcsOrKg * int.parse(detailOrder.service.price);
                    print(total.toString());
                    detailOrder.total = total;
                  });
                },
              ),
            ],
          ),
            Visibility(
              visible:detailOrder.service?.item != null ? true : false,
                child:
                  Divider()
            ),
            Visibility(child:
              InkWell(
                child: Container(
                  margin: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 40,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  ),
                  child: Center(
                      child: Text(
                        "Tambah Item",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700),
                      )),
                ),
                onTap: () async {
                  await showBottomSheetItem(detailOrder);
                  }
              ),
              visible: detailOrder.service.item != null ? true : false,
            ),
           ListView.builder(
               shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: detailOrder.listItem.length,
              itemBuilder: (BuildContext context, int index){
                return Row(
                  children: [
                    SizedBox(width: 10,),
                    Text("*${detailOrder.listItem[index].name}"),
                    Spacer(),
                    InkWell(
                      child :Container(
                        width: 100,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(color: Colors.black12, blurRadius: 1)
                            ]),
                        margin: EdgeInsets.all(10.0),
                        padding: EdgeInsets.all(5),
                        child: Center(child: Text(detailOrder.listItem[index].value),
                        ),
                      ),
                      onTap: () async {
                        await _showDialogInput(detailOrder,detailOrder.listItem[index],index);
                      },
                    ),
                  ],
                );
              }
          ),
          Divider(),
          InkWell(
              child: Container(
                margin: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 40,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                ),
                child: Center(
                    child: Text(
                      "Kondisi Barang",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    )),
              ),
              onTap: () async {
                  await showBottomSheetKondisi(detailOrder);
              }
          ),
          SizedBox(
            child :
          ListView.builder(
            itemCount: detailOrder.listKondisi.length,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index){
              Condition kondisi = detailOrder.listKondisi[index];
              return  Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.black12, blurRadius: 1)
                    ]),
                margin: EdgeInsets.all(5.0),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Center(child: Text(kondisi.name),
                ),
              );
            },
          ),
            height: 40,
          ),
          Divider(),
            InkWell(
              onTap: (){
                _showDialogInputCatatan(detailOrder, index);
              },
              child : Container(
                width: 100,
                decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                color: Colors.white,
                boxShadow: [
                BoxShadow(color: Colors.black12, blurRadius: 1)
                ]),
                margin: EdgeInsets.only(right: MediaQuery.of(context).size.width - 200,left: 5),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Center(
                  child: Row(children: [
                    Icon(Icons.add_circle_outlined),
                    SizedBox(width: 10,),
                    Text(detailOrder.description.length == 0 ? "Tulis catatan" : "Edit catatan"),
                    ],
                  ),
                ),
              ),
            ),
          Divider(),
          Padding(padding: EdgeInsets.all(10),child : Text(detailOrder.description ?? "")),

          Divider(),
          SizedBox(child : Center(
            child: Text(
            "Rp."+detailOrder.total.toString() + ",-",
            style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20.0),
            )
            ),
            height: 50,
          ),
        ],
      ),
    );
  }

  showBottomSheetKondisi(DetailOrder detailOrder) async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
                  padding: EdgeInsets.only(top: 30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(color: Colors.grey.withOpacity(0.6)),
                    color: Colors.white,
                  ),
                  child: BottomSheetKondisiBarang(listHasSelected:detailOrder.listKondisi,),
                );
        }).then((value){
          setStateDataCondition(value,detailOrder);
         });
  }

  showBottomSheetItem(DetailOrder detailOrder) async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: BottomSheetItem(listItem: detailOrder.service.item,),
          );
        }).then((value){
          setSelected(detailOrder,value);
        });
  }

  void setStateDataCondition(List<Condition> listCondition, DetailOrder detailOrderd){
    setState(() {
      final detailOrder = this.listDetailorder.firstWhere((e) => e.service.id == detailOrderd.service.id,orElse: () => null);
      if (detailOrder != null) {
        detailOrderd.listKondisi.map((e){
          var contains = detailOrderd.listKondisi.where((element) => e.name == element.name);
          if(contains.isEmpty){
            detailOrderd.listKondisi.add(e);
          }
        }).toList();
      }

        //
    });
  }

  setSelected(DetailOrder detailOrderd,ServiceItem item){
    setState(() {
        final detailOrder = this.listDetailorder.firstWhere((e) => e.service.id == detailOrderd.service.id,orElse: () => null);
        if (detailOrder != null){
            var contains = detailOrderd.listItem.where((element) => item.name == element.name);
            if(contains.isEmpty){
              detailOrder.listItem.add(item);
            }
        }
    });
  }

  _showDialogInput(DetailOrder detailOrder,ServiceItem item,int index) {
    int value = int.parse(item == null ? 0 : item.value);
    return showDialog(
        context: context,
      builder: (BuildContext contexts) {
        return StatefulBuilder(builder: (context, StateSetter setState){
          return AlertDialog(
            content: Container(
              child: Center(child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: value == 0 ? null : () {
                      setState((){
                        value = value - 1;
                      });
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      color: value ==  0 ? Colors.grey.withOpacity(0.5) : Colors.green,
                      child: Center(child: Icon(
                        Icons.remove, color: Colors.white, size: 20,),),
                    ),
                  ),
                  Flexible(child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 1)
                        ]),
                    margin: EdgeInsets.all(10.0),
                    padding: EdgeInsets.all(5),
                    child: Center(child: Text("$value"),
                    ),
                  ),
                  ),

                  InkWell(
                    onTap: () {
                      setState((){
                        value = value + 1;
                      });
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      color: Colors.green,
                      child: Center(child: Icon(
                        Icons.add, color: Colors.white, size: 20,),),
                    ),
                  ),
                ],
              ),
              ),
              height: 70,
            ),
            actions: [
              InkWell(
                  child: Container(
                    margin: EdgeInsets.all(0),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    ),
                    child: Center(
                        child: Text(
                          "Ok",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w700),
                        )),
                  ),
                  onTap: () async {
                    Navigator.of(context).pop(value);
                  }
              ),
              InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 5),
                    width: MediaQuery.of(contexts).size.width,
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    ),
                    child: Center(
                        child: Text(
                          "Batal",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w700),
                        )),
                  ),
                  onTap: () {
                    Navigator.of(contexts).pop();
                  }
              ),
            ],
          );
        });
      }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        var contains = detailOrder.listItem.firstWhere((element) => item.name == element.name,orElse: () => null);
        if(contains != null){
          contains.name = item.name;
          contains.value = value.toString();
          if(value == 0){
            detailOrder.listItem.removeAt(index);
          }
          else {
            detailOrder.listItem[index] = contains;
          }
        }
      });
    });
  }


  _showDialogInputCatatan(DetailOrder detailOrder,int index) {
    String keterangan = detailOrder.description ?? "";
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: SizerUtil.deviceType == DeviceType.Tablet ? 250 : 120,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(
                              top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.grey.withOpacity(0.6)),
                            color: Colors.white,
                          ),
                          child: TextFormField(
                            key: UniqueKey(),
                            expands: true,
                            initialValue: detailOrder.description ?? "",
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            autofocus: true,
                            onChanged: (value) => keterangan = value,
                            onSaved: (value) => keterangan = value,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // icon: Icon(Icons.vpn_key,
                              //   color: Color(0xff6bceff),
                              // ),
                              hintText: 'Keterangan',
                              hintStyle: TextStyle(color: Colors.black,fontSize: 12.0.sp),
                            ),
                          ),
                        ),
                    ),
                  ],
                ),
                ),
                height: 200,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Simpan",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(keterangan);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop(false);
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        if(value != false){
          detailOrder.description = value;
        }

      });
    });
  }
}
class DetailOrder {
  int totalPcsOrKg;
  Layanan service;
  String description;
  int total;
  int amount;
  List<ServiceItem> listItem;
  List<Condition> listKondisi;
  DetailOrder({this.service,this.description,this.total,this.listKondisi,this.amount,this.listItem,this.totalPcsOrKg});
}
