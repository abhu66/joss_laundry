import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';

class OrderApi{
  final createOrderUrl   = "order";
  final listOrderByUrl   = "order/list/";
  final listOrderUnpaidByUrl   = "order/list/";
  final finishOrderUrl   = "order/status";
  final detailOrderUrl   = "order/";
  Config config  = new Config();

  Future<ResponseBodyOrder> createOrder({String key,String secretKey,BodyRequestOrder bodyRequestOrder}) async {
    return ResponseBodyOrder.fromJson(
        await config.doPosts(endpoint:config.baseUrl + createOrderUrl,dataBody:bodyRequestOrder ,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseBodyListTransaksi> getListTransactionnByOutletId({String key,String secretKey,dynamic outletId}) async {
    return ResponseBodyListTransaksi.fromJson(
        await config.doGets(endpoint:config.baseUrl + listOrderByUrl+outletId,
            key: key,
            secretKey: secretKey));
  }

  Future<ResponseBodyFinishOrder> finishOrder({String key,String secretKey,RequestBodyFinishOrder requestBodyFinishOrder}) async {
    return ResponseBodyFinishOrder.fromJson(
        await config.doPosts(endpoint:config.baseUrl + finishOrderUrl,dataBody:requestBodyFinishOrder ,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseBodyDetailOrder> detDetailOrder({String key,String secretKey,dynamic orderId}) async {
    return ResponseBodyDetailOrder.fromJson(
        await config.doGets(endpoint:config.baseUrl + detailOrderUrl+orderId,
            key: key,
            secretKey: secretKey));
  }
  Future<ResponseBodyListTransaksi> getListUnpaid({String key,String secretKey,dynamic outletId}) async {
    return ResponseBodyListTransaksi.fromJson(
        await config.doGets(
            endpoint: config.baseUrl + listOrderUnpaidByUrl + outletId + "/paid",
            key: key,
            secretKey: secretKey));
  }
}