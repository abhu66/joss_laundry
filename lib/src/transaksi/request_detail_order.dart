// To parse this JSON data, do
//
//     final requestDetailOrder = requestDetailOrderFromJson(jsonString);

import 'dart:convert';

import 'package:joss_laundry/src/transaksi/request_body_transaction.dart';

RequestDetailOrder requestDetailOrderFromJson(String str) => RequestDetailOrder.fromJson(json.decode(str));

String requestDetailOrderToJson(RequestDetailOrder data) => json.encode(data.toJson());

class RequestDetailOrder {
  RequestDetailOrder({
    this.serviceId,
    this.amount,
    this.serviceItem,
    this.condition,
    this.description,
    this.total,
  });

  int serviceId;
  int amount;
  List<ServiceItem> serviceItem;
  List<KondisiBarang> condition;
  String description;
  int total;

  factory RequestDetailOrder.fromJson(Map<String, dynamic> json) => RequestDetailOrder(
    serviceId: json["service_id"],
    amount: json["amount"],
    serviceItem: List<ServiceItem>.from(json["service_item"].map((x) => ServiceItem.fromJson(x))),
    condition: List<KondisiBarang>.from(json["condition"].map((x) => KondisiBarang.fromJson(x))),
    description: json["description"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "service_id": serviceId,
    "amount": amount,
    "service_item": List<dynamic>.from(serviceItem.map((x) => x.toJson())),
    "condition": List<dynamic>.from(condition.map((x) => x.toJson())),
    "description": description,
    "total": total,
  };
}

