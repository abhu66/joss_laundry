import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/outlet_presenter.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/transaksi/history/print_page.dart';
import 'package:joss_laundry/src/transaksi/history/printer_screen.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class DetailDataTransaksi extends StatefulWidget{
  final DataOrder data;
  DetailDataTransaksi({this.data});

  @override
  _State createState() => _State();
}

class _State extends State<DetailDataTransaksi>  implements OrderContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  LoginResponse _loginResponse;
  bool isLoading = true;
  OrderPresenter _orderPresenter;
  ResponseBodyDetailOrder dataDetail;

  List choices = [
    CustomPopupMenu(title: 'Cetak', icon: Icons.print),
    CustomPopupMenu(title: 'Kirim Nota', icon: Icons.send_outlined),
    CustomPopupMenu(title: 'Bayar', icon: Icons.account_balance_wallet),
    CustomPopupMenu(title: 'Selesaikan Laundry', icon: Icons.assignment_turned_in_rounded),
    CustomPopupMenu(title: 'Laundry Diambil', icon: Icons.bike_scooter_rounded),
  ];
  CustomPopupMenu _selectedChoices;

  @override
  void initState() {
    _orderPresenter = OrderPresenter(this);
    _orderPresenter.loadSharedPrefs();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Detail Transaksi",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(color: Colors.black),
        actions: [
          _paddingPopup(),
        ],
      ),
      body: ListView(
        physics: ScrollPhysics(),
          children: [
          Container(
            width: MediaQuery.of(context).size.width, // Lebar
            height: SizerUtil.deviceType == DeviceType.Tablet ? 200: 90,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Colors.white,
            ),
            margin: EdgeInsets.all(10.0),
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Center(
                  child: Text(
                    "${widget.data.orderNumber}",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp),
                  ),
                ),
                Divider(height: 10.0, color: Colors.grey,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Column(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Icon(Icons.date_range_outlined, size: 20.0.sp)
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Padding(padding: EdgeInsets.all(10)),
                        Text("${DateFormat("dd MMMM yyyy").format(widget.data.orderDate)}", style: TextStyle(fontSize: 12.0.sp),)
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Padding(padding: EdgeInsets.all(10)),
                        Text("-", style: TextStyle(fontSize: 12.0.sp),)
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Padding(padding: EdgeInsets.all(10)),
                        Icon(Icons.date_range_outlined, size: 20.0.sp,)
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Padding(padding: EdgeInsets.all(10)),
                        Text("${widget.data.completionDate != null ?
                        DateFormat("dd MMMM yyyy").format(DateTime.parse(widget.data.completionDate)) :
                        DateFormat("dd MMMM yyyy").format(DateTime.parse(widget.data.estimationDate))}", style: TextStyle(fontSize: 12.0.sp)),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(10),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Text(
                    "Informasi Pelanggan",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp),
                  ),
                  Divider(color: Colors.grey),
                  Wrap(
                    children: [
                      Row(

                        children: [
                          Text("Nama              :", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                          Text("${widget.data.customer.name}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("No. Hp            :", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                          Text("${widget.data.customer.phone}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("Email               :", textAlign: TextAlign.left,style: TextStyle(fontSize: 12.0.sp)),
                          Text("${widget.data.customer.email}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("Alamat            :", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                          Flexible(child: Text("${widget.data.customer.address}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp))),
                        ],
                      ),
                      Row(
                        children: [
                          Text("Tanggal Lahir :", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                          Text("${DateFormat("dd MMMM yyyy").format(widget.data.customer.dateOfBirth)}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0.sp)),
                        ],
                      )
                    ],
                  ),
                ],
              )
          ),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(10),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Text(
                    "Informasi Layanan",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp),
                  ),
                  Divider(color: Colors.grey),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount:dataDetail == null  || dataDetail.data.detail.length == 0 ? 0  : dataDetail.data.detail.length,
                    itemBuilder: (BuildContext context, int index){
                      Details dat = dataDetail.data.detail[index];
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text((index+1).toString()+ ". ${dat.serviceName} : ",style: TextStyle(fontSize: 12.0.sp)),
                          Text("Rp. ${dat.total},- ", style: TextStyle(fontSize: 12.0.sp)),
                        ],
                      );
                    },
                  ),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 300.0,
                        height: 45.0,
                        margin: EdgeInsets.only(bottom: 20.0),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(50)
                        ),
                        child: Center(
                          child: Text(
                              "${widget.data.status}", style: TextStyle(color: Colors.white, fontSize: 12.0.sp),
                          ),
                        ),
                      ),
                      // Container(
                      //   width: 100,
                      //   decoration: BoxDecoration(
                      //       color: Colors.red,
                      //       borderRadius: BorderRadius.circular(50)
                      //   ),
                      //   child: Center(
                      //     child: Text(
                      //         "Belum Selesai", style: TextStyle(color: Colors.white),
                      //     ),
                      //   ),
                      // )
                    ],
                  )
                ],
              )
          ),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(10),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Text(
                    "Informasi Pembayaran",
                    style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                  ),
                  Divider(color: Colors.grey),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Status Bayar",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Text("${dataDetail?.data?.paymentStatus ?? "0.00"}".toUpperCase(),
                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Diskon", style: TextStyle(fontSize: 12.0.sp)),
                      Text("${dataDetail?.data?.discountAmount ?? "0.00"},-", style: TextStyle(fontSize: 12.0.sp)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Tagihan", style: TextStyle(fontSize: 12.0.sp)),
                      Text("${dataDetail?.data?.paymentRemaining ?? "0.00"},-", style: TextStyle(fontSize: 12.0.sp)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Pajak", style: TextStyle(fontSize: 12.0.sp)),
                      Text("${dataDetail?.data?.taxAmount ?? "0.00"},-", style: TextStyle(fontSize: 12.0.sp)),
                    ],
                  ),
                  Divider(color: Colors.grey),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Grand Total",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Text("Rp.${dataDetail?.data?.total ?? "0.00"},-".toUpperCase(),style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                    ],
                  ),
                  Divider(color: Colors.grey),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Bayar",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Text("Rp.${dataDetail?.data?.paymentAmount ?? "0.00"},-".toUpperCase(),style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                    ],
                  ),
                  Divider(color: Colors.grey),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sisa Bayar",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Text("Rp.${dataDetail?.data?.paymentRemaining ?? "0.00"},-".toUpperCase(),style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                    ],
                  ),
                  Divider(color: Colors.grey),
                  SizedBox(height: 100,)
                ],
              )
            ),
          ]
        ),
      // bottomSheet: _buttonNavigation(),
    );
  }

  Widget _buttonNavigation(){
    return Container(
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            child: InkWell(
              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => PrinterScreen(dataOrder: widget.data,dataDetail: dataDetail,)));
              },
              child: Column(
                children: [
                  Container(
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xff6bceff),
                            Color(0xFF00abff),
                          ],
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(0)
                        )
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.print, color: Colors.white, size: 20.0.sp),
                          SizedBox(width: 16.0,),
                          Text('Cetak'.toUpperCase(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 12.0.sp
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Flexible(
          //   child: Column(
          //     children: [
          //       Icon(Icons.send_sharp),
          //       Text("Kirim Nota", style: TextStyle(fontSize: 10),)
          //     ],
          //   ),
          // ),
          // Flexible(
          //   child: Column(
          //     children: [
          //       Icon(Icons.payment_sharp),
          //       Text("Bayar", style: TextStyle(fontSize: 10),)
          //     ],
          //   ),
          // ),
          // Flexible(
          //   child: Column(
          //     children: [
          //       Icon(Icons.cloud_done_sharp),
          //       Expanded(child: Text("Selesaikan Laundry", style: TextStyle(fontSize: 10), textAlign: TextAlign.center,))
          //     ],
          //   ),
          // ),
          // Flexible(
          //   child: Column(
          //     children: [
          //       Icon(Icons.motorcycle_sharp),
          //       Text("Laundry Diambil", style: TextStyle(fontSize: 10), textAlign: TextAlign.center,)
          //     ],
          //   ),
          // ),
        ],
      )
    );
  }



  @override
  void onFailure(String error) {
    setState(() {

      showAalert("Failed !","message : "+error.replaceAll("Exception:", ""),context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi responseBodyListTransaksi) {
    setState(() {
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {

  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(this._loginResponse.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _orderPresenter.doGetDetailByOrderId(digestUsername.toString(), response.data.accessSecret, widget.data.id.toString());
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessFinishOrder
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    setState(() {
      print("ddd : "+responseBodyDetailOrder.toJson().toString());
      this.dataDetail = responseBodyDetailOrder;
    });
  }


  Widget _paddingPopup() => PopupMenuButton<int>(

    onSelected: select,
    itemBuilder: (context) => [
      PopupMenuItem(
        value: 1,
        child: Row(children: [
          Icon(Icons.print),
          SizedBox(width: 10,),
          Text(
            "Cetak",
          )
        ],)
      ),
      PopupMenuItem(
        value: 2,
        child: Row(children: [
          Icon(Icons.send_rounded),
          SizedBox(width: 10,),
          Text(
            "Kirim Nota",
          )
        ],)
      ),
      PopupMenuItem(
        value: 3,
        child:Row(children: [
          Icon(Icons.payment_rounded),
          SizedBox(width: 10,),
          Text(
            "Bayar",
          )
        ],)
      ),
      PopupMenuItem(
        value: 4,
        child: Row(children: [
          Icon(Icons.assignment_turned_in_rounded),
          SizedBox(width: 10,),
          Text(
            "Selesaikan Laundry",
          )
        ],)
      ),
      PopupMenuItem(
        value: 5,
        child:Row(children: [
          Icon(Icons.bike_scooter_rounded),
          SizedBox(width: 10,),
          Text(
            "Laundry Diambil",
          )
        ],)
      ),
    ],
    elevation: 0,
    padding: EdgeInsets.symmetric(horizontal: 1),
  );

  void select(int selected) {
    setState(() {
      switch (selected.toString()) {
        case "1" :
          return showBottomSheetItem();
          break;
      }
    });
  }

  showBottomSheetItem() async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: PrinterScreen(dataOrder: widget.data, dataDetail: dataDetail,));
        }).then((value){
    });
  }
}

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.icon});
  String title;
  IconData icon;
}
