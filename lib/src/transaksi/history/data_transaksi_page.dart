import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/transaksi/history/detail_data_transaksi_page.dart';
import 'package:joss_laundry/src/transaksi/history/transaksi_search.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class DataTransaksiPage extends StatefulWidget{
  final List<DataOrder> listTransaksiWidget;
  DataTransaksiPage({this.listTransaksiWidget});

  @override
  State<StatefulWidget> createState() =>  _State();
}
class _State extends State<DataTransaksiPage> implements OrderContract{
  List<DataOrder> listTr = [] ;
  OrderPresenter _orderPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;

  @override
  void initState(){
    super.initState();
    _orderPresenter = OrderPresenter(this);
    _orderPresenter.loadSharedPrefs();

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Data Transaksi",style: TextStyle(color: Colors.black,fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(
          color: Colors.black
        ),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => TransaksiSearch(listTransaksiWidget:this.listTr,)),
            ).then((_){
              _orderPresenter.loadSharedPrefs();
            });
          })
        ],
      ),
      body: this.isLoading ? Center(child : CircularProgressIndicator(strokeWidth: 1,)) : Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child : ListView.builder(
              shrinkWrap: true,
              itemCount: this.listTr.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index){
                DataOrder data = this.listTr[index];
                return _buildListView(context, index, data);
              },
            ),),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListView(BuildContext context, int index,DataOrder dataOrder) {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                  Radius.circular(10.0)
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 1
                )
              ]
          ),
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(10),
        child: ListView(
          shrinkWrap:true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Text(dataOrder.orderNumber, style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
            Divider(color: Colors.grey,),
            Text("${dataOrder.customer.name} - ${dataOrder.customer.phone} ", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
            SizedBox(height: 10,),
            Text("Rp. ${dataOrder.total}", style: TextStyle(color: Colors.blue, fontSize: 10.0.sp),),
            SizedBox(height: 10,),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Diterima", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                Text(dataOrder.orderDate.toLocal().toString().substring(0,16), style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
            ],),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Selesai", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                Text(dataOrder.completionDate != null ? dataOrder.completionDate.toString().substring(0,16) :
                (dataOrder.orderDate).toLocal().toString().substring(0,16) , style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
              ],),
            SizedBox(height: 20,),
            Row(
              children: [
                InkWell(
                  onTap: (){
                  },
                  child: Container(
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 40 : 35,
                    width: SizerUtil.deviceType == DeviceType.Tablet ? 180 : 100,
                    decoration: BoxDecoration(
                        color: dataOrder.paymentStatus?.toUpperCase() == "PAID" ? Colors.red : Colors.yellow,
                        borderRadius: BorderRadius.all(
                            Radius.circular(50)
                        )
                    ),
                    child: Center(
                      child: Text("${dataOrder.paymentStatus?.toUpperCase()}",
                        style: TextStyle(
                            color: dataOrder.paymentStatus?.toUpperCase() == "PAID" ? Colors.white : Colors.red,
                            fontWeight: FontWeight.bold,
                            fontSize: 8.0.sp
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                InkWell(
                  onTap: (){
                  },
                  child: Container(
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 40 : 35,
                    width: SizerUtil.deviceType == DeviceType.Tablet ? 140 : 100,
                    decoration: BoxDecoration(
                      color: dataOrder.status.toUpperCase() == "DONE" ? Colors.blue : Colors.green,
                        borderRadius: BorderRadius.all(
                            Radius.circular(50)
                        )
                    ),
                    child: Center(
                      child: Text(dataOrder.status.toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 8.0.sp
                        ),
                      ),
                    ),
                  ),
                ),
              ],)
          ],
        )
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailDataTransaksi(data:dataOrder)));
      },
    );
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Failed !","message : "+error.replaceAll("Exception:", ""),context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi responseBodyListTransaksi) {
    setState(() {
      this.listTr = responseBodyListTransaksi.data;
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {

  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(this._loginResponse.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _orderPresenter.doGetListTransactionByOutletId(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessFinishOrder
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    // TODO: implement onsSuccessDetailOrder
  }

  @override
  void onSuccessOrderUnpaid(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessOrderUnpaid
  }
}