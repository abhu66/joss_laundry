import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/transaksi/history/detail_data_transaksi_page.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:sizer/sizer.dart';

class TransaksiSearch extends StatefulWidget {
  final List<DataOrder> listTransaksiWidget;
  TransaksiSearch({this.listTransaksiWidget});

  @override
  _TransaksiSearchState createState() => _TransaksiSearchState();
}

class _TransaksiSearchState extends State<TransaksiSearch> {
  TextEditingController controller = new TextEditingController();
  List<DataOrder> listTr = [];
  List<DataOrder> listSearch = [];
  bool firstSearch = true;
  String query ="";

  @override
  void initState() {
    super.initState();
    this.listTr = widget.listTransaksiWidget;
    controller.addListener(() {
      //menambahkan method yang akan dipanggil ketika object ada berubah
      if (controller.text.isEmpty) {
        setState(() {
          firstSearch = true;
          query = "";
        });
      } else {
        //data tidak kosong
        setState(() {
          firstSearch = false;
          query = controller.text;
          print("Query : " + query);
        });
      }
    });
  }

  Widget _performSearch(){
    listSearch = new List<DataOrder>();
    for(int i=0; i<listTr.length;i++){
      var item = listTr[i];

      if(item.customer.name.toLowerCase().contains(query.toLowerCase())){
        listSearch.add(item);
      }
    }
    return Expanded(
      child: SingleChildScrollView(child :  ListView.builder(
        shrinkWrap: true,
        itemCount: this.listSearch.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index){
          DataOrder data = this.listSearch[index];
          return _buildListView(data,index);
        },
      ),),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: 70,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){
                Navigator.pop(context);
              }),
              Flexible(child: Container(
                margin: EdgeInsets.all(10.0.sp),
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45.0,
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    color: Colors.white,
                    boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.search,
                      color: Color(0xff6bceff),
                    ),
                    hintText: 'Cari Transaksi',
                  ),
                ),
              ),
              ),
            ],
          ),
          !firstSearch ?  _performSearch() : Expanded(
            child: SingleChildScrollView(child :  ListView.builder(
              shrinkWrap: true,
              itemCount: this.listTr.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index){
                DataOrder data  = this.listTr[index];
                return _buildListView(data, index);
              },
            ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView(DataOrder dataOrder, int index) {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                  Radius.circular(10.0)
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 1
                )
              ]
          ),
          margin: EdgeInsets.all(10.0),
          padding: EdgeInsets.all(10),
          child: ListView(
            shrinkWrap:true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Text(dataOrder.orderNumber, style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
              Divider(color: Colors.grey,),
              Text("${dataOrder.customer.name} - ${dataOrder.customer.phone}", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
              SizedBox(height: 10,),
              Text("Rp. ${dataOrder.total}", style: TextStyle(color: Colors.blue, fontSize: 10.0.sp),),
              SizedBox(height: 10,),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Diterima", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(dataOrder.orderDate.toLocal().toString().substring(0,16), style:
                  TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Selesai", style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(dataOrder.completionDate != null ? dataOrder.completionDate.toString().substring(0,16) : "-",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              SizedBox(height: 20,),
              Row(
                children: [
                  InkWell(
                    onTap: (){
                    },
                    child: Container(
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 40 : 35,
                      width: SizerUtil.deviceType == DeviceType.Tablet ? 140 : 100,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xff6bceff),
                              Color(0xFF00abff),
                            ],
                          ),
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          )
                      ),
                      child: Center(
                        child: Text(dataOrder.status.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 8.0.sp
                          ),
                        ),
                      ),
                    ),
                  ),
                ],)
            ],
          )
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailDataTransaksi()));
      },
    );
  }
}
