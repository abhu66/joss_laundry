import 'dart:typed_data';

import 'package:bluetooth_thermal_printer/bluetooth_thermal_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/const/const_string.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs_presenters.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'dart:developer';

class PrinterScreen extends StatefulWidget {
  final DataOrder dataOrder;
  final ResponseBodyDetailOrder dataDetail;
  PrinterScreen({this.dataOrder,this.dataDetail});
  @override
  _PrinterScreenState createState() => _PrinterScreenState();
}

class _PrinterScreenState extends State<PrinterScreen> implements SharedPrefContract {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  OrderPresenter _orderPresenter;
  List availableBluetoothDevices = new List();
  bool connected = false;
  SharedPrefPresenter _sharedPrefPresenter;
  LoginResponse _loginResponse;
  bool isLoading = true;
  Details details;
  Color color;
  int selectedPrinter;
  bool isFindBluetooth = false;

  Future<void> getBluetooth() async {
    setState(() {
      this.isFindBluetooth = true;
    });
    await BluetoothThermalPrinter.getBluetooths.then((value){
      print("error $value");
      setState(() {
        availableBluetoothDevices = value;
        this.isFindBluetooth = true;
      });
    }).catchError((error){
      this.isFindBluetooth = true;
      print("error $error");
    });
  }

  Future<void> setConnect(String mac) async {
    final String result = await BluetoothThermalPrinter.connect(mac);
    print("state connected $result");
    if (result == "true") {
      setState(() {
        connected = true;
      });
    }
  }

  Future<void> printTicket() async {
    String isConnected = await BluetoothThermalPrinter.connectionStatus;
    if (isConnected == "true") {
      Ticket ticket = await getTicket();
      final result = await BluetoothThermalPrinter.writeBytes(ticket.bytes);
      print("Print $result");
      Navigator.of(context).pop();
    } else {
      //Hadnle Not Connected Senario
    }
  }

  Future<void> printGraphics() async {
    String isConnected = await BluetoothThermalPrinter.connectionStatus;
    if (isConnected == "true") {
      Ticket ticket = await getGraphicsTicket();
      final result = await BluetoothThermalPrinter.writeBytes(ticket.bytes);
      print("Print $result");
    } else {
      //Hadnle Not Connected Senario
    }
  }

  Future<Ticket> getGraphicsTicket() async {
    //CapabilityProfile profile = await CapabilityProfile.load();
    final Ticket ticket = Ticket(PaperSize.mm80);

    // Print QR Code using native function
    ticket.qrcode('${widget.dataOrder.orderNumber}');

    ticket.hr();

    // Print Barcode using native function
    final List<dynamic> barData = [widget.dataOrder.orderNumber];

    ticket.barcode(Barcode.codabar(barData));

    ticket.cut();

    return ticket;
  }

  Future<Ticket> getTicket() async {
   // CapabilityProfile profile = await CapabilityProfile.load();
    // Print Barcode using native function
    final List<dynamic> barData = widget.dataOrder.orderNumber.split("");
    print("bardata  :$barData");
    final Ticket ticket = Ticket(PaperSize.mm58);

    bool isModeText = await SharedPref.readModeTeks("text");

    if(isModeText){
      ticket.text("", styles: PosStyles(align: PosAlign.center, height: PosTextSize.size2, width: PosTextSize.size7,), linesAfter:2,);
    }
    else {
      bool isGunakanBarcode = await SharedPref.readGunakanBarcode("barcode");
      isGunakanBarcode ? ticket.barcode(Barcode.code39(barData)) : ticket.qrcode('${widget.dataOrder.orderNumber}',align: PosAlign.center,size: QRSize.Size8);
    }

    ticket.text("${this._loginResponse.data.outlet.name}", styles: PosStyles(align: PosAlign.center, height: PosTextSize.size2, width: PosTextSize.size7,), linesAfter:2,);
    bool isHideAddress = await SharedPref.readBool("hideAddress");
    if(!isHideAddress){
      ticket.text("${this._loginResponse.data.outlet.address}", styles: PosStyles(align: PosAlign.center));
    }

    ticket.text('Tel: ${this._loginResponse.data.outlet.phone}',
        styles: PosStyles(align: PosAlign.center));
    ticket.hr();

    ticket.row([
      PosColumn(text: 'Nota', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
      PosColumn(text: '${widget.dataOrder.orderNumber}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    bool isHidePelanggan = await SharedPref.readBool("hidePelanggan");
    if(!isHidePelanggan){
      ticket.row([
        PosColumn(text: 'Customer', width: 5, styles: PosStyles(align: PosAlign.left,  )),
        PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
        PosColumn(text: '${widget.dataOrder.customer.name}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
      ]);

      ticket.row([
        PosColumn(text: 'Telp', width: 5, styles: PosStyles(align: PosAlign.left,  )),
        PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
        PosColumn(text: '${widget.dataOrder.customer.phone}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
      ]);
    }


    ticket.row([
      PosColumn(text: 'Terima', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
      PosColumn(text: '${DateFormat("dd/MM/yyyy HH:mm:ss").format(widget.dataOrder.orderDate)}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    bool isHideTanggal = await SharedPref.readBool("hideTanggalSelesai");
    if(!isHideTanggal){
      ticket.row([
        PosColumn(text: 'Selesai', width: 5, styles: PosStyles(align: PosAlign.left,  )),
        PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
        PosColumn(text: '${widget.dataOrder.completionDate == null ? DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(widget.dataOrder.estimationDate)) : DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(widget.dataOrder.completionDate))}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
      ]);
    }

    // ticket.row([
    //   PosColumn(text: 'Parfum', width: 5, styles: PosStyles(align: PosAlign.left,  )),
    //   PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
    //   PosColumn(text: 'Downy Blue Cinta Raja', width: 6, styles: PosStyles(align: PosAlign.right,  )),
    // ]);

    ticket.hr();

    widget.dataDetail.data.detail.map((e) {
      ticket.row([
        PosColumn(text: '${e.serviceName}', width: 7, styles: PosStyles(align: PosAlign.left,width:PosTextSize.size8, )),
        PosColumn(text: 'Rp ${e.total},-', width: 5, styles: PosStyles(align: PosAlign.left,width:PosTextSize.size8,)),
      ]);
    }).toList();

    // ticket.row([
    //   PosColumn(text: '@ Rp 30.000,-', width: 12, styles: PosStyles(align: PosAlign.left,)),
    // ]);
    ticket.hr();
    ticket.row([
      PosColumn(text: 'Total Item', width: 5, styles: PosStyles(align: PosAlign.left,)),
      PosColumn(text: ':', width: 2, styles: PosStyles(align: PosAlign.center,)),
      PosColumn(text: '${widget.dataDetail.data.detail.length} Item ', width: 5, styles: PosStyles(align: PosAlign.right,)),
    ]);
    ticket.hr();

    ticket.row([
      PosColumn(text: 'Total ', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: '${widget.dataDetail.data.subTotal} ,-', width: 7, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    ticket.row([
      PosColumn(text: 'Grand Total', width: 7, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: '${widget.dataDetail.data.total},-', width: 5, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    ticket.row([
      PosColumn(text: 'Pembayaran', width: 12, styles: PosStyles(align: PosAlign.left,  )),
    ]);

    ticket.row([
      PosColumn(text: '-', width: 2, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: 'Tunai', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: '${widget.dataDetail.data.total},-', width: 5, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    ticket.hr();

    ticket.row([
      PosColumn(text: 'Kembalian', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: 'Rp 0 ,-', width: 7, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    ticket.hr();

    ticket.row([
      PosColumn(text: 'Status', width: 5, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
      PosColumn(text: '${widget.dataOrder.paymentStatus}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
    ]);

    // ticket.row([
    //   PosColumn(text: 'Catatan', width: 5, styles: PosStyles(align: PosAlign.left,  )),
    //   PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,  )),
    //   PosColumn(text: '${widget.dataDetail.data.detail[0].description}', width: 6, styles: PosStyles(align: PosAlign.right,  )),
    // ]);
    ticket.hr();
    ticket.row([
      PosColumn(text: 'Express', width: 5, styles: PosStyles(align: PosAlign.left,)),
      PosColumn(text: ':', width: 1, styles: PosStyles(align: PosAlign.center,)),
      PosColumn(text: '${widget.dataOrder.express == true ? "Ya" : "Tidak"}', width: 6, styles: PosStyles(align: PosAlign.right,)),
    ]);
    ticket.row([
      PosColumn(text: '${DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.now())}', width: 7, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: '${_loginResponse.data.outlet.address}', width: 5, styles: PosStyles(align: PosAlign.right,  ))
    ]);

    ticket.hr();

    ticket.row([
      PosColumn(text: 'PERHATIAN :', width: 12, styles: PosStyles(align: PosAlign.left,  )),
    ]);

    ticket.row([
      PosColumn(text: '1. ', width: 1, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ConstString.CONST_WARNING_1, width: 11, styles: PosStyles(align: PosAlign.left,  )),
    ]);

    ticket.row([
      PosColumn(text: '2. ', width: 1, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ConstString.CONST_WARNING_2,width: 11, styles: PosStyles(align: PosAlign.left,)),
    ]);

    ticket.row([
      PosColumn(text: '3. ', width: 1, styles: PosStyles(align: PosAlign.left,)),
      PosColumn(text: ConstString.CONST_WARNING_3, width: 11, styles: PosStyles(align: PosAlign.left,)),
    ]);

    ticket.row([
      PosColumn(text: '4. ', width: 1, styles: PosStyles(align: PosAlign.left,  )),
      PosColumn(text: ConstString.CONST_WARNING_4, width: 11, styles: PosStyles(align: PosAlign.left,)),
    ]);

    ticket.row([
      PosColumn(text: '5. ', width: 1, styles: PosStyles(align: PosAlign.left,)),
      PosColumn(text: ConstString.CONST_WARNING_5, width: 11, styles: PosStyles(align: PosAlign.left,)),
    ]);

    ticket.row([
      PosColumn(text: '6. ', width: 1, styles: PosStyles(align: PosAlign.left,)),
      PosColumn(text: ConstString.CONST_WARNING_6, width: 11, styles: PosStyles(align: PosAlign.left,)),
    ]);

    ticket.hr(ch: '=', linesAfter: 1);

    ticket.text('Terima Kasih', styles: PosStyles(align: PosAlign.center,));

    ticket.hr(ch: '=', linesAfter: 1);

    // ticket.row([
    //   PosColumn(text: "1", width: 1),
    //   PosColumn(text: "Tea", width: 5, styles: PosStyles(align: PosAlign.left,)),
    //   PosColumn(text: "10", width: 2, styles: PosStyles(align: PosAlign.center,)),
    //   PosColumn(text: "1", width: 2, styles: PosStyles(align: PosAlign.center)),
    //   PosColumn(text: "10", width: 2, styles: PosStyles(align: PosAlign.right)),
    // ]);
    //
    // ticket.row([
    //   PosColumn(text: "2", width: 1),
    //   PosColumn(
    //       text: "Sada Dosa",
    //       width: 5,
    //       styles: PosStyles(
    //         align: PosAlign.left,
    //       )),
    //   PosColumn(
    //       text: "30",
    //       width: 2,
    //       styles: PosStyles(
    //         align: PosAlign.center,
    //       )),
    //   PosColumn(text: "1", width: 2, styles: PosStyles(align: PosAlign.center)),
    //   PosColumn(text: "30", width: 2, styles: PosStyles(align: PosAlign.right)),
    // ]);
    //
    // ticket.row([
    //   PosColumn(text: "3", width: 1),
    //   PosColumn(
    //       text: "Masala Dosa",
    //       width: 5,
    //       styles: PosStyles(
    //         align: PosAlign.left,
    //       )),
    //   PosColumn(
    //       text: "50",
    //       width: 2,
    //       styles: PosStyles(
    //         align: PosAlign.center,
    //       )),
    //   PosColumn(text: "1", width: 2, styles: PosStyles(align: PosAlign.center)),
    //   PosColumn(text: "50", width: 2, styles: PosStyles(align: PosAlign.right)),
    // ]);
    //
    // ticket.row([
    //   PosColumn(text: "4", width: 1),
    //   PosColumn(
    //       text: "Rova Dosa",
    //       width: 5,
    //       styles: PosStyles(
    //         align: PosAlign.left,
    //       )),
    //   PosColumn(
    //       text: "70",
    //       width: 2,
    //       styles: PosStyles(
    //         align: PosAlign.center,
    //       )),
    //   PosColumn(text: "1", width: 2, styles: PosStyles(align: PosAlign.center)),
    //   PosColumn(text: "70", width: 2, styles: PosStyles(align: PosAlign.right)),
    // ]);
    //
    // ticket.hr();
    //
    // ticket.row([
    //   PosColumn(
    //       text: 'TOTAL',
    //       width: 6,
    //       styles: PosStyles(
    //         align: PosAlign.left,
    //         height: PosTextSize.size4,
    //         width: PosTextSize.size4,
    //       )),
    //   PosColumn(
    //       text: "160",
    //       width: 6,
    //       styles: PosStyles(
    //         align: PosAlign.right,
    //         height: PosTextSize.size4,
    //         width: PosTextSize.size4,
    //       )),
    // ]);

    // ticket.hr(ch: '=', linesAfter: 1);

    // ticket.feed(2);
    // ticket.text('Thank you!', styles: PosStyles(align: PosAlign.center,  ));
    //
    // ticket.text("26-11-2020 15:22:45", styles: PosStyles(align: PosAlign.center), linesAfter: 1);
    //
    // ticket.text('Note: Goods once sold will not be taken back or exchanged.', styles: PosStyles(align: PosAlign.center, bold: false));
    // ticket.cut();
    return ticket;
  }

  @override
  void initState(){
    super.initState();
    _sharedPrefPresenter = SharedPrefPresenter(this);
    _sharedPrefPresenter.loadDataLogin();
    this.getBluetooth();
  }


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Text("Daftar Perangkat"),
                      Container(
                        height: 200,
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: availableBluetoothDevices.length > 0
                              ? availableBluetoothDevices.length
                              : 0,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(top: 10),
                              padding:  EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color:index == selectedPrinter ? Colors.blue : Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: ListTile(
                                onTap: () {
                                  setState(() {
                                    selectedPrinter = index;
                                    String select = availableBluetoothDevices[index];
                                    List list = select.split("#");
                                    String name = list[0];
                                    String mac = list[1];
                                    this.setConnect(mac);
                                    log("se : $select");
                                    connected = true;
                                  });
                                },
                                title: Text('${availableBluetoothDevices[index]}'),
                                subtitle: Text("Click to connect"),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              OutlineButton(
                onPressed: connected ? this.printTicket : null,
                child: Text("Cetak"),
              ),
            ],
          ),
        ),
    );
  }
  @override
  void onFailure(String error) {
    // TODO: implement onFailure
  }

  @override
  void onLoadingFcmToken(bool isLoading) {
    // TODO: implement onLoadingFcmToken
  }

  @override
  void onLoadingShared(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessAuth(AuthResponse authResponse) {
    // TODO: implement onSuccessAuth
  }

  @override
  void onSuccessDataLogin(LoginResponse loginResponse) {
    setState(() {
      this._loginResponse =  loginResponse;
    });
  }

  @override
  void onSuccessFCMToken(String token) {
    // TODO: implement onSuccessFCMToken
  }

}

class PosColumnCustom {
  PosColumnCustom({
    this.any,
    this.text = '',
    this.textEncoded,
    this.containsChinese = false,
    this.width = 2,
    this.styles = const PosStyles(),
  }) {
    if (width < 1 || width > 12) {
      throw Exception('Column width must be between 1..12');
    }
    if (text != null &&
        text.length > 0 &&
        textEncoded != null &&
        textEncoded.length > 0) {
      throw Exception(
          'Only one parameter - text or textEncoded - should be passed');
    }
  }

  String text;
  Uint8List textEncoded;
  bool containsChinese;
  int width;
  PosStyles styles;
  dynamic any;
}



