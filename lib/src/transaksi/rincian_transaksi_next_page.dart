import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/payment/bottom_sheet_diskon.dart';
import 'package:joss_laundry/src/payment/bottom_sheet_payment_method.dart';
import 'package:joss_laundry/src/payment/response_payment_method.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:joss_laundry/src/transaksi/transaksi_page.dart';
import 'package:sizer/sizer.dart';
import 'dart:developer';

class RincianTranskasiNextPage extends StatefulWidget {
  final List<DetailOrder> detailOrders;
  final Customer customer;
  final bool isExpress;
  final bool isDelivery;
  RincianTranskasiNextPage({this.detailOrders,this.customer,this.isExpress,this.isDelivery});

  @override
  _State createState() => _State();
}

class _State extends State<RincianTranskasiNextPage> implements OrderContract {

  TextEditingController _nominal      = TextEditingController(text:"0");
  TextEditingController _diskonValue  = TextEditingController(text:"0");
  TextEditingController _pajakValue   = TextEditingController(text: "0");
  bool isLoading = false;
  bool _switchVal1 = true;
  bool _switchVal2 = true;
  Datum paymentMethod;
  LoginResponse _loginResponse;
  OrderPresenter orderPresenter;
  double grandTotal = 0;
  double gtSementara = 0;
  double subTotal   = 0;
  double sisa       = 0;
  Diskon diskonSelected;
  int diskonValue = 0;
  var sum = 0;
  List<DetailOrder> listDetailOrder = [];
  double diskonAmount = 0;
  double taxAmount    = 0;



  @override
  void initState(){
    super.initState();
    orderPresenter = OrderPresenter(this);
    orderPresenter.loadSharedPrefs();
    listDetailOrder.addAll(widget.detailOrders);
    //subTotal = sum;
    setTotal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Rincian Transaksi",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
                width: MediaQuery.of(context).size.width / 1.2,
                margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
              child: widget.isExpress ? whenExpress() : whenNotExpress(),

            ),
      Container(
        width: MediaQuery.of(context).size.width / 1.2,
        margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
        padding: EdgeInsets.only(
            top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child:
            ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ListTile(
                  dense: true,
                  title: Text("Sub Total", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                  trailing: Wrap(children: [
                    Text("Rp.$subTotal", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                  ],
                  ),
                ),
                ListTile(
                  dense: true,
                  title: Text("Pajak", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                  trailing :Wrap(
                    spacing: 10,
                    children: [
                    Container(
                      width: 120,
                      child: Row(children: [
                        Container(
                          width: 80,
                          height:SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40.0,
                          padding: EdgeInsets.only(left: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10.0)
                              ),
                              color: Colors.white,
                              border: Border.all(color: Colors.grey)
                          ),
                          child: TextFormField(
                            readOnly: true,
                            controller: _pajakValue,
                            textAlign: TextAlign.end,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '',
                            ),
                            onTap: (){
                              _showDialogInputPajak(_pajakValue.value.text.length == 0 ? 0 : int.parse(_pajakValue.value.text));
                            },
                          ),
                        ),
                        SizedBox(width:10,),
                        Text("%", style: TextStyle(fontSize: 12.0.sp,fontWeight: FontWeight.w700)),
                        ],
                      ),
                    )
                    ],
                  ),
                ),
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      title: Text("Diskon", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                      trailing: Wrap(children: [
                        Icon(Icons.arrow_forward_ios_rounded)
                      ],
                      ),
                      onTap: (){
                        showBottomSheettDiskon();
                      },
                    ),
                    Visibility(child:ListTile(
                      title: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                            color: Colors.blue,
                            boxShadow: [
                              BoxShadow(color: Colors.black12, blurRadius: 1)
                            ]),
                        margin: EdgeInsets.all(5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                        child: Center(child: Text(diskonSelected != null ? diskonSelected.name : "",style: TextStyle(color: Colors.white),),
                        ),
                      ),
                      trailing: Wrap(children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 200,
                          height:SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40.0,
                          padding: EdgeInsets.only(left: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10.0)
                              ),
                              color: Colors.white,
                              border: Border.all(color: Colors.grey)
                          ),
                          child: TextFormField(
                            readOnly: true,
                            controller: _diskonValue,
                            textAlign: TextAlign.end,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '',
                            ),
                            onTap: (){
                              if(diskonSelected.name == "%"){
                                _showDialogInputDiskonPersen(_diskonValue.value.text.length == 0 || _diskonValue.value.text == "null" ? 0 : double.parse(_diskonValue.value.text));
                              }
                              else {
                                _showDialogInputDiskon(_diskonValue.value.text.length == 0 || _diskonValue.value.text == "null" ? 0 : double.parse(_diskonValue.value.text));

                              }
                              },
                          ),
                        ),
                      ],
                      ),
                    ),
                      visible: diskonSelected != null ? true : false,
                    ),
                  ],
                ),
              ],
            ),
      ),

            Container(
              width: MediaQuery.of(context).size.width / 1.2,
              margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
              padding: EdgeInsets.only(
                  top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child:
              ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  ListTile(
                    dense: true,
                    title: Text("Grand Total", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                    trailing: Wrap(children: [
                      Text("Rp.$grandTotal", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                    ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.2,
              margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
              padding: EdgeInsets.only(
                  top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child:  ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      title: Text("Metode Pembayaran", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                      trailing: Wrap(children: [
                        Icon(Icons.arrow_forward_ios_rounded)
                      ],
                      ),
                      onTap: (){
                        showBottomSheetPaymentMethod();
                      },
                    ),
                    Visibility(child:ListTile(
                      title: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                            color: Colors.blue,
                            boxShadow: [
                              BoxShadow(color: Colors.black12, blurRadius: 1)
                            ]),
                        margin: EdgeInsets.all(5.0),
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                        child: Center(child: Text(paymentMethod != null ? paymentMethod.name : "",style: TextStyle(color: Colors.white),),
                        ),
                      ),
                      trailing: Wrap(children: [
                        Container(
                           width: MediaQuery.of(context).size.width - 200,
                          height:SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40.0,
                           padding: EdgeInsets.only(left: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10.0)
                              ),
                              color: Colors.white,
                               border: Border.all(color: Colors.grey)
                          ),
                          child: TextFormField(
                            readOnly: true,
                            controller: _nominal,
                            textAlign: TextAlign.end,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '',
                            ),
                            onTap: (){
                              _showDialogInputNominal(_nominal.value.text.length == 0 || _nominal.value.text == "null" ? 0 : int.parse(_nominal.value.text));
                            },
                          ),
                        ),
                      ],
                      ),
                      onTap: (){
                        showBottomSheetPaymentMethod();
                      },
                    ),
                      visible: paymentMethod != null ? true : false,
                    ),
                  ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.2,
              margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
              padding: EdgeInsets.only(
                  top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child:
              ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  ListTile(
                    dense: true,
                    title: Text("Sisa", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                    trailing: Wrap(children: [
                      Text("Rp.$sisa", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                    ],
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
                child: Container(
                  margin: EdgeInsets.all(20),
                  width: MediaQuery.of(context).size.width,
                  height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 45,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  ),
                  child: Center(
                      child: this.isLoading ? CircularProgressIndicator(strokeWidth: 1,) : Text(
                        "Simpan",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700),
                      )),
                ),
                onTap: this.isLoading ? null : () async {
                  setState(() {
                    List<Detail> listDetail             = [];
                    List<Condition> listCondition       = [] ;
                    List<ServiceItem> listServiceItem   = [];

                    widget.detailOrders.map((e) {
                        print(e.listItem.toSet());
                        //init condition
                        e.listKondisi.map((e) {
                          Condition c = Condition(name: e.name);
                          listCondition.add(c);
                        }).toList();

                        //init service itm
                        e.listItem.map((e) {
                          ServiceItem si = ServiceItem(
                              name: e.name,
                              value: e.value
                          );
                          listServiceItem.add(si);
                        }).toList();

                       Detail d =  Detail(
                        serviceId: int.parse(e.service.id),
                        description: e.description,
                        total: e.total,
                        condition: listCondition,
                        amount: e.amount,
                        serviceItem: listServiceItem,
                         duration: e.service.duration,
                      );
                      listDetail.add(d);
                    }).toList();

                    if(this.paymentMethod == null){
                      showAalert("Warning","message : Pilih metode pembayaran",context);
                    }
                    else {
                      BodyRequestOrder bodyRequest = BodyRequestOrder(
                          outletId: this._loginResponse.data.outletId,
                          userId: this._loginResponse.data.id,
                          customerId: widget.customer.id,
                          orderDate:DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now()),
                          express: widget.isExpress,
                          delivery: widget.isDelivery,
                          subTotal: subTotal.toInt(),
                          taxValue: _pajakValue.value.text,
                          taxAmount: taxAmount,
                          discountType: diskonSelected?.value,
                          discountValue: _diskonValue.value.text,
                          discountAmount: diskonAmount,
                          total: grandTotal.toInt(),
                          paymentMethodId: this.paymentMethod.id,
                          paymentAmount:int.parse(_nominal.value.text),
                          paymentRemaining:sisa.toInt(),
                          paymentStatus: sisa != 0 ? "partialy": "paid",
                          detail:listDetail
                      );

                      print("Body : "+bodyRequest.toJson().toString());

                      var bytesUsername  = utf8.encode(this._loginResponse.data.email); // data being hashed
                      var digestUsername = sha1.convert(bytesUsername);
                      //Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));
                      orderPresenter.doCreateOrder(digestUsername.toString(), this._loginResponse.data.accessSecret, bodyRequest);
                    }
                  });
                }
            ),
          ],
        ),
      ),
    );
  }
  showBottomSheetPaymentMethod() async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: BottomSheetPaymentMethod(),
          );
        }).then((value){
          setState(() {
            this.paymentMethod = value;
          });
        });
  }

  showBottomSheettDiskon() async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: BottomSheetDiskon(),
          );
        }).then((value){
      setState(() {
        this.diskonSelected = value;
        // this._pajakValue.text = "0";

        if(this.diskonSelected.value == "Percent"){
          this._diskonValue.text = ((diskonAmount * 100) / gtSementara).toString();
        }
        else {
         this._diskonValue.text = diskonAmount.toString();
        }
      });
    });
  }

  Widget whenExpress(){
    return  ListView.builder(
        shrinkWrap: true,
        itemCount: this.listDetailOrder.length,
        itemBuilder: (BuildContext context, int index){
          DetailOrder detailOrder = this.listDetailOrder[index];
          return Container(
            padding: EdgeInsets.only(
                top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
            child : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((index + 1).toString() +". "+ detailOrder.service.name+
                    "\n     "+detailOrder.totalPcsOrKg.toString() + " x "+ detailOrder.service.priceExpress),
                Text( detailOrder.total.toString())
              ],
            ),
          );
        }
    );
  }

  Widget whenNotExpress(){
    return ListView.builder(
        shrinkWrap: true,
        itemCount: widget.detailOrders.length,
        itemBuilder: (BuildContext context, int index){
          DetailOrder detailOrder =listDetailOrder[index];
          return Container(
            padding: EdgeInsets.only(
                top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
            child : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((index + 1).toString() +". "+ detailOrder.service.name+
                    "\n     "+detailOrder.totalPcsOrKg.toString() + " x "+ detailOrder.service.price),
                Text( detailOrder.total.toString())
              ],
            ),
          );
        }
    );
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Gagal","message : "+error.replaceAll("Exception:", ""),context);
    });

  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {
    setState(() {
      showAalert(responseBodyOrder.status,"message : "+responseBodyOrder.message.replaceAll("Exception:", ""),context);
      Future.delayed(Duration(seconds: 2)).then((_){
        Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));
      });
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi mmm) {
    // TODO: implement onSuccessListOrder
  }

  setTotal(){
    setState(() {
      subTotal = 0;
      for (var i = 0; i < listDetailOrder.length; i++) {
        subTotal += listDetailOrder[i].total;
      }
      grandTotal += subTotal;
      sisa = grandTotal;
      _nominal.text = "0";
    });

  }

  _showDialogInputDiskon(double diskon) {
    double value = diskon;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: TextFormField(
                        controller: _diskonValue,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '',
                        ),
                      ),
                      ),
                    ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(_diskonValue.value.text);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        _diskonValue.text = value.toString();
        diskonAmount = int.parse(_diskonValue.value.text).toDouble();
      });
    });
  }


  _showDialogInputPajak(int pajak) {
    int value = pajak;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: value == 0 ? null : () {
                        setState((){
                          value = value - 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: value ==  0 ? Colors.grey.withOpacity(0.5) : Colors.green,
                        child: Center(child: Icon(
                          Icons.remove, color: Colors.white, size: 20,),),
                      ),
                    ),
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: Text("$value"),
                      ),
                    ),
                    ),

                    InkWell(
                      onTap: () {
                        setState((){
                          value = value + 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: Colors.green,
                        child: Center(child: Icon(
                          Icons.add, color: Colors.white, size: 20,),),
                      ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(value);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      if(value != null){
        print("value pajak : "+value.toString());
        setState(() {
          _pajakValue.text = value.toString();
          double p = (subTotal * value) / 100;
          gtSementara = (subTotal + p);
          diskonAmount = (gtSementara * double.parse(_diskonValue.value.text)) / 100;
          grandTotal = gtSementara - diskonAmount;
          sisa = grandTotal - double.parse(_nominal.value.text);

        });
      }
    });
  }

  _showDialogInputDiskonPersen(double pajak) {
    double value = pajak;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: value == 0 ? null : () {
                        setState((){
                          value = value - 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: value ==  0 ? Colors.grey.withOpacity(0.5) : Colors.green,
                        child: Center(child: Icon(
                          Icons.remove, color: Colors.white, size: 20,),),
                      ),
                    ),
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: Text("$value"),
                      ),
                    ),
                    ),

                    InkWell(
                      onTap: () {
                        setState((){
                          value = value + 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: Colors.green,
                        child: Center(child: Icon(
                          Icons.add, color: Colors.white, size: 20,),),
                      ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(value);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value diskon : "+value.toString());
      setState(() {
        _diskonValue.text = value.toString();
        diskonAmount = (gtSementara * value) / 100;
        log("disk a : $diskonAmount");
        grandTotal = gtSementara - diskonAmount;
        print("gra : "+grandTotal.toString());
        sisa = grandTotal - double.parse(_nominal.value.text);
      });
    });
  }

  _showDialogInputNominal(int nominal) {
    int value = nominal;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: TextFormField(
                        controller: _nominal,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '',
                        ),
                      ),
                      ),
                    ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(_nominal.value.text);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        _nominal.text = value.toString();
          sisa = (grandTotal - double.parse(_nominal.value.text));
      });
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessFinishOrder
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    // TODO: implement onsSuccessDetailOrder
  }

  @override
  void onSuccessOrderUnpaid(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessOrderUnpaid
  }
}
