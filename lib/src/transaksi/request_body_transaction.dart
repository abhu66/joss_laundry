// To parse this JSON data, do
//
//     final requestBodyTransactions = requestBodyTransactionsFromJson(jsonString);

import 'dart:convert';

RequestBodyTransactions requestBodyTransactionsFromJson(String str) => RequestBodyTransactions.fromJson(json.decode(str));

String requestBodyTransactionsToJson(RequestBodyTransactions data) => json.encode(data.toJson());

class RequestBodyTransactions {
  RequestBodyTransactions({
    this.outletId,
    this.userId,
    this.customerId,
    this.orderDate,
    this.express,
    this.delivery,
    this.subTotal,
    this.taxValue,
    this.taxAmount,
    this.discountType,
    this.discountValue,
    this.discountAmount,
    this.total,
    this.paymentMethodId,
    this.detail,
  });

  int outletId;
  int userId;
  int customerId;
  DateTime orderDate;
  bool express;
  bool delivery;
  int subTotal;
  dynamic taxValue;
  dynamic taxAmount;
  dynamic discountType;
  dynamic discountValue;
  dynamic discountAmount;
  int total;
  int paymentMethodId;
  List<Detail> detail;

  factory RequestBodyTransactions.fromJson(Map<String, dynamic> json) => RequestBodyTransactions(
    outletId: json["outlet_id"],
    userId: json["user_id"],
    customerId: json["customer_id"],
    orderDate: DateTime.parse(json["order_date"]),
    express: json["express"],
    delivery: json["delivery"],
    subTotal: json["sub_total"],
    taxValue: json["tax_value"],
    taxAmount: json["tax_amount"],
    discountType: json["discount_type"],
    discountValue: json["discount_value"],
    discountAmount: json["discount_amount"],
    total: json["total"],
    paymentMethodId: json["payment_method_id"],
    detail: List<Detail>.from(json["detail"].map((x) => Detail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "user_id": userId,
    "customer_id": customerId,
    "order_date": orderDate.toIso8601String(),
    "express": express,
    "delivery": delivery,
    "sub_total": subTotal,
    "tax_value": taxValue,
    "tax_amount": taxAmount,
    "discount_type": discountType,
    "discount_value": discountValue,
    "discount_amount": discountAmount,
    "total": total,
    "payment_method_id": paymentMethodId,
    "detail": List<dynamic>.from(detail.map((x) => x.toJson())),
  };
}

class Detail {
  Detail({
    this.serviceId,
    this.amount,
    this.serviceItem,
    this.condition,
    this.description,
    this.total,
  });

  int serviceId;
  int amount;
  List<ServiceItem> serviceItem;
  List<KondisiBarang> condition;
  String description;
  int total;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
    serviceId: json["service_id"],
    amount: json["amount"],
    serviceItem: json["service_item"] == null ? null : List<ServiceItem>.from(json["service_item"].map((x) => ServiceItem.fromJson(x))),
    condition: List<KondisiBarang>.from(json["condition"].map((x) => KondisiBarang.fromJson(x))),
    description: json["description"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "service_id": serviceId,
    "amount": amount,
    "service_item": serviceItem == null ? null : List<dynamic>.from(serviceItem.map((x) => x.toJson())),
    "condition": List<dynamic>.from(condition.map((x) => x.toJson())),
    "description": description,
    "total": total,
  };
}

class KondisiBarang {
  KondisiBarang({
    this.name,
  });

  String name;

  factory KondisiBarang.fromJson(Map<String, dynamic> json) => KondisiBarang(
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
  };
}

class ServiceItem {
  ServiceItem({
    this.name,
    this.value,
  });

  String name;
  String value;

  factory ServiceItem.fromJson(Map<String, dynamic> json) => ServiceItem(
    name: json["name"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "value": value,
  };
}
