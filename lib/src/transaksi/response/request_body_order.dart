// To parse this JSON data, do
//
//     final bodyRequestOrder = bodyRequestOrderFromJson(jsonString);

import 'dart:convert';

BodyRequestOrder bodyRequestOrderFromJson(String str) => BodyRequestOrder.fromJson(json.decode(str));

String bodyRequestOrderToJson(BodyRequestOrder data) => json.encode(data.toJson());

class BodyRequestOrder {
  BodyRequestOrder({
    this.outletId,
    this.userId,
    this.customerId,
    this.orderDate,
    this.express,
    this.delivery,
    this.subTotal,
    this.taxValue,
    this.taxAmount,
    this.discountType,
    this.discountValue,
    this.discountAmount,
    this.total,
    this.paymentMethodId,
    this.paymentAmount,
    this.paymentRemaining,
    this.paymentStatus,
    this.detail,
  });

  int outletId;
  int userId;
  int customerId;
  String orderDate;
  bool express;
  bool delivery;
  int subTotal;
  dynamic taxValue;
  dynamic taxAmount;
  dynamic discountType;
  dynamic discountValue;
  dynamic discountAmount;
  int total;
  int paymentMethodId;
  int paymentAmount;
  int paymentRemaining;
  String paymentStatus;
  List<Detail> detail;

  factory BodyRequestOrder.fromJson(Map<String, dynamic> json) => BodyRequestOrder(
    outletId: json["outlet_id"],
    userId: json["user_id"],
    customerId: json["customer_id"],
    orderDate:json["order_date"],
    express: json["express"],
    delivery: json["delivery"],
    subTotal: json["sub_total"],
    taxValue: json["tax_value"],
    taxAmount: json["tax_amount"],
    discountType: json["discount_type"],
    discountValue: json["discount_value"],
    discountAmount: json["discount_amount"],
    total: json["total"],
    paymentMethodId: json["payment_method_id"],
    paymentAmount: json["payment_amount"],
    paymentRemaining: json["payment_remaining"],
    paymentStatus: json["payment_status"],
    detail: List<Detail>.from(json["detail"].map((x) => Detail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "outlet_id": outletId,
    "user_id": userId,
    "customer_id": customerId,
    "order_date": orderDate,
    "express": express,
    "delivery": delivery,
    "sub_total": subTotal,
    "tax_value": taxValue,
    "tax_amount": taxAmount,
    "discount_type": discountType,
    "discount_value": discountValue,
    "discount_amount": discountAmount,
    "total": total,
    "payment_method_id": paymentMethodId,
    "payment_amount": paymentAmount,
    "payment_remaining": paymentRemaining,
    "payment_status": paymentStatus,
    "detail": List<dynamic>.from(detail.map((x) => x.toJson())),
  };
}

class Detail {
  Detail({
    this.serviceId,
    this.amount,
    this.serviceItem,
    this.condition,
    this.duration,
    this.description,
    this.total,
  });

  int serviceId;
  int amount;
  List<ServiceItem> serviceItem;
  List<Condition> condition;
  String duration;
  String description;
  int total;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
    serviceId: json["service_id"],
    amount: json["amount"],
    serviceItem: json["service_item"] == null ? null : List<ServiceItem>.from(json["service_item"].map((x) => ServiceItem.fromJson(x))),
    condition: List<Condition>.from(json["condition"].map((x) => Condition.fromJson(x))),
    duration: json["duration"],
    description: json["description"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "service_id": serviceId,
    "amount": amount,
    "service_item": serviceItem == null ? null : List<dynamic>.from(serviceItem.map((x) => x.toJson())),
    "condition": List<dynamic>.from(condition.map((x) => x.toJson())),
    "duration": duration,
    "description": description,
    "total": total,
  };
}

class Condition {
  Condition({
    this.name,
  });

  String name;

  factory Condition.fromJson(Map<String, dynamic> json) => Condition(
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
  };
}

class ServiceItem {
  ServiceItem({
    this.name,
    this.value,
  });

  String name;
  String value;

  factory ServiceItem.fromJson(Map<String, dynamic> json) => ServiceItem(
    name: json["name"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "value": value,
  };
}
