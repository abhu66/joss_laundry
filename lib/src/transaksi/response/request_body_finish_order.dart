// To parse this JSON data, do
//
//     final requestBodyFinishOrder = requestBodyFinishOrderFromJson(jsonString);

import 'dart:convert';

RequestBodyFinishOrder requestBodyFinishOrderFromJson(String str) => RequestBodyFinishOrder.fromJson(json.decode(str));

String requestBodyFinishOrderToJson(RequestBodyFinishOrder data) => json.encode(data.toJson());

class RequestBodyFinishOrder {
  RequestBodyFinishOrder({
    this.orderId,
  });

  List<int> orderId;

  factory RequestBodyFinishOrder.fromJson(Map<String, dynamic> json) => RequestBodyFinishOrder(
    orderId: List<int>.from(json["order_id"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "order_id": List<dynamic>.from(orderId.map((x) => x)),
  };
}
