// To parse this JSON data, do
//
//     final responseBodyListTransaksi = responseBodyListTransaksiFromJson(jsonString);

import 'dart:convert';

import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';

ResponseBodyListTransaksi responseBodyListTransaksiFromJson(String str) => ResponseBodyListTransaksi.fromJson(json.decode(str));

String responseBodyListTransaksiToJson(ResponseBodyListTransaksi data) => json.encode(data.toJson());

class ResponseBodyListTransaksi {
  ResponseBodyListTransaksi({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<DataOrder> data;

  factory ResponseBodyListTransaksi.fromJson(Map<String, dynamic> json) => ResponseBodyListTransaksi(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<DataOrder>.from(json["data"].map((x) => DataOrder.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataOrder {
  DataOrder({
    this.id,
    this.orderNumber,
    this.outletId,
    this.userId,
    this.customerId,
    this.orderDate,
    this.completionDate,
    this.status,
    this.express,
    this.delivery,
    this.deliveryStatus,
    this.deliveryDate,
    this.subTotal,
    this.taxValue,
    this.taxAmount,
    this.discountType,
    this.discountValue,
    this.discountAmount,
    this.total,
    this.paymentMethodId,
    this.paymentStatus,
    this.user,
    this.outlet,
    this.paymentMethod,
    this.customer,
    this.estimationDate
  });

  int id;
  String orderNumber;
  int outletId;
  int userId;
  int customerId;
  DateTime orderDate;
  dynamic completionDate;
  String status;
  bool express;
  bool delivery;
  dynamic deliveryStatus;
  dynamic deliveryDate;
  String subTotal;
  dynamic taxValue;
  dynamic taxAmount;
  dynamic discountType;
  dynamic discountValue;
  dynamic discountAmount;
  String total;
  int paymentMethodId;
  String paymentStatus;
  String user;
  String outlet;
  String paymentMethod;
  Customer customer;
  String estimationDate;

  factory DataOrder.fromJson(Map<String, dynamic> json) => DataOrder(
    id: json["id"],
    orderNumber: json["order_number"],
    outletId: json["outlet_id"],
    userId: json["user_id"],
    customerId: json["customer_id"],
    orderDate: DateTime.parse(json["order_date"]),
    completionDate: json["completion_date"],
    status: json["status"],
    express: json["express"],
    delivery: json["delivery"],
    deliveryStatus: json["delivery_status"],
    deliveryDate: json["delivery_date"],
    subTotal: json["sub_total"],
    taxValue: json["tax_value"],
    taxAmount: json["tax_amount"],
    discountType: json["discount_type"],
    discountValue: json["discount_value"],
    discountAmount: json["discount_amount"],
    total: json["total"],
    paymentMethodId: json["payment_method_id"],
    paymentStatus: json["payment_status"],
    user: json["user"],
    outlet: json["outlet"],
    paymentMethod: json["payment_method"],
    estimationDate: json["estimation_date"],
    customer: Customer.fromJson(json["customer"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "order_number": orderNumber,
    "outlet_id": outletId,
    "user_id": userId,
    "customer_id": customerId,
    "order_date": orderDate.toIso8601String(),
    "completion_date": completionDate,
    "status": status,
    "express": express,
    "delivery": delivery,
    "delivery_status": deliveryStatus,
    "delivery_date": deliveryDate,
    "sub_total": subTotal,
    "tax_value": taxValue,
    "tax_amount": taxAmount,
    "discount_type": discountType,
    "discount_value": discountValue,
    "discount_amount": discountAmount,
    "total": total,
    "payment_method_id": paymentMethodId,
    "payment_status": paymentStatus,
    "user": user,
    "outlet": outlet,
    "estimation_date": estimationDate,
    "payment_method": paymentMethod,
    "customer": customer.toJson(),
  };
}
