// To parse this JSON data, do
//
//     final responseBodyDetailOrder = responseBodyDetailOrderFromJson(jsonString);

import 'dart:convert';

import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/transaksi/response/detail_orders.dart';

ResponseBodyDetailOrder responseBodyDetailOrderFromJson(String str) => ResponseBodyDetailOrder.fromJson(json.decode(str));

String responseBodyDetailOrderToJson(ResponseBodyDetailOrder data) => json.encode(data.toJson());

class ResponseBodyDetailOrder {
  ResponseBodyDetailOrder({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory ResponseBodyDetailOrder.fromJson(Map<String, dynamic> json) => ResponseBodyDetailOrder(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.orderNumber,
    this.outletId,
    this.userId,
    this.customerId,
    this.orderDate,
    this.completionDate,
    this.status,
    this.express,
    this.delivery,
    this.deliveryStatus,
    this.deliveryDate,
    this.subTotal,
    this.taxValue,
    this.taxAmount,
    this.discountType,
    this.discountValue,
    this.discountAmount,
    this.total,
    this.paymentMethodId,
    this.paymentAmount,
    this.paymentRemaining,
    this.paymentStatus,
    this.deposited,
    this.estimationDate,
    this.user,
    this.outlet,
    this.paymentMethod,
    this.customer,
    this.detail,
  });

  int id;
  String orderNumber;
  int outletId;
  int userId;
  int customerId;
  DateTime orderDate;
  dynamic completionDate;
  String status;
  bool express;
  bool delivery;
  dynamic deliveryStatus;
  dynamic deliveryDate;
  String subTotal;
  String taxValue;
  String taxAmount;
  String discountType;
  String discountValue;
  String discountAmount;
  String total;
  int paymentMethodId;
  String paymentAmount;
  String paymentRemaining;
  String paymentStatus;
  bool deposited;
  DateTime estimationDate;
  String user;
  String outlet;
  String paymentMethod;
  Customer customer;
  List<Details> detail;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    orderNumber: json["order_number"],
    outletId: json["outlet_id"],
    userId: json["user_id"],
    customerId: json["customer_id"],
    orderDate: DateTime.parse(json["order_date"]),
    completionDate: json["completion_date"],
    status: json["status"],
    express: json["express"],
    delivery: json["delivery"],
    deliveryStatus: json["delivery_status"],
    deliveryDate: json["delivery_date"],
    subTotal: json["sub_total"],
    taxValue: json["tax_value"],
    taxAmount: json["tax_amount"],
    discountType: json["discount_type"],
    discountValue: json["discount_value"],
    discountAmount: json["discount_amount"],
    total: json["total"],
    paymentMethodId: json["payment_method_id"],
    paymentAmount: json["payment_amount"],
    paymentRemaining: json["payment_remaining"],
    paymentStatus: json["payment_status"],
    deposited: json["deposited"],
    estimationDate: DateTime.parse(json["estimation_date"]),
    user: json["user"],
    outlet: json["outlet"],
    paymentMethod: json["payment_method"],
    customer: Customer.fromJson(json["customer"]),
    detail: List<Details>.from(json["detail"].map((x) => Details.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "order_number": orderNumber,
    "outlet_id": outletId,
    "user_id": userId,
    "customer_id": customerId,
    "order_date": orderDate.toIso8601String(),
    "completion_date": completionDate,
    "status": status,
    "express": express,
    "delivery": delivery,
    "delivery_status": deliveryStatus,
    "delivery_date": deliveryDate,
    "sub_total": subTotal,
    "tax_value": taxValue,
    "tax_amount": taxAmount,
    "discount_type": discountType,
    "discount_value": discountValue,
    "discount_amount": discountAmount,
    "total": total,
    "payment_method_id": paymentMethodId,
    "payment_amount": paymentAmount,
    "payment_remaining": paymentRemaining,
    "payment_status": paymentStatus,
    "deposited": deposited,
    "estimation_date": estimationDate.toIso8601String(),
    "user": user,
    "outlet": outlet,
    "payment_method": paymentMethod,
    "customer": customer.toJson(),
    "detail": List<dynamic>.from(detail.map((x) => x.toJson())),
  };
}



class Details {
  Details({
    this.id,
    this.orderId,
    this.serviceId,
    this.amount,
    this.serviceItem,
    this.condition,
    this.description,
    this.total,
    this.duration,
    this.serviceName,
  });

  int id;
  int orderId;
  int serviceId;
  String amount;
  List<ServiceItems> serviceItem;
  List<ConditionBarang> condition;
  String description;
  String total;
  String duration;
  String serviceName;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    id: json["id"],
    orderId: json["order_id"],
    serviceId: json["service_id"],
    amount: json["amount"],
    serviceItem: List<ServiceItems>.from(json["service_item"].map((x) => ServiceItems.fromJson(x))),
    condition: List<ConditionBarang>.from(json["condition"].map((x) => ConditionBarang.fromJson(x))),
    description: json["description"],
    total: json["total"],
    duration: json["duration"],
    serviceName: json["service_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "order_id": orderId,
    "service_id": serviceId,
    "amount": amount,
    "service_item": List<dynamic>.from(serviceItem.map((x) => x.toJson())),
    "condition": List<dynamic>.from(condition.map((x) => x.toJson())),
    "description": description,
    "total": total,
    "duration": duration,
    "service_name": serviceName,
  };
}
