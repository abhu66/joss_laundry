// To parse this JSON data, do
//
//     final detailsOrder = detailsOrderFromJson(jsonString);

import 'dart:convert';

DetailsOrder detailsOrderFromJson(String str) => DetailsOrder.fromJson(json.decode(str));

String detailsOrderToJson(DetailsOrder data) => json.encode(data.toJson());

class DetailsOrder {
  DetailsOrder({
    this.serviceId,
    this.amount,
    this.serviceItem,
    this.condition,
    this.description,
    this.total,
  });

  int serviceId;
  int amount;
  List<ServiceItems> serviceItem;
  List<ConditionBarang> condition;
  String description;
  int total;

  factory DetailsOrder.fromJson(Map<String, dynamic> json) => DetailsOrder(
    serviceId: json["service_id"],
    amount: json["amount"],
    serviceItem: List<ServiceItems>.from(json["service_item"].map((x) => ServiceItems.fromJson(x))),
    condition: List<ConditionBarang>.from(json["condition"].map((x) => ConditionBarang.fromJson(x))),
    description: json["description"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "service_id": serviceId,
    "amount": amount,
    "service_item": List<dynamic>.from(serviceItem.map((x) => x.toJson())),
    "condition": List<dynamic>.from(condition.map((x) => x.toJson())),
    "description": description,
    "total": total,
  };
}

class ConditionBarang {
  ConditionBarang({
    this.name,
  });

  String name;

  factory ConditionBarang.fromJson(Map<String, dynamic> json) => ConditionBarang(
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
  };
}

class ServiceItems {
  ServiceItems({
    this.name,
    this.value,
  });

  String name;
  String value;

  factory ServiceItems.fromJson(Map<String, dynamic> json) => ServiceItems(
    name: json["name"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "value": value,
  };
}
