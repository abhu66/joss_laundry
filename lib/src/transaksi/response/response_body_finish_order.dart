// To parse this JSON data, do
//
//     final responseBodyFinishOrder = responseBodyFinishOrderFromJson(jsonString);

import 'dart:convert';

ResponseBodyFinishOrder responseBodyFinishOrderFromJson(String str) => ResponseBodyFinishOrder.fromJson(json.decode(str));

String responseBodyFinishOrderToJson(ResponseBodyFinishOrder data) => json.encode(data.toJson());

class ResponseBodyFinishOrder {
  ResponseBodyFinishOrder({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  List<DataOrderFinish> data;

  factory ResponseBodyFinishOrder.fromJson(Map<String, dynamic> json) => ResponseBodyFinishOrder(
    status: json["status"],
    message: json["message"],
    data: List<DataOrderFinish>.from(json["data"].map((x) => DataOrderFinish.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataOrderFinish {
  DataOrderFinish({
    this.orderNumber,
    this.completionDate,
    this.status,
  });

  String orderNumber;
  DateTime completionDate;
  String status;

  factory DataOrderFinish.fromJson(Map<String, dynamic> json) => DataOrderFinish(
    orderNumber: json["order_number"],
    completionDate: DateTime.parse(json["completion_date"]),
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "order_number": orderNumber,
    "completion_date": completionDate.toIso8601String(),
    "status": status,
  };
}
