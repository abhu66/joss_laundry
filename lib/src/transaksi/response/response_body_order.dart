// To parse this JSON data, do
//
//     final responseBodyOrder = responseBodyOrderFromJson(jsonString);

import 'dart:convert';

ResponseBodyOrder responseBodyOrderFromJson(String str) => ResponseBodyOrder.fromJson(json.decode(str));

String responseBodyOrderToJson(ResponseBodyOrder data) => json.encode(data.toJson());

class ResponseBodyOrder {
  ResponseBodyOrder({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory ResponseBodyOrder.fromJson(Map<String, dynamic> json) => ResponseBodyOrder(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.orderNumber,
    this.outletId,
    this.userId,
    this.customerId,
    this.orderDate,
    this.status,
    this.express,
    this.delivery,
    this.subTotal,
    this.taxValue,
    this.taxAmount,
    this.discountType,
    this.discountValue,
    this.discountAmount,
    this.total,
    this.paymentMethodId,
    this.id,
    this.user,
    this.outlet,
    this.paymentMethod,
    this.estimationDate
  });

  String orderNumber;
  int outletId;
  int userId;
  int customerId;
  DateTime orderDate;
  String status;
  bool express;
  bool delivery;
  int subTotal;
  dynamic taxValue;
  dynamic taxAmount;
  dynamic discountType;
  dynamic discountValue;
  dynamic discountAmount;
  int total;
  int paymentMethodId;
  int id;
  String user;
  String outlet;
  String paymentMethod;
  DateTime estimationDate;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    orderNumber: json["order_number"],
    outletId: json["outlet_id"],
    userId: json["user_id"],
    customerId: json["customer_id"],
    orderDate: DateTime.parse(json["order_date"]),
    status: json["status"],
    express: json["express"],
    delivery: json["delivery"],
    subTotal: json["sub_total"],
    taxValue: json["tax_value"],
    taxAmount: json["tax_amount"],
    discountType: json["discount_type"],
    discountValue: json["discount_value"],
    discountAmount: json["discount_amount"],
    total: json["total"],
    paymentMethodId: json["payment_method_id"],
    id: json["id"],
    user: json["user"],
    outlet: json["outlet"],
    paymentMethod: json["payment_method"],
    estimationDate: DateTime.parse(json["estimation_date"]),
  );

  Map<String, dynamic> toJson() => {
    "order_number": orderNumber,
    "outlet_id": outletId,
    "user_id": userId,
    "customer_id": customerId,
    "order_date": orderDate.toIso8601String(),
    "status": status,
    "express": express,
    "delivery": delivery,
    "sub_total": subTotal,
    "tax_value": taxValue,
    "tax_amount": taxAmount,
    "discount_type": discountType,
    "discount_value": discountValue,
    "discount_amount": discountAmount,
    "total": total,
    "payment_method_id": paymentMethodId,
    "id": id,
    "user": user,
    "outlet": outlet,
    "payment_method": paymentMethod,
    "estimation_date": estimationDate
  };
}
