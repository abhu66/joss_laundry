import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/payment/bottom_sheet_diskon.dart';
import 'package:joss_laundry/src/payment/bottom_sheet_payment_method.dart';
import 'package:joss_laundry/src/payment/response_payment_method.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:joss_laundry/src/transaksi/rincian_transaksi_next_page.dart';
import 'package:joss_laundry/src/transaksi/transaksi_page.dart';
import 'package:sizer/sizer.dart';

class RincianTranskasiPage extends StatefulWidget {
  final List<DetailOrder> detailOrders;
  final Customer customer;
  RincianTranskasiPage({this.detailOrders,this.customer});

  @override
  _RincianTranskasiPageState createState() => _RincianTranskasiPageState();
}

class _RincianTranskasiPageState extends State<RincianTranskasiPage> implements OrderContract {

  TextEditingController _nominal      = TextEditingController();
  TextEditingController _diskonValue  = TextEditingController();
  TextEditingController _pajakValue   = TextEditingController();
  bool isLoading = false;
  bool _switchVal1 = true;
  bool _switchVal2 = true;
  Datum paymentMethod;
  LoginResponse _loginResponse;
  OrderPresenter orderPresenter;
  bool isExpress  = false;
  bool isDelivery = false;
  double grandTotal = 0;
  int subTotal   = 0;
  int sisa       = 0;
  Diskon diskonSelected;
  int diskonValue = 0;
  var sum = 0;
  List<DetailOrder> listDetailOrder = [];



  @override
  void initState(){
    super.initState();
    orderPresenter = OrderPresenter(this);
    orderPresenter.loadSharedPrefs();
    listDetailOrder.addAll(widget.detailOrders);
    //subTotal = sum;
    setSubTotal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Rincian Transaksi",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
                width: MediaQuery.of(context).size.width / 1.2,
                margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
              child:isExpress ? whenExpress() : whenNotExpress(),

            ),
      Container(
        width: MediaQuery.of(context).size.width / 1.2,
        margin: EdgeInsets.only(top: 16, right: 10.0, left: 10.0),
        padding: EdgeInsets.only(
            top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child:
            ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ListTile(
                  title: Text("Tarif Express",style: TextStyle(fontSize: 10.0.sp)),
                  trailing: Wrap(children: [
                    Switch(value: this.isExpress,onChanged: (bool value) {
                      setState(() {
                        this.isExpress = value;
                      });
                    }),
                   ],
                  ),
                ),
                Divider(color: Colors.grey),
                ListTile(
                  title: Text("Pengantaran", style: TextStyle(fontSize: 10.0.sp)),
                  trailing: Wrap(children: [
                    Switch(value: this.isDelivery,onChanged: (bool value) {
                      setState(() => this.isDelivery = value);
                    })
                  ],
                  ),
                ),
                Divider(color: Colors.grey),
                ListTile(
                  dense: true,
                  title: Text("Sub Total", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                  trailing: Wrap(children: [
                    Text("Rp.$subTotal", style: TextStyle(fontSize: 10.0.sp,fontWeight: FontWeight.w700)),
                  ],
                  ),
                ),
            InkWell(
                child: Container(
                  margin: EdgeInsets.all(20),
                  width: MediaQuery.of(context).size.width,
                  height: SizerUtil.deviceType == DeviceType.Tablet ? 50 : 45,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  ),
                  child: Center(
                      child: this.isLoading ? CircularProgressIndicator(strokeWidth: 1,) : Text(
                        "Lanjutkan",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700),
                      )),
                ),
                onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => RincianTranskasiNextPage(
                          detailOrders: this.listDetailOrder,
                          customer: widget.customer,
                            isExpress: this.isExpress,
                          isDelivery: this.isDelivery,
                          ),
                    ));
                },
            ),
          ],
        ),
      ),
      ],
    ),
      ),
    );
  }
  showBottomSheetPaymentMethod() async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: BottomSheetPaymentMethod(),
          );
        }).then((value){
          setState(() {
            this.paymentMethod = value;
          });
        });
  }

  showBottomSheettDiskon() async {
    return await showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(top: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Colors.grey.withOpacity(0.6)),
              color: Colors.white,
            ),
            child: BottomSheetDiskon(),
          );
        }).then((value){
      setState(() {
        this.diskonSelected = value;
      });
    });
  }

  Widget whenExpress(){
    return  ListView.builder(
        shrinkWrap: true,
        itemCount: this.listDetailOrder.length,
        itemBuilder: (BuildContext context, int index){
          DetailOrder detailOrder = this.listDetailOrder[index];
          detailOrder.total = detailOrder.totalPcsOrKg * int.parse(detailOrder.service.priceExpress);
          this.listDetailOrder[this.listDetailOrder.indexWhere((element) => element == detailOrder)] = detailOrder;
          Future.delayed(Duration.zero, () async {
            setState(() {
              setSubTotal();
            });
          });
          return Container(
            padding: EdgeInsets.only(
                top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
            child : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((index + 1).toString() +". "+ detailOrder.service.name+
                    "\n     "+detailOrder.totalPcsOrKg.toString() + " x "+ detailOrder.service.priceExpress),
                Text( detailOrder.total.toString())
              ],
            ),
          );
        }
    );
  }

  Widget whenNotExpress(){
    return ListView.builder(
        shrinkWrap: true,
        itemCount: widget.detailOrders.length,
        itemBuilder: (BuildContext context, int index){
          DetailOrder detailOrder =listDetailOrder[index];
          detailOrder.total = detailOrder.totalPcsOrKg * int.parse(detailOrder.service.price);
          listDetailOrder[listDetailOrder.indexWhere((element) => element == detailOrder)] = detailOrder;
          Future.delayed(Duration.zero, () async {
            setSubTotal();
          });
          return Container(
            padding: EdgeInsets.only(
                top: 4.0.sp, left: 5.0.sp, right: 5.0.sp, bottom: 4.0.sp),
            child : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((index + 1).toString() +". "+ detailOrder.service.name+
                    "\n     "+detailOrder.totalPcsOrKg.toString() + " x "+ detailOrder.service.price),
                Text( detailOrder.total.toString())
              ],
            ),
          );
        }
    );
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Login Gagal","message : "+error.replaceAll("Exception:", ""),context);
    });

  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {
    setState(() {
      showAalert(responseBodyOrder.status,"message : "+responseBodyOrder.message.replaceAll("Exception:", ""),context);
      Future.delayed(Duration(seconds: 2)).then((_){
        Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));
      });
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi mmm) {
    // TODO: implement onSuccessListOrder
  }

  setSubTotal(){
    setState(() {
      if(isExpress){
        subTotal = 0;
        for (var i = 0; i < listDetailOrder.length; i++) {
          subTotal += listDetailOrder[i].total;
        }
      }
      else {
        subTotal = 0;
        for (var i = 0; i < listDetailOrder.length; i++) {
          subTotal += listDetailOrder[i].total;
        }
      }
    });

  }

  _showDialogInputDiskon(int diskon) {
    int value = diskon;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: TextFormField(
                        controller: _diskonValue,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '',
                        ),
                      ),
                      ),
                    ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(_diskonValue.value.text);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        _diskonValue.text = value.toString();
        grandTotal +=  subTotal;
        grandTotal -= int.parse(_diskonValue.value.text).toDouble();
        print("gra : "+grandTotal.toString());
      });
    });
  }


  _showDialogInputPajak(int pajak) {
    int value = pajak;
    return showDialog(
        context: context,
        builder: (BuildContext contexts) {
          return StatefulBuilder(builder: (context, StateSetter setState){
            return AlertDialog(
              content: Container(
                child: Center(child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: value == 0 ? null : () {
                        setState((){
                          value = value - 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: value ==  0 ? Colors.grey.withOpacity(0.5) : Colors.green,
                        child: Center(child: Icon(
                          Icons.remove, color: Colors.white, size: 20,),),
                      ),
                    ),
                    Flexible(child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black12, blurRadius: 1)
                          ]),
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(5),
                      child: Center(child: Text("$value"),
                      ),
                    ),
                    ),

                    InkWell(
                      onTap: () {
                        setState((){
                          value = value + 1;
                        });
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        color: Colors.green,
                        child: Center(child: Icon(
                          Icons.add, color: Colors.white, size: 20,),),
                      ),
                    ),
                  ],
                ),
                ),
                height: 70,
              ),
              actions: [
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(0),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () async {
                      Navigator.of(context).pop(value);
                    }
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(contexts).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Batal",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      Navigator.of(contexts).pop();
                    }
                ),
              ],
            );
          });
        }
    ).then((value){
      print("value : "+value.toString());
      setState(() {
        _pajakValue.text = value.toString();
        double p = (grandTotal * value) / 100;
        grandTotal += p;
        print("value : "+grandTotal.toString());
      });
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessFinishOrder
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    // TODO: implement onsSuccessDetailOrder
  }

  @override
  void onSuccessOrderUnpaid(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessOrderUnpaid
  }
}
