import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/transaksi/api/order_api.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';

abstract class OrderContract{
  void onLoading(bool isLoading);
  void onSuccessSharedPref(LoginResponse response);
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder);
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder);
  void onSuccessListOrder(ResponseBodyListTransaksi responseBodyListTransaksi);
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder);
  void onFailure(String error);
}

class OrderPresenter{
  OrderApi api = OrderApi();
  SharedPref sharedPref = SharedPref();
  OrderContract contract;
  OrderPresenter(this.contract);

  loadSharedPrefs() async{
    contract.onLoading(true);
    LoginResponse data = await sharedPref.getDataLogin();
    if(data != null){
      contract.onLoading(false);
      contract.onSuccessSharedPref(data);
    }
    else {
      contract.onLoading(false);
      contract.onFailure("Terjadi kesalahan !");
    }
  }

  doCreateOrder(String key,String secretKey,BodyRequestOrder bodyRequestOrder) async {
    contract.onLoading(true);
    api.createOrder(key: key,secretKey: secretKey,bodyRequestOrder: bodyRequestOrder).then((ResponseBodyOrder responseBodyOrder){
      contract.onLoading(false);
      contract.onSuccessOrder(responseBodyOrder);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetListTransactionByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListTransactionnByOutletId(key: key,secretKey: secretKey,outletId: outletId).then((ResponseBodyListTransaksi responseBodyListTransaksi){
      contract.onLoading(false);
      contract.onSuccessListOrder(responseBodyListTransaksi);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doFinishOrder(String key,String secretKey,RequestBodyFinishOrder requestBodyFinishOrder) async {
    contract.onLoading(true);
    api.finishOrder(key: key,secretKey: secretKey,requestBodyFinishOrder:requestBodyFinishOrder).then((ResponseBodyFinishOrder responseBodyFinishOrder){
      contract.onLoading(false);
      contract.onSuccessFinishOrder(responseBodyFinishOrder);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }
  doGetDetailByOrderId(String key,String secretKey,dynamic orderId) async {
    contract.onLoading(true);
    api.detDetailOrder(key: key,secretKey: secretKey,orderId: orderId).then((ResponseBodyDetailOrder responseBodyDetailOrder){
      contract.onLoading(false);
      contract.onsSuccessDetailOrder(responseBodyDetailOrder);
    }).catchError((error){
      print("eee "+error.toString());
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }
  doGetListTransactionUnpaidByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListUnpaid(key: key,secretKey: secretKey,outletId: outletId).then((ResponseBodyListTransaksi responseBodyListTransaksi){
      contract.onLoading(false);
      contract.onSuccessListOrder(responseBodyListTransaksi);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }
}