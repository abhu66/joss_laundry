import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/payment/payment_method_api.dart';
import 'package:joss_laundry/src/payment/response_payment_method.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_api.dart';
import 'package:joss_laundry/src/pelanggan/request_body_pelanggan.dart';
import 'package:joss_laundry/src/pelanggan/response_body_pelanggan_baru.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';

abstract class PaymentMethodContract{
  void onPaymentMethodLoading(bool isLoading);
  void onSuccessListPaymentMethod(ResponseBodyPaymentMethod responseBodyPaymentMethod);
  void onSuccessSharedPref(LoginResponse response);
  void onFailurePaymentMethod(String error);
}

class PaymentMethodPresenter{
  PaymentMethodApi api = PaymentMethodApi();
  SharedPref sharedPref = SharedPref();
  PaymentMethodContract contract;
  PaymentMethodPresenter(this.contract);

  loadSharedPrefs() async{
    contract.onPaymentMethodLoading(true);
    LoginResponse data = await sharedPref.getDataLogin();
    if(data != null){
      contract.onPaymentMethodLoading(false);
      contract.onSuccessSharedPref(data);
    }
    else {
      contract.onPaymentMethodLoading(false);
      contract.onFailurePaymentMethod("Terjadi kesalahan !");
    }
  }

  doGetListPaymentMethodByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onPaymentMethodLoading(true);
    api.getListPaymentMethod(key: key,secretKey: secretKey,outletId: outletId).then((ResponseBodyPaymentMethod responseBodyPaymentMethod){
      contract.onPaymentMethodLoading(false);
      contract.onSuccessListPaymentMethod(responseBodyPaymentMethod);
    }).catchError((error){
      contract.onPaymentMethodLoading(false);
      contract.onFailurePaymentMethod(error.toString());
    });
  }
}