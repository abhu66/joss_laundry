// To parse this JSON data, do
//
//     final responseBodyPaymentMethod = responseBodyPaymentMethodFromJson(jsonString);

import 'dart:convert';

ResponseBodyPaymentMethod responseBodyPaymentMethodFromJson(String str) => ResponseBodyPaymentMethod.fromJson(json.decode(str));

String responseBodyPaymentMethodToJson(ResponseBodyPaymentMethod data) => json.encode(data.toJson());

class ResponseBodyPaymentMethod {
  ResponseBodyPaymentMethod({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<Datum> data;

  factory ResponseBodyPaymentMethod.fromJson(Map<String, dynamic> json) => ResponseBodyPaymentMethod(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.organizationId,
    this.name,
    this.description,
  });

  int id;
  int organizationId;
  String name;
  String description;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    organizationId: json["organization_id"],
    name: json["name"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organization_id": organizationId,
    "name": name,
    "description": description,
  };
}
