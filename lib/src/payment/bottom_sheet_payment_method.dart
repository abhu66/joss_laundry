
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/payment/payment_method_presenter.dart';
import 'package:joss_laundry/src/payment/response_payment_method.dart';
import 'package:sizer/sizer.dart';

class BottomSheetPaymentMethod extends StatefulWidget {
  @override
  _State createState() => _State();
}


class _State extends State<BottomSheetPaymentMethod> implements PaymentMethodContract{
  int selectedIndex;
  PaymentMethodPresenter _methodPresenter;
  bool isLoading      = true;
  LoginResponse      _loginResponse;
  List<Datum> listPaymentMethod = [];

  @override
  void initState() {
    _methodPresenter = PaymentMethodPresenter(this);
    _methodPresenter.loadSharedPrefs();
    super.initState();
  }
  setSelectedIndex(int index){
    setState(() {
      this.selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    margin: EdgeInsets.only(left: 10, bottom: 10, top: 10, right: 10),
                    child: Text("Pilih Metode Pembayaran",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                      textAlign: TextAlign.left,
                    ),
                  )
                ]),
                Expanded(
                  child: SingleChildScrollView(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: this.listPaymentMethod.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        Datum items =this.listPaymentMethod[index];
                        return _buildListView(items, index);
                      },
                    ),
                  ),
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      setState(() {
                        Navigator.of(context).pop(listPaymentMethod[selectedIndex]);
                      });

                    }
                ),
                SizedBox(height: 30,),
              ])),
    );
  }

  Widget _buildListView(Datum item, int index) {
    return InkWell(child : Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Visibility(
              visible: selectedIndex == index ? true : false,
              child:Icon(Icons.check_circle_rounded,color: Colors.blue,)
          ),
          Flexible(child:Container(
            height: 40,
            margin: EdgeInsets.only(left:5,top: 0,right: 5),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(10),
            child: Text(item?.name, style: TextStyle(color: Colors.black),),
            decoration: BoxDecoration(
                color:  Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border:
                Border.all(color: Colors.grey.withOpacity(0.5))),
          ),
          ),
        ],
      ),
    ),
      onTap: (){
        setSelectedIndex(index);
      },
    );
  }

  @override
  void onFailurePaymentMethod(String error) {
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onPaymentMethodLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListPaymentMethod(ResponseBodyPaymentMethod responseBodyPaymentMethod) {
     setState(() {
       this.listPaymentMethod = responseBodyPaymentMethod.data;
     });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _methodPresenter.doGetListPaymentMethodByOutletId(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString());
    });
  }
}
