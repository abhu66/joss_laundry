import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/payment/response_payment_method.dart';

class PaymentMethodApi{
  final listPaymentMethodUrl = "/payment_method/list/";
  Config config  = new Config();

  Future<ResponseBodyPaymentMethod> getListPaymentMethod({String key,String secretKey,dynamic outletId}) async {
    return ResponseBodyPaymentMethod.fromJson(
        await config.doGets(endpoint:config.baseUrl + listPaymentMethodUrl+outletId,
            key: key,
            secretKey: secretKey));
  }
}