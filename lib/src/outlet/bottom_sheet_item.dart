
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:joss_laundry/src/transaksi/request_body_transaction.dart';
import 'package:sizer/sizer.dart';

class BottomSheetItem extends StatefulWidget {
  final  dynamic listItem;
  BottomSheetItem({this.listItem});

  @override
  _State createState() => _State();
}


class _State extends State<BottomSheetItem> {
  int selectedIndex;
  List<ServiceItem> listServiceitem = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      print("dddd : "+widget.listItem.length.toString());
      widget.listItem.map((e){
        listServiceitem.add(ServiceItem(name: e['name'], value: "1",));
      }).toList();
    });

  }
  setSelectedIndex(int index){
    setState(() {
      this.selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(left: 10, bottom: 10, top: 10, right: 10),
                          child: Text("Pilih Item",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                            textAlign: TextAlign.left,
                          ),
                        )
                      ]),
                Expanded(
                  child: SingleChildScrollView(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: this.listServiceitem.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        ServiceItem items =this.listServiceitem[index];
                        return _buildListView(items, index);
                      },
                    ),
                  ),
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      setState(() {
                        Navigator.of(context).pop(listServiceitem[selectedIndex]);
                      });

                    }
                ),
                SizedBox(height: 30,),
              ])),
    );
  }

  Widget _buildListView(ServiceItem item, int index) {
    return InkWell(child : Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Visibility(
              visible: selectedIndex == index ? true : false,
              child:Icon(Icons.check_circle_rounded,color: Colors.blue,)
          ),
          Flexible(child:Container(
            height: 40,
            margin: EdgeInsets.only(left:5,top: 0,right: 5),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(10),
            child: Text(item?.name, style: TextStyle(color: Colors.black),),
            decoration: BoxDecoration(
                color:  Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border:
                Border.all(color: Colors.grey.withOpacity(0.5))),
              ),
          ),
           ],
        ),
    ),
      onTap: (){
          setSelectedIndex(index);
      },
    );
  }
}
