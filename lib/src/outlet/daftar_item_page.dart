import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/outlet_presenter.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_items_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:sizer/sizer.dart';

class DaftarItemPage extends StatefulWidget {
  final bool isFromTransaction;
  DaftarItemPage({this.isFromTransaction});

  @override
  _State createState() => _State();
}

class _State extends State<DaftarItemPage> implements OutletContract {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  OutletPresenter _outletPresenter;
  LoginResponse _loginResponse;
  bool isLoading = true;
  List<Items> listItems = [];

  @override
  void initState() {
    super.initState();
    _outletPresenter = OutletPresenter(this);
    _outletPresenter.loadSharedPrefs();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Daftar Item",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0, // untuk membuat garis dibawah appBar
        iconTheme: IconThemeData(
            color: Colors.black
        ),
      ),
      body: this.isLoading ? Center(child: CircularProgressIndicator(strokeWidth: 1,),)
      : Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: this.listItems.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        Items items = this.listItems[index];
                        return _buildListView(items, index);
                      },
                    ),
                  ),
                ),
              ]
          )
      ),
    );
  }

  Widget _buildListView(Items items, int index) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: ListTile(
        title: Text(
          "Nama Item : "+items.name,
          style: TextStyle(color: Colors.black, fontSize: 12.0.sp,fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          "\nDescription : ${items.description}",
          style: TextStyle(color: Colors.black, fontSize: 10.0.sp),
        ),
        // onTap: !widget.isFromTransaction ? null : (){
        //   Navigator.of(context).pop(items);
        // },
      ),
    );
  }


  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListCondition(ResponseConditionByOutlet responseConditionByOutlet) {
    // TODO: implement onSuccessListCondition
  }

  @override
  void onSuccessListServices(ResponseServicesByOutlet listServicesResponse) {
    // TODO: implement onSuccessListServices
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _outletPresenter.doGetListItemByOutletId(digestUsername.toString(),
          response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessListItem(ResponseItemByOutlet responseItemByOutlet) {
    // TODO: implement onSuccessListItem
    setState(() {
      this.listItems = responseItemByOutlet.data;
    });
  }
}
