import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/const/const_image.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/outlet_presenter.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_items_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:sizer/sizer.dart';

class InformasiOutletPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<InformasiOutletPage> implements OutletContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  OutletPresenter _outletPresenter;
  LoginResponse _loginResponse;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _outletPresenter = OutletPresenter(this);
    _outletPresenter.loadSharedPrefs();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi Outlet", style: TextStyle(color: Colors.black, fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.0, // untuk membuat garis dibawah appBar
        iconTheme: IconThemeData(
            color: Colors.black
        ),
      ),
      body: this.isLoading ? Center(child: CircularProgressIndicator(strokeWidth: 1))
          : SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width, // Lebar
              height: SizerUtil.deviceType == DeviceType.Tablet ? 200 : 200,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  SizedBox(height: 10.0,),
                  CircleAvatar(
                    backgroundColor: Colors.greenAccent,
                    backgroundImage:AssetImage(ConstImages.IC_JOSS_LAUNDRY),
                    radius: 50,
                  ),
                  SizedBox(height: 20.0,),
                  Center(
                      child: Text(
                        "JOSS LAUNDRY ${_loginResponse.data.outlet.name.toUpperCase()}",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0.sp),
                      ),
                    ),
                ],
              ),
              ),
            Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.black12, blurRadius: 1)
                    ]),
                margin: EdgeInsets.all(10.0),
                padding: EdgeInsets.all(5),
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      title: Text("Nama :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(this._loginResponse.data.outlet.name),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("No. Telepon :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(this._loginResponse.data.outlet.phone),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Alamat :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(this._loginResponse.data.outlet.address),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Provinsi :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(this._loginResponse.data.outlet.province),
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Kota :", style: TextStyle(fontSize: 10.0.sp)),
                      subtitle: Text(this._loginResponse.data.outlet.city),
                    ),
                  ],
                )),
            SizedBox(
              height: 50.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListCondition(ResponseConditionByOutlet responseConditionByOutlet) {
    // TODO: implement onSuccessListCondition
  }

  @override
  void onSuccessListServices(ResponseServicesByOutlet listServicesResponse) {
    // TODO: implement onSuccessListServices
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this._loginResponse = response;
    });
  }

  @override
  void onSuccessListItem(ResponseItemByOutlet responseItemByOutlet) {
    // TODO: implement onSuccessListItem
  }
}
