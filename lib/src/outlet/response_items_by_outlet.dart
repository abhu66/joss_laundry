// To parse this JSON data, do
//
//     final responseItemByOutlet = responseItemByOutletFromJson(jsonString);

import 'dart:convert';

ResponseItemByOutlet responseItemByOutletFromJson(String str) => ResponseItemByOutlet.fromJson(json.decode(str));

String responseItemByOutletToJson(ResponseItemByOutlet data) => json.encode(data.toJson());

class ResponseItemByOutlet {
  ResponseItemByOutlet({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<Items> data;

  factory ResponseItemByOutlet.fromJson(Map<String, dynamic> json) => ResponseItemByOutlet(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<Items>.from(json["data"].map((x) => Items.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Items {
  Items({
    this.id,
    this.organizationId,
    this.name,
    this.description,
  });

  int id;
  int organizationId;
  String name;
  String description;

  factory Items.fromJson(Map<String, dynamic> json) => Items(
    id: json["id"],
    organizationId: json["organization_id"],
    name: json["name"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organization_id": organizationId,
    "name": name,
    "description": description,
  };
}
