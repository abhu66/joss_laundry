import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/outlet/daftar_item_page.dart';
import 'package:joss_laundry/src/outlet/daftar_kondisi_page.dart';
import 'package:joss_laundry/src/outlet/daftar_layanan_page.dart';
import 'package:joss_laundry/src/outlet/informasi_outlet_page.dart';
import 'package:sizer/sizer.dart';

class InfoOutletPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<InfoOutletPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Info Outlet",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0, // untuk membuat garis dibawah appBar
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width, // Lebar
              height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(5),
              child: Center(
                child: Text(
                  "JOSS LAUNDRY",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0.sp),
                ),
              ),
            ),
            Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.black12, blurRadius: 1)
                    ]),
                margin: EdgeInsets.all(10.0),
                padding: EdgeInsets.all(5),
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      title: Text("Informasi Outlet",
                          style: TextStyle(fontSize: 10.0.sp)),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => InformasiOutletPage()));
                      },
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Daftar Layanan",
                          style: TextStyle(fontSize: 10.0.sp)),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DaftarLayananPage(isFromTransaction: false,)));
                      },
                    ),
                    Divider(color: Colors.grey),
                    ListTile(
                      title: Text("Daftar Item",
                          style: TextStyle(fontSize: 10.0.sp)),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DaftarItemPage()));
                      },
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    ListTile(
                      title: Text("Daftar Kondisi",
                          style: TextStyle(fontSize: 10.0.sp)),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DaftarKondisiPage()));
                      },
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
