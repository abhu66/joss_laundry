// To parse this JSON data, do
//
//     final responseConditionByOutlet = responseConditionByOutletFromJson(jsonString);

import 'dart:convert';

ResponseConditionByOutlet responseConditionByOutletFromJson(String str) => ResponseConditionByOutlet.fromJson(json.decode(str));

String responseConditionByOutletToJson(ResponseConditionByOutlet data) => json.encode(data.toJson());

class ResponseConditionByOutlet {
  ResponseConditionByOutlet({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<Condition> data;

  factory ResponseConditionByOutlet.fromJson(Map<String, dynamic> json) => ResponseConditionByOutlet(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<Condition>.from(json["data"].map((x) => Condition.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Condition {
  Condition({
    this.id,
    this.organizationId,
    this.name,
    this.description,
  });

  int id;
  int organizationId;
  String name;
  String description;

  factory Condition.fromJson(Map<String, dynamic> json) => Condition(
    id: json["id"],
    organizationId: json["organization_id"],
    name: json["name"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organization_id": organizationId,
    "name": name,
    "description": description,
  };
}
