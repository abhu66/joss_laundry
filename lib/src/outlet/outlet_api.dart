import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_items_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';

class OutletApi{
  final listServicesByOutletUrl = "/service/list/outlet/";
  final listConditionByOutletUrl = "/condition/list/";
  final listItemByOutletUrl = "service/item/list/";
  Config config  = new Config();

  Future<ResponseServicesByOutlet> getListServiceByOutletId({String key,String secretKey,dynamic outletId}) async {
    return ResponseServicesByOutlet.fromJson(
        await config.doGets(endpoint:config.baseUrl + listServicesByOutletUrl+outletId,
            key: key,
            secretKey: secretKey));
  }

  Future<ResponseConditionByOutlet> getListConditionByOutletId({String key,String secretKey,dynamic outletId}) async {
    return ResponseConditionByOutlet.fromJson(
        await config.doGets(endpoint:config.baseUrl + listConditionByOutletUrl+outletId,
            key: key,
            secretKey: secretKey));
  }

  Future<ResponseItemByOutlet> getListItemByOutletId({String key,String secretKey,dynamic outletId}) async {
    return ResponseItemByOutlet.fromJson(
        await config.doGets(endpoint:config.baseUrl + listItemByOutletUrl+outletId,
            key: key,
            secretKey: secretKey));
  }
}