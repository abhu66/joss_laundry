import 'dart:collection';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/outlet_presenter.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_items_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:sizer/sizer.dart';

class BottomSheetKondisiBarang extends StatefulWidget {
  final List<Condition> listHasSelected;
  BottomSheetKondisiBarang({this.listHasSelected});

  @override
  _State createState() => _State();
}


class _State extends State<BottomSheetKondisiBarang> implements OutletContract {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  OutletPresenter _outletPresenter;
  LoginResponse _loginResponse;
  bool isLoading = true;
  List<Condition> listCondition = [];
  List<int> listSelectedCondition = [];
  List<Condition> listConditionSelected = [];
  int selectedIndex;
  List<Condition> listKondisiDipilih = [];

  @override
  void initState() {
    super.initState();
    _outletPresenter = OutletPresenter(this);
    _outletPresenter.loadSharedPrefs();
    this.listKondisiDipilih = widget.listHasSelected;
  }


  setSelectedIndex(int index, Condition condition){
    setState(() {
      if(this.listKondisiDipilih.where((element) => element.name == condition.name).isEmpty){
        this.listKondisiDipilih.add(condition);
      }
      else {
        this.listKondisiDipilih.removeWhere((element) => condition.name == element.name);

      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this.isLoading
          ? Center(child: CircularProgressIndicator(strokeWidth: 1))
          : Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(left: 10, bottom: 10, top: 10, right: 10),
                          child: Text("Pilih Kondisi Barang",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0.sp),
                            textAlign: TextAlign.left,
                          ),
                        )
                      ]),
                Expanded(
                  child: SingleChildScrollView(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: this.listCondition.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        Condition condition = this.listCondition[index];
                        return _buildListView(condition, index);
                      },
                    ),
                  ),
                ),
                InkWell(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 40,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Center(
                          child: Text(
                            "Ok",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.w700),
                          )),
                    ),
                    onTap: () {
                      setState(() {
                        Navigator.of(context).pop(this.listKondisiDipilih);
                      });

                    }
                ),
                SizedBox(height: 30,),
              ])),
    );
  }

  Widget _buildListView(Condition condition, int index) {
    return InkWell(child : Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Visibility(
              visible: this.listKondisiDipilih.where((element) => element.name == condition.name).isEmpty ? false : true,
              child:Icon(Icons.check_circle_rounded,color: Colors.blue,)
          ),
          Flexible(child:Container(
            height: 40,
            margin: EdgeInsets.only(left:5,top: 0,right: 5),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(10),
            child: Text(condition?.name, style: TextStyle(color: Colors.black),),
            decoration: BoxDecoration(
                color:  Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border:
                Border.all(color: Colors.grey.withOpacity(0.5))),
              ),
          ),
           ],
        ),
    ),
      onTap: (){
          setSelectedIndex(index,condition);
      },
    );
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListCondition(ResponseConditionByOutlet responseConditionByOutlet) {
    // TODO: implement onSuccessListServices
    setState(() {
      this.listCondition = responseConditionByOutlet.data;
    });
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      this._loginResponse = response;
      var bytesUsername = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _outletPresenter.doGetListConditionByOutletId(digestUsername.toString(),
          response.data.accessSecret, response.data.outletId.toString());
    });
  }

  @override
  void onSuccessListServices(ResponseServicesByOutlet listServicesResponse) {
    // TODO: implement onSuccessListServices
  }

  @override
  void onSuccessListItem(ResponseItemByOutlet responseItemByOutlet) {
    // TODO: implement onSuccessListItem
  }
}
