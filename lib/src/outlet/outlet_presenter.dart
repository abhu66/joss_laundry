import 'package:flutter/material.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/outlet_api.dart';
import 'package:joss_laundry/src/outlet/response_condition_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_items_by_outlet.dart';
import 'package:joss_laundry/src/outlet/response_services_by_outlet.dart';
import 'package:joss_laundry/src/pelanggan/list_pelanggan_response.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_api.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs_presenters.dart';

abstract class OutletContract{
  void onLoading(bool isLoading);
  void onSuccessListServices(ResponseServicesByOutlet listServicesResponse);
  void onSuccessListCondition(ResponseConditionByOutlet responseConditionByOutlet);
  void onSuccessListItem(ResponseItemByOutlet responseItemByOutlet);
  void onSuccessSharedPref(LoginResponse response);
  void onFailure(String error);
}

class OutletPresenter{
  OutletApi api = OutletApi();
  SharedPref sharedPref = SharedPref();
  OutletContract contract;
  OutletPresenter(this.contract);

  loadSharedPrefs() async{
    contract.onLoading(true);
    LoginResponse data = await sharedPref.getDataLogin();
    if(data != null){
      contract.onLoading(false);
      contract.onSuccessSharedPref(data);
    }
    else {
      contract.onLoading(false);
      contract.onFailure("Terjadi kesalahan !");
    }
  }

  doGetListServicesByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListServiceByOutletId(key: key,secretKey: secretKey,outletId: outletId).then((ResponseServicesByOutlet responseServicesByOutlet){
      contract.onLoading(false);
      contract.onSuccessListServices(responseServicesByOutlet);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetListConditionByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListConditionByOutletId(key: key,secretKey: secretKey,outletId: outletId).then((ResponseConditionByOutlet responseConditionByOutlet){
      contract.onLoading(false);
      contract.onSuccessListCondition(responseConditionByOutlet);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }

  doGetListItemByOutletId(String key,String secretKey,dynamic outletId) async {
    contract.onLoading(true);
    api.getListItemByOutletId(key: key,secretKey: secretKey,outletId: outletId).then((ResponseItemByOutlet responseItemByOutlet){
      contract.onLoading(false);
      contract.onSuccessListItem(responseItemByOutlet);
    }).catchError((error){
      contract.onLoading(false);
      contract.onFailure(error.toString());
    });
  }
}