// To parse this JSON data, do
//
//     final responseServicesByOutlet = responseServicesByOutletFromJson(jsonString);

import 'dart:convert';

ResponseServicesByOutlet responseServicesByOutletFromJson(String str) => ResponseServicesByOutlet.fromJson(json.decode(str));

String responseServicesByOutletToJson(ResponseServicesByOutlet data) => json.encode(data.toJson());

class ResponseServicesByOutlet {
  ResponseServicesByOutlet({
    this.status,
    this.message,
    this.count,
    this.data,
  });

  String status;
  String message;
  int count;
  List<Layanan> data;

  factory ResponseServicesByOutlet.fromJson(Map<String, dynamic> json) => ResponseServicesByOutlet(
    status: json["status"],
    message: json["message"],
    count: json["count"],
    data: List<Layanan>.from(json["data"].map((x) => Layanan.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "count": count,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Layanan {
  Layanan({
    this.id,
    this.item,
    this.name,
    this.unit,
    this.price,
    this.duration,
    this.priceExpress,
  });

  String id;
  dynamic item;
  String name;
  String unit;
  String price;
  String duration;
  String priceExpress;

  factory Layanan.fromJson(Map<String, dynamic> json) => Layanan(
    id: json["id"],
    item: json["item"],
    name: json["name"],
    unit: json["unit"],
    price: json["price"],
    duration: json["duration"],
    priceExpress: json["price_express"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "item": item,
    "name": name,
    "unit": unit,
    "price": price,
    "duration": duration,
    "price_express": priceExpress,
  };
}

class Item {
  Item({
    this.id,
    this.name,
  });

  String id;
  String name;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
