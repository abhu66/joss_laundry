import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:sizer/sizer.dart';

class PengaturanPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<StatefulWidget> {
  final GlobalKey<FormState> _pengaturan = GlobalKey<FormState>();
  bool isLoading = false;
  bool _switchVal1 = true;
  bool _switchVal2 = true;
  bool _switchVal3 = true;
  bool _switchVal4 = true;
  bool _switchVal5 = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Pengaturan",
            style: TextStyle(color: Colors.black, fontSize: 14.0.sp),
          ),
          backgroundColor: Colors.white,
          elevation: 0.6,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      color: Colors.white,
                      boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
                  margin: EdgeInsets.all(10.0),
                  padding: EdgeInsets.all(10),
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Text("Konfigurasi Printer", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Ukuran Printer",style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("80 mm", style: TextStyle(fontSize: 8.0.sp)),
                      ),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Gunakan Barcode",style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("nota akan mencetak barcode menggantikan QRCode",style: TextStyle(fontSize: 8.0.sp)),
                        trailing: Wrap(children: [
                            Switch(value: this._switchVal1,onChanged: (bool value) {
                                  setState((){
                                    this._switchVal1 = value;
                                    if(value == true){
                                      SharedPref.setGunakanBarcode("barcode", true);
                                    }
                                    else {
                                      SharedPref.setGunakanBarcode("barcode", false);
                                    }
                                  });
                                })
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Mode Teks", style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("cetak tanpa logo dan QRCode",style: TextStyle(fontSize: 8.0.sp)),
                        trailing: Wrap(children: [
                            Switch(value: this._switchVal2,onChanged: (bool value) {
                                  setState(() {
                                    this._switchVal2 = value;
                                    if(value == true){
                                      SharedPref.setModeTeks("text", true);
                                    }
                                    else {
                                      SharedPref.setModeTeks("text", false);
                                    }
                                  });
                                })
                          ],
                        ),
                      ),
                     ],
                  )
              ),
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      color: Colors.white,
                      boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
                  margin: EdgeInsets.all(10.0),
                  padding: EdgeInsets.all(10),
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Text("Konfigurasi Nota", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12.0.sp),),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Sembunyikan Alamat",style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("cetak tanpa informasi alamat",style: TextStyle(fontSize: 8.0.sp)),
                        trailing: Wrap(children: [
                          Switch(value: this._switchVal3,onChanged: (bool value) {
                            setState(() {
                              this._switchVal3 = value;
                              if(value){
                                SharedPref.saveBool("hideAddress", true);
                              }
                              else {
                                SharedPref.saveBool("hideAddress", false);
                              }
                            });
                          }),
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Sembunyikan Tanggal Selesai",style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("cetak tanpa tanggal selesai",style: TextStyle(fontSize: 8.0.sp)),
                        trailing: Wrap(children: [
                          Switch(value: this._switchVal4,onChanged: (bool value) {
                            setState(() {
                              this._switchVal4 = value;
                              if(value){
                                SharedPref.saveBool("hideTanggalSelesai", true);
                              }
                              else {
                                SharedPref.saveBool("hideTanggalSelesai", false);
                              }
                            });
                          })
                        ],
                        ),
                      ),
                      Divider(color: Colors.grey),
                      ListTile(
                        title: Text("Sembunyikan Pelanggan", style: TextStyle(fontSize: 10.0.sp)),
                        subtitle: Text("cetak tanpa informasi pelanggan",style: TextStyle(fontSize: 8.0.sp)),
                        trailing: Wrap(children: [
                          Switch(value: this._switchVal5,onChanged: (bool value) {
                            setState(() {
                              this._switchVal5 = value;
                              if(value){
                                SharedPref.saveBool("hidePelanggan", true);
                              }
                              else {
                                SharedPref.saveBool("hidePelanggan", false);
                              }
                            });
                          })
                        ],
                        ),
                      ),
                    ],
                  )
              ),
              Container(
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                width: MediaQuery.of(context).size.width/1.2,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff6bceff),
                        Color(0xFF00abff),
                      ],
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(50)
                    )
                ),
                child: Center(
                  child: this.isLoading ? CircularProgressIndicator(strokeWidth: 1,) :Text('Simpan'.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp
                    ),
                  ),
                ),
              ),
              SizedBox(height: 50.0,),
              Text("V.1.0.2",style: TextStyle(fontSize: 10.0.sp)),
              SizedBox(height: 50.0,),
            ],
          ),
        ),
    );
  }
}
