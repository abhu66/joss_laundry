import 'package:flutter/material.dart';

showAalert(String title,String message,BuildContext context){
  return  // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext contexts) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            FlatButton(onPressed: (){
              Navigator.pop(contexts);
            }, child: Text("Tutup"),
            ),
          ],
        );
      },
    );
}

showAalertSuccess(String title,String message,BuildContext context){
  return  // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext contexts) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            FlatButton(onPressed: (){
              Navigator.pop(contexts);
              Navigator.pop(contexts);
            }, child: Text("Tutup"),
            ),
          ],
        );
      },
    );
}