import 'package:dio/dio.dart';
import 'dart:developer';

class Config{
  final baseUrl       = "http://103.102.153.11:9091/";
  Dio dio             = new Dio();
  Response response;

  //post function with token
  Future<dynamic> doPostWithToken({String endpoint, String token, dynamic dataBody}) async {
    print("endpoint : " + endpoint);
    Map headers = <String, String>{
      'Content-type': 'application/json',
      'token': token,
    };
    var body = dataBody.toJson();
    print(body.toString());
    try {
      response = await dio.post(endpoint, data: body, options: Options(headers: headers));
      print(response.toString());
      if (response.data['status'] == "FAILED")
        return throw new Exception(response.data['message'].toString());
      // String path = "assets/dummy/"+catalogSoldOut;
      // Map<String, dynamic> data = await parseJsonFromAssets(path);
      return response.data;
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT) {
        return throw new Exception("Harap periksa koneksi internet anda !");
      }
      else if (e.type == DioErrorType.CONNECT_TIMEOUT) {
        return throw new Exception("Connection Time Out");
      }
      else if (e.type == DioErrorType.SEND_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RECEIVE_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RESPONSE) {
        if (e.error.toString().contains("[404]")) {
          return throw new Exception("Page Not Found !");
        }
        else if (e.error.toString().contains("[500]")) {
          print("Error kenapa  : " + e.toString());
          return throw new Exception(
              "Tidak dapat menghubungi server, Terjadi kesalahan pada server");
        }
        else {
          return throw new Exception("Mohon maaf sedang terjadi perbaikan");
        }
      }
      else {
        //print("error : "+e.error.toString());
        return throw new Exception(e.error);
      }
    }
  }

  //post function with token
    Future<dynamic> doPostWithoutToken({String endpoint,dynamic dataBody}) async {
      print("endpoint : " + endpoint);
      Map headers = <String, String>{
        'Content-type': 'application/json',
      };
      var body = dataBody.toJson();
      print(body.toString());
      try {
        response = await dio.post(endpoint, data: body, options: Options(headers: headers));
        print(response.toString());
        if (response.data['status'] == "FAILED")
          return throw new Exception(response.data['message'].toString());
        // String path = "assets/dummy/"+catalogSoldOut;
        // Map<String, dynamic> data = await parseJsonFromAssets(path);
        return response.data;
      } on DioError catch (e) {
        if (e.type == DioErrorType.DEFAULT) {
          return throw new Exception("Harap periksa koneksi internet anda !");
        }
        else if (e.type == DioErrorType.CONNECT_TIMEOUT) {
          return throw new Exception("Connection Time Out");
        }
        else if (e.type == DioErrorType.SEND_TIMEOUT) {
          return throw new Exception("Request Time Out");
        }
        else if (e.type == DioErrorType.RECEIVE_TIMEOUT) {
          return throw new Exception("Request Time Out");
        }
        else if (e.type == DioErrorType.RESPONSE) {
          if (e.error.toString().contains("[404]")) {
            return throw new Exception("Page Not Found !");
          }
          else if (e.error.toString().contains("[500]")) {
            print("Error kenapa  : " + e.toString());
            return throw new Exception(
                "Tidak dapat menghubungi server, Terjadi kesalahan pada server");
          }
          else {
            return throw new Exception("Mohon maaf sedang terjadi perbaikan");
          }
        }
        else {
          //print("error : "+e.error.toString());
          return throw new Exception(e.error);
        }
      }
    }


  //GET function with KEY AND SECRET KEY
  Future<dynamic> doGets({String endpoint,String key,String secretKey}) async {
    print("endpoint : " + endpoint);
    Map headers = <String, String>{
      'Content-type': 'application/json',
      'key':key,
      'secret':secretKey
    };

    try {
      response = await dio.get(endpoint, options: Options(headers: headers));
      log("List Response ${response.toString()}");
      if (response.data['status'] == "FAILED")
        return throw new Exception(response.data['message'].toString());
      // String path = "assets/dummy/"+catalogSoldOut;
      // Map<String, dynamic> data = await parseJsonFromAssets(path);
      return response.data;
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT) {
        return throw new Exception("Harap periksa koneksi internet anda !");
      }
      else if (e.type == DioErrorType.CONNECT_TIMEOUT) {
        return throw new Exception("Connection Time Out");
      }
      else if (e.type == DioErrorType.SEND_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RECEIVE_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RESPONSE) {
        if (e.error.toString().contains("[404]")) {
          return throw new Exception("Page Not Found !");
        }
        else if (e.error.toString().contains("[500]")) {
          print("Error kenapa  : " + e.toString());
          return throw new Exception(
              "Tidak dapat menghubungi server, Terjadi kesalahan pada server");
        }
        else {
          return throw new Exception("Mohon maaf sedang terjadi perbaikan");
        }
      }
      else {
        print("error : "+e.error.toString());
        return throw new Exception(e.error);
      }
    }
  }

  //GET function with KEY AND SECRET KEY
  Future<dynamic> doPosts({String endpoint,String key,String secretKey,dynamic dataBody}) async {
    print("endpoint : " + endpoint);
    Map headers = <String, String>{
      'Content-type': 'application/json',
      'key':key,
      'secret':secretKey
    };
    var body = dataBody.toJson();
    print("body "+body.toString());
    try {
      response = await dio.post(endpoint,data: body, options: Options(headers: headers));
      print("List Responsesss" +response.toString());
      if (response.data['status'] == "FAILED")
        return throw new Exception(response.data['message'].toString());
      // String path = "assets/dummy/"+catalogSoldOut;
      // Map<String, dynamic> data = await parseJsonFromAssets(path);
      return response.data;
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT) {
        return throw new Exception("Harap periksa koneksi internet anda !");
      }
      else if (e.type == DioErrorType.CONNECT_TIMEOUT) {
        return throw new Exception("Connection Time Out");
      }
      else if (e.type == DioErrorType.SEND_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RECEIVE_TIMEOUT) {
        return throw new Exception("Request Time Out");
      }
      else if (e.type == DioErrorType.RESPONSE) {
        if (e.error.toString().contains("[404]")) {
          return throw new Exception("Page Not Found !");
        }
        else if (e.error.toString().contains("[500]")) {
          print("Error kenapa  : " + e.toString());
          return throw new Exception(
              "Tidak dapat menghubungi server, Terjadi kesalahan pada server");
        }
        else {
          return throw new Exception("Mohon maaf sedang terjadi perbaikan");
        }
      }
      else {
        //print("error : "+e.error.toString());
        return throw new Exception(e.error);
      }
    }
  }
}