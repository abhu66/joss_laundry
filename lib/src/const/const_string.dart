class ConstString{
  static String CONST_WARNING_1 = "Pengambilan barang harus disertai nota";
  static String CONST_WARNING_2 = "Barang yang tidak diambil selama 1 bulan, hilang/rusak tidak diganti";
  static String CONST_WARNING_3 = "Barang hilang/rusak karena proses pengerjaan diganti maksimal 5x biaya";
  static String CONST_WARNING_4 = "Klaim luntur tidak dipisah diluar tanggungan";
  static String CONST_WARNING_5 = "Hak klaim berlaku 2 jam setelah barang diambil";
  static String CONST_WARNING_6 = "Setiap konsumen dianggap setuju dengan isi perhitungan tersebut diatas";

}