import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/main.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/finish/selesaikan_order_page.dart';
import 'package:joss_laundry/src/home/widget_home_page.dart';
import 'package:joss_laundry/src/kas/kas_page.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/outlet/info_outlet_page.dart';
import 'package:joss_laundry/src/pelanggan/pelanggan_page.dart';
import 'package:joss_laundry/src/pengaturan/pengaturan_page.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs_presenters.dart';
import 'package:joss_laundry/src/transaksi/history/data_transaksi_page.dart';
import 'package:joss_laundry/src/transaksi/transaksi_page.dart';
import 'package:joss_laundry/src/utils/color_utils.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';


class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}
class _State extends State<HomePage> implements SharedPrefContract{
  SharedPrefPresenter _sharedPrefPresenter;
  LoginResponse _loginResponse;
  List<MenuService> _listMenuService = [];
  bool isLoading = true;

  @override
  void initState(){
    super.initState();
    _sharedPrefPresenter = SharedPrefPresenter(this);
    _sharedPrefPresenter.loadDataLogin();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _listMenuService = [
      new MenuService(imageUrl: Icon(Icons.add_shopping_cart_outlined,color: HexColor("#3A4276"),size: 40,),title: "Transaksi"),
      new MenuService(imageUrl: Icon(Icons.assignment_turned_in_rounded,color: HexColor("#3A4276"),size: 40),title: "Selesaikan Laundry"),
      new MenuService(imageUrl: Icon(Icons.account_circle_rounded,color: HexColor("#3A4276"),size: 40),title: "Pelanggan"),
      new MenuService(imageUrl: Icon(Icons.list_outlined,color: HexColor("#3A4276"),size: 40),title: "Riwayat Transaksi"),
      new MenuService(imageUrl: Icon(Icons.attach_money_outlined,color: HexColor("#3A4276"),size: 40),title: "Kas"),
      new MenuService(imageUrl: Icon(Icons.settings,color: HexColor("#3A4276"),size: 40),title: "Pengaturan"),
      new MenuService(imageUrl: Icon(Icons.info,color: HexColor("#3A4276"),size: 40),title: "Info Outlet"),
      new MenuService(imageUrl: Icon(Icons.exit_to_app,color: HexColor("#3A4276"),size: 40),title: "Keluar"),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text("JOSS Laundry",style: TextStyle(color: Colors.black,fontSize: 14.0.sp),),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: ListView(
        padding:EdgeInsets.only(top: 0),
        shrinkWrap: true,
        children: [
          WidgetHomePage(),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.center,
            child: this.isLoading ? Text("") : Text(this._loginResponse.data.outlet.name,style: TextStyle(color: Colors.black,fontSize:12.0.sp),),
          ),
          Align(
            alignment: Alignment.center,
            child: this.isLoading ? Text("") :  Text(this._loginResponse.data.outlet.address,style: TextStyle(color: Colors.black,fontSize:12.0.sp),),
          ),
          SizedBox(height: 20,),
          Container(
            margin: EdgeInsets.all(10.0.sp),
            width: MediaQuery.of(context).size.width/1.2,
            height:SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45.0,
            padding: EdgeInsets.only(
                top: 4.0.sp,left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(50.0)
                ),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      blurRadius: 1
                  )
                ]
            ),
            child: TextField(
              readOnly: true,
              decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.search,
                  color: Color(0xff6bceff),
                ),
                hintText: 'Pengambilan / Penyelesaian Laundry',
              ),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => OrderFinishPage()),
                );
              },
            ),
          ),
          _buildGridView(),
          SizedBox(height: 20,),
        ],
      ),
    );
  }

  _buildGridView() {
    return  Padding(
      padding: const EdgeInsets.only(left: 25,right: 25),
      child: GridView.count(
          crossAxisCount: 4,
          childAspectRatio: 0.7,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
          //disable GridView's scrolling
          shrinkWrap: true,

          children: _listMenuService.map((menuService) {
            int index = _listMenuService.indexOf(menuService);
            return ListView(
                 shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    Container(
                      height: SizerUtil.deviceType == DeviceType.Tablet ?  200 : 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: HexColor(COLOR_F1F3F6),
                        //color:Colors.white
                      ),
                      child:InkWell(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: 10,),
                            Expanded(child : menuService.imageUrl),
                            Expanded(
                              flex: 1,
                              child : Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(0),
                                child : RichText(
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  text: TextSpan(
                                    text: menuService.title,
                                    style: TextStyle(
                                        color: HexColor("#3A4276"),
                                        fontSize: SizerUtil.deviceType == DeviceType.Tablet ? 25.0 : 10,
                                        //fontFamily: FONT_NUNITO,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        onTap: (){
                          switch(index){
                            case 0 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => TransaksiPage()),
                            ).then((value){
                              setState(() {
                                this.isLoading = true;
                              });
                            });break;
                            case 1 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => OrderFinishPage()),
                            );break;
                            case 2 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PelangganPage(isFromTransaction: false,)),
                            );break;
                            case 3 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => DataTransaksiPage()),
                            );break;
                            case 4 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => KasPage()),
                            );break;
                            case 5 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PengaturanPage()),
                            );break;
                            case 6 : Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => InfoOutletPage()),
                            );break;
                            case 7 :
                              SharedPref.remove("login");
                              Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => MyApp()),
                            );break;
                          }
                        },
                      ),
                    ),
                  ],
            );
          }).toList(),
      ),
    );
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
  }

  @override
  void onLoadingFcmToken(bool isLoading) {
    // TODO: implement onLoadingFcmToken
  }

  @override
  void onLoadingShared(bool isLoading) {
   setState(() {
     this.isLoading = isLoading;
   });
  }

  @override
  void onSuccessAuth(AuthResponse authResponse) {
    // TODO: implement onSuccessAuth
  }

  @override
  void onSuccessDataLogin(LoginResponse loginResponse) {
   setState(() {
     this._loginResponse =  loginResponse;
   });
  }

  @override
  void onSuccessFCMToken(String token) {
    // TODO: implement onSuccessFCMToken
  }
}
class MenuService{
  Icon imageUrl;
  String title;
  MenuService({this.imageUrl, this.title});
}