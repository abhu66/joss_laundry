import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/kas/kas_presenter.dart';
import 'package:joss_laundry/src/kas/response_body_cashout.dart';
import 'package:joss_laundry/src/kas/response_kas.dart';
import 'package:joss_laundry/src/kas/response_kas_jurnal.dart';
import 'package:joss_laundry/src/kas/response_kas_keluar.dart';
import 'package:joss_laundry/src/kas/response_kas_masuk.dart';
import 'package:joss_laundry/src/kas/response_list_cash_out_item.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/utils/color_utils.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class WidgetHomePage extends StatefulWidget {
  final bool isReload;
  WidgetHomePage({this.isReload});
  @override
  _State createState() => _State();
}

class _State extends State<WidgetHomePage> with SingleTickerProviderStateMixin implements KasContract{
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  LoginResponse _loginResponse;
  ResponseKasBalance responseKasBalance;
  bool isLoading = true;
  List<KasMasuk> listKasMasuk = [];
  KasPresenter _kasPresenter;

  @override
  void initState(){
    _kasPresenter = KasPresenter(this);
    _kasPresenter.loadSharedPrefs();

    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      return this.isLoading ?
      Container(
        height: 50,
          width: 50,
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 5,
            ),
          )
      ) :
      Container(
        padding: EdgeInsets.all(10.0.sp),
        margin: EdgeInsets.all(10.0.sp),
        width: MediaQuery.of(context).size.width,
        height: SizerUtil.deviceType == DeviceType.Tablet ? 180 : 100,
        decoration: BoxDecoration(
          color: HexColor(COLOR_F1F3F6),
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Center(
          child: ListTile(
            title: Text("Kas saat ini", style: TextStyle(fontSize: 12.0.sp)),
            subtitle: Text("Rp.${responseKasBalance?.data.toString()},-", style: TextStyle(fontSize: 12.0.sp),),
            trailing: Wrap(
              children: [
                Text("Perbarui data", style: TextStyle(fontSize: 12.0.sp)),
                InkWell(
                    child: Icon(Icons.wifi_protected_setup_outlined, size: 20.0.sp),
                  onTap: (){
                      setState(() {
                        _kasPresenter.loadSharedPrefs();
                      });
                  },
                )
              ],
            ),
          ),
        ),
      );
    }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
    setState(() {
      showAalert("Failed !", error.toString(), context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    // TODO: implement onLoading
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessKasBalanace(ResponseKasBalance responseKasBalance) {
    // TODO: implement onSuccessKasBalanace
    setState(() {
      this.responseKasBalance = responseKasBalance;
    });
  }

  @override
  void onSuccessListKasMasuk(ResponseListKasMasuk responseListKasMasuk) {
    // TODO: implement onSuccessListKasMasuk
  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    // TODO: implement onSuccessSharedPref
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername  = utf8.encode(response.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _kasPresenter.doGetKasBalanceByOutledIdAndDate(digestUsername.toString(), response.data.accessSecret, response.data.outletId.toString(), formattedDate.toString());
    });
  }

  @override
  void onSuccessListKeluar(ResponseKasKeluar responseKasKeluar) {
    // TODO: implement onSuccessListKeluar
  }

  @override
  void onSuccessSetorKas(ResponseBodyCashJournal responseBodyCashJournal) {
    // TODO: implement onSuccessSetorKas
  }

  @override
  void onSuccessListCashOutItem(ResponseListCashOutItem responseListCashOutItem) {
    // TODO: implement onSuccessListCashOutItem
  }

  @override
  void onSuccessCreateCashOut(ResponseBodyCashOut responseBodyCashOut) {
    // TODO: implement onSuccessCreateCashOut
  }
}
