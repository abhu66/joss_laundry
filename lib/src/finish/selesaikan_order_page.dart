import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/finish/selesaikan_order_search_page.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/transaksi/history/detail_data_transaksi_page.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class OrderFinishPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() =>  _State();
}

class _State extends State<OrderFinishPage>implements OrderContract {

  OrderPresenter _orderPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;
  List<CheckList> dataCheck = [];
  List<DataOrder> listTr = [];

  @override
  void initState() {
    super.initState();
    _orderPresenter = OrderPresenter(this);
    _orderPresenter.loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Data Transaksi",
          style: TextStyle(color: Colors.black, fontSize: 14.0.sp),),
        backgroundColor: Colors.white,
        elevation: 0.6,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => OrderFinishSearchPage()),
            ).then((_){
              _orderPresenter.loadSharedPrefs();
            });
          })
        ],
      ),
      body: this.isLoading ? Center(
          child: CircularProgressIndicator(strokeWidth: 1,)) : Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(child: ListView.builder(
                shrinkWrap: true,
                itemCount: this.listTr.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  DataOrder data = this.listTr[index];
                  return _buildListView(context, index, data);
                },
              ),),
            ),
            InkWell(
              onTap: (){
                List<int> listId = [];
                dataCheck.map((e){
                  setState(() {
                    if(e.value == true) listId.add(e.id);
                  });
                }).toList();
                if(listId.length == 0){
                  showAalert("Informasi !!!", "Setidaknya pilih satu transaksi untuk diselesaikan",context);
                    }
                else {
                  RequestBodyFinishOrder body = RequestBodyFinishOrder(
                    orderId: listId,
                  );
                  print("BODY : " + body.toJson().toString());

                  var bytesUsername = utf8.encode(
                      this._loginResponse.data.email); // data being hashed
                  var digestUsername = sha1.convert(bytesUsername);
                  _orderPresenter.doFinishOrder(digestUsername.toString(),
                      _loginResponse.data.accessSecret, body);
                }
              },
              child: Container(
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff6bceff),
                        Color(0xFF00abff),
                      ],
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(0)
                    )
                ),
                child: Center(
                  child: Text('Selesaikan Order'.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0.sp
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListView(BuildContext context, int index, DataOrder dataOrder) {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                  Radius.circular(10.0)
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 1
                )
              ]
          ),
          margin: EdgeInsets.all(10.0),
          padding: EdgeInsets.all(10),
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Text(dataOrder.orderNumber,
                  style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  value: dataCheck[index].value,
                  onChanged: (bool value) {
                    setState(() {
                      dataCheck[index].id = dataOrder.id;
                      dataCheck[index].value = value;
                    });
                  },
                ),
              ],),
              Divider(color: Colors.grey,),
              Text("${dataOrder.customer.name} - ${dataOrder.customer.phone} ",
                style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
              SizedBox(height: 10,),
              Text("Rp. ${dataOrder.total}",
                style: TextStyle(color: Colors.blue, fontSize: 10.0.sp),),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Diterima",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(
                    DateFormat("dd MMMM yyyy").format(dataOrder.orderDate),
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Selesai",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(
                    dataOrder.completionDate != null ?
                    DateFormat("dd MMMM yyyy").format(DateTime.parse(dataOrder.completionDate)) :
                    DateFormat("dd MMMM yyyy").format(DateTime.parse(dataOrder.estimationDate)),
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              SizedBox(height: 20,),
              Row(
                children: [
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: SizerUtil.deviceType == DeviceType.Tablet
                          ? 40
                          : 35,
                      width: SizerUtil.deviceType == DeviceType.Tablet
                          ? 140
                          : 100,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xff6bceff),
                              Color(0xFF00abff),
                            ],
                          ),
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          )
                      ),
                      child: Center(
                        child: Text(dataOrder.status.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 8.0.sp
                          ),
                        ),
                      ),
                    ),
                  ),
                ],)
            ],
          )
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailDataTransaksi(data:dataOrder)));
      },
    );
  }

  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Failed !", "message : " + error.replaceAll("Exception:", ""),
          context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi responseBodyListTransaksi) {
    setState(() {
      this.listTr = responseBodyListTransaksi.data;
      if(responseBodyListTransaksi.data.length != 0){
        this.listTr.map((e){
          dataCheck.add(CheckList(key: e.orderNumber,value: false));
        }).toList();
      }
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {

  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(
          DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername = utf8.encode(
          this._loginResponse.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _orderPresenter.doGetListTransactionUnpaidByOutletId(
          digestUsername.toString(), response.data.accessSecret,
          response.data.outletId.toString());
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
   setState(() {
     showAalert(responseBodyFinishOrder.status,"message : "+responseBodyFinishOrder.message.replaceAll("Exception:", ""),context);
     Future.delayed(Duration(seconds: 2)).then((_){
       Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));
     });
   });
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    // TODO: implement onsSuccessDetailOrder
  }
}

class CheckList{
  int id;
  String key;
  bool value;
  CheckList({this.key,this.value,this.id});
}