import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/finish/selesaikan_order_page.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/transaksi/history/detail_data_transaksi_page.dart';
import 'package:joss_laundry/src/transaksi/presenter/order_presenter.dart';
import 'package:joss_laundry/src/transaksi/response/request_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_detail_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_finish_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_list_order.dart';
import 'package:joss_laundry/src/transaksi/response/response_body_order.dart';
import 'package:sizer/sizer_util.dart';
import 'package:sizer/sizer.dart';

class OrderFinishSearchPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() =>  _State();
}
class _State extends State<OrderFinishSearchPage> implements OrderContract{

  TextEditingController controller = new TextEditingController();
  List<DataOrder> listTr = [];
  List<DataOrder> listSearch = [];

  OrderPresenter _orderPresenter;
  LoginResponse _loginResponse;
  bool isLoading = false;
  bool firstSearch = true;
  String query ="";
  List<CheckList> dataCheck = [];

  @override
  void initState() {
    _orderPresenter = OrderPresenter(this);
    _orderPresenter.loadSharedPrefs();
    super.initState();
    controller.addListener((){
      //menambahkan method yang akan dipanggil ketika object ada berubah
      if(controller.text.isEmpty){
        setState(() {
          firstSearch = true;
          query ="";
        });
      } else{
        //data tidak kosong
        setState(() {
          firstSearch = false;
          query = controller.text;
          print("Query : "+query);
        });
      }
    });
  }

  //membuat widget untuk hasil search
  Widget _performSearch(){
   this.listSearch = new List<DataOrder>();
    for(int i=0; i<listTr.length;i++){
      var item = listTr[i];

      if(item.customer.name.toLowerCase().contains(query.toLowerCase())){
        this.listSearch.add(item);
      }
    }
    return Expanded(
      child: SingleChildScrollView(child : ListView.builder(
        shrinkWrap: true,
        itemCount: this.listSearch.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          DataOrder data = this.listSearch[index];
          return _buildListView(context, index, data);
        },
      ),),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: 70,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){
                Navigator.pop(context);
              }),
              Flexible(child: Container(
                margin: EdgeInsets.all(10.0.sp),
                height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45.0,
                padding: EdgeInsets.only(
                    top: 4.0.sp, left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    color: Colors.white,
                    boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 1)]),
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.search,
                      color: Color(0xff6bceff),
                    ),
                    hintText: 'Cari order',
                  ),
                ),
              ),
              ),
            ],
          ),
          !firstSearch ?  _performSearch() : Expanded(
            child: SingleChildScrollView(child: ListView.builder(
              shrinkWrap: true,
              itemCount: listTr.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                DataOrder data = listTr[index];
                return _buildListView(context, index, data);
              },
            ),),
          ),
          InkWell(
            onTap: (){
              List<int> listId = [];
              dataCheck.map((e){
                setState(() {
                  if(e.value == true) listId.add(e.id);
                });
              }).toList();
              RequestBodyFinishOrder body = RequestBodyFinishOrder(
                orderId: listId,
              );
              print("BODY : "+body.toJson().toString());

              var bytesUsername = utf8.encode(this._loginResponse.data.email); // data being hashed
              var digestUsername = sha1.convert(bytesUsername);
              _orderPresenter.doFinishOrder(digestUsername.toString(), _loginResponse.data.accessSecret,body);

            },
            child: Container(
              height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xff6bceff),
                      Color(0xFF00abff),
                    ],
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(0)
                  )
              ),
              child: Center(
                child: Text('Selesaikan Order'.toUpperCase(),
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0.sp
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView(BuildContext context, int index, DataOrder dataOrder) {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                  Radius.circular(10.0)
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 1
                )
              ]
          ),
          margin: EdgeInsets.all(10.0),
          padding: EdgeInsets.all(10),
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Text(dataOrder.orderNumber,
                  style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                Checkbox(
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  value: dataCheck[index].value,
                  onChanged: (bool value) {
                    setState(() {
                      dataCheck[index].id    = dataOrder.id;
                      dataCheck[index].value = value;
                    });
                  },
                ),
              ],),
              Divider(color: Colors.grey,),
              Text("${dataOrder.customer.name} - ${dataOrder.customer.phone} ",
                style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
              SizedBox(height: 10,),
              Text("Rp. ${dataOrder.total}",
                style: TextStyle(color: Colors.blue, fontSize: 10.0.sp),),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Diterima",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(
                    dataOrder.orderDate.toLocal().toString().substring(0, 16),
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Selesai",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                  Text(
                    dataOrder.completionDate != null ? dataOrder.completionDate
                        .toLocal().toString().substring(0, 16) : "-",
                    style: TextStyle(color: Colors.black, fontSize: 10.0.sp),),
                ],),
              SizedBox(height: 20,),
              Row(
                children: [
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: SizerUtil.deviceType == DeviceType.Tablet
                          ? 40
                          : 35,
                      width: SizerUtil.deviceType == DeviceType.Tablet
                          ? 140
                          : 100,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xff6bceff),
                              Color(0xFF00abff),
                            ],
                          ),
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          )
                      ),
                      child: Center(
                        child: Text(dataOrder.status.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 8.0.sp
                          ),
                        ),
                      ),
                    ),
                  ),
                ],)
            ],
          )
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailDataTransaksi(data:dataOrder)));
      },
    );
  }


  @override
  void onFailure(String error) {
    setState(() {
      showAalert("Failed !", "message : " + error.replaceAll("Exception:", ""),
          context);
    });
  }

  @override
  void onLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessListOrder(ResponseBodyListTransaksi responseBodyListTransaksi) {
    setState(() {
      if(responseBodyListTransaksi.data.length != 0){
        responseBodyListTransaksi.data.map((e){
          if(e.status != "Done"){
            this.listTr.add(e);
            dataCheck.add(CheckList(key: e.orderNumber,value: false));
          }

        }).toList();
      }
    });
  }

  @override
  void onSuccessOrder(ResponseBodyOrder responseBodyOrder) {

  }

  @override
  void onSuccessSharedPref(LoginResponse response) {
    setState(() {
      String formattedDate = DateFormat('yyyy-MM-dd').format(
          DateTime.now().toLocal());
      this._loginResponse = response;
      var bytesUsername = utf8.encode(
          this._loginResponse.data.email); // data being hashed
      var digestUsername = sha1.convert(bytesUsername);
      _orderPresenter.doGetListTransactionByOutletId(
          digestUsername.toString(), response.data.accessSecret,
          response.data.outletId.toString());
    });
  }

  @override
  void onSuccessFinishOrder(ResponseBodyFinishOrder responseBodyFinishOrder) {
    setState(() {
      showAalert(responseBodyFinishOrder.status,"message : "+responseBodyFinishOrder.message.replaceAll("Exception:", ""),context);
      Future.delayed(Duration(seconds: 2)).then((_){
        Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));
      });
    });
  }

  @override
  void onsSuccessDetailOrder(ResponseBodyDetailOrder responseBodyDetailOrder) {
    // TODO: implement onsSuccessDetailOrder
  }

  @override
  void onSuccessOrderUnpaid(ResponseBodyFinishOrder responseBodyFinishOrder) {
    // TODO: implement onSuccessOrderUnpaid
  }
}