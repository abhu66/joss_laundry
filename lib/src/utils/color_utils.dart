import 'package:flutter/cupertino.dart';

//class for convert hex color
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    int colors;
    hexColor = hexColor.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      colors = int.parse("0x$hexColor");
    }
    return colors;
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

//const color
const COLOR_FFFFFF = "#FFFFFF";
const COLOR_E5E5E5 = "#E5E5E5";
const COLOR_1B1D28 = "#1B1D28";
const COLOR_7B7F9E = "#7B7F9E";
const COLOR_CCCCCC = "#CCCCCC";
const COLOR_333333 = "#333333";
const COLOR_212330 = "#212330";
const COLOR_FFAC30 = "#FFAC30";
const COLOR_F1F3F6 = "#F1F3F6";
const COLOR_3A4276 = "#3A4276";