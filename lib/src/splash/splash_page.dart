import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/main.dart';
import 'package:joss_laundry/src/auth/auth_presenters.dart';
import 'package:joss_laundry/src/auth/auth_request_body.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/const/const_alert.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_page.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs_presenters.dart';

class SplashPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}
class _State extends State<SplashPage> implements AuthContract,SharedPrefContract{
  bool isLoading = true;
  AuthPresenter       _authPresenter;
  SharedPrefPresenter _sharedPrefPresenter;

  @override
  void initState(){
    super.initState();
    _authPresenter       = AuthPresenter(this);
    _sharedPrefPresenter = SharedPrefPresenter(this);
    startSplashScreen();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: Center(
          child: this.isLoading ? CircularProgressIndicator(
            strokeWidth: 1,
          ): Text(""),
        ),
      ),
    );
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration,() async {
      await SharedPref().getDataLogin().then((LoginResponse loginResponse) {
        if(loginResponse != null){
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_){
              return HomePage();
            }),
          );
        }
        else {
          _sharedPrefPresenter.loadDataLogin();
        }
      }).catchError((error){
        showAalert("Failed", error.toString(), context);
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_){
            return MyApp();
          }),
        );
      });
    });
  }



  @override
  void onFailure(String error) {
    // TODO: implement onFailure
  }

  @override
  void onFailureAuth(String error) {
    // TODO: implement onFailureAuth
  }

  @override
  void onLoadingFcmToken(bool isLoading) {
    // TODO: implement onLoadingFcmToken
  }

  @override
  void onLoadingShared(bool isLoading) {
     setState(() {
       this.isLoading = isLoading;
     });
  }


  @override
  void onSuccessAuth(AuthResponse authResponse) {
    setState(() {
      SharedPref.save("auth", authResponse);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    });
  }

  @override
  void onSuccessDataLogin(LoginResponse loginResponse) {
    setState(() {
      if(loginResponse == null) {
        _authPresenter.doGetAuth(
            AuthRequestBody(appId: "F8VMMqvLcKPSRvMsLxf9ccAEphsZ5MsGhdg8KkPZ", appSecret: "xh0jEh46sz1jFHJAS4nd5zsS3Pqy4crSUAihvc3J")
        );
      }
      else {
        //ini sementara
        _authPresenter.doGetAuth(
            AuthRequestBody(appId: "F8VMMqvLcKPSRvMsLxf9ccAEphsZ5MsGhdg8KkPZ", appSecret: "xh0jEh46sz1jFHJAS4nd5zsS3Pqy4crSUAihvc3J")
        );
      }
    });
  }

  @override
  void onSuccessFCMToken(String token) {
    // TODO: implement onSuccessFCMToken
  }

  @override
  void onAuthLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }
}