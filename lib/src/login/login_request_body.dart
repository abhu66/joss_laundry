// To parse this JSON data, do
//
//     final loginRequestBody = loginRequestBodyFromJson(jsonString);

import 'dart:convert';

LoginRequestBody loginRequestBodyFromJson(String str) => LoginRequestBody.fromJson(json.decode(str));

String loginRequestBodyToJson(LoginRequestBody data) => json.encode(data.toJson());

class LoginRequestBody {
  LoginRequestBody({
    this.email,
    this.password,
  });

  String email;
  String password;

  factory LoginRequestBody.fromJson(Map<String, dynamic> json) => LoginRequestBody(
    email: json["email"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
  };
}
