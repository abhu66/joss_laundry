import 'package:joss_laundry/src/const/config.dart';
import 'package:joss_laundry/src/login/login_request_body.dart';
import 'package:joss_laundry/src/login/login_response.dart';

class LoginApi{
  final loginUrl = "user/login";
  Config config  = new Config();

  Future<LoginResponse> postLogin({LoginRequestBody loginBody,String token}) async {
    return LoginResponse.fromJson(await config.doPostWithToken(endpoint:config.baseUrl + loginUrl,dataBody: loginBody,token: token));
  }
}