import 'package:joss_laundry/src/login/login_api.dart';
import 'package:joss_laundry/src/login/login_request_body.dart';
import 'package:joss_laundry/src/login/login_response.dart';

abstract class LoginContract{
  void onLoginLoading(bool isLoading);
  void onSuccessLogin(LoginResponse loginResponse);
  void onFailureLogin(String error);
}
class LoginPresenter{
  LoginApi api = LoginApi();
  LoginContract contract;
  LoginPresenter(this.contract);

  doLogin(LoginRequestBody loginRequestBody,String token) async {
    contract.onLoginLoading(true);
    api.postLogin(loginBody: loginRequestBody,token: token).then((LoginResponse loginResponse){
      contract.onLoginLoading(false);
      contract.onSuccessLogin(loginResponse);
    }).catchError((error){
      contract.onLoginLoading(false);
      contract.onFailureLogin(error.toString());
    });
  }
}