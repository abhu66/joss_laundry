// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.organizationId,
    this.roleId,
    this.name,
    this.phone,
    this.email,
    this.pin,
    this.outletId,
    this.workshopId,
    this.accessSecret,
    this.organization,
    this.role,
    this.access,
    this.outlet,
  });

  int id;
  int organizationId;
  int roleId;
  String name;
  String phone;
  String email;
  dynamic pin;
  int outletId;
  dynamic workshopId;
  String accessSecret;
  String organization;
  String role;
  Access access;
  Outlet outlet;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    organizationId: json["organization_id"],
    roleId: json["role_id"],
    name: json["name"],
    phone: json["phone"],
    email: json["email"],
    pin: json["pin"],
    outletId: json["outlet_id"],
    workshopId: json["workshop_id"],
    accessSecret: json["access_secret"],
    organization: json["organization"],
    role: json["role"],
    access: Access.fromJson(json["access"]),
    outlet: Outlet.fromJson(json["outlet"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organization_id": organizationId,
    "role_id": roleId,
    "name": name,
    "phone": phone,
    "email": email,
    "pin": pin,
    "outlet_id": outletId,
    "workshop_id": workshopId,
    "access_secret": accessSecret,
    "organization": organization,
    "role": role,
    "access": access.toJson(),
    "outlet": outlet.toJson(),
  };
}

class Access {
  Access({
    this.outlet,
    this.workshop,
    this.backoffice,
  });

  List<Backoffice> outlet;
  List<Backoffice> workshop;
  List<Backoffice> backoffice;

  factory Access.fromJson(Map<String, dynamic> json) => Access(
    outlet: List<Backoffice>.from(json["outlet"].map((x) => Backoffice.fromJson(x))),
    workshop: List<Backoffice>.from(json["workshop"].map((x) => Backoffice.fromJson(x))),
    backoffice: List<Backoffice>.from(json["backoffice"].map((x) => Backoffice.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "outlet": List<dynamic>.from(outlet.map((x) => x.toJson())),
    "workshop": List<dynamic>.from(workshop.map((x) => x.toJson())),
    "backoffice": List<dynamic>.from(backoffice.map((x) => x.toJson())),
  };
}

class Backoffice {
  Backoffice({
    this.name,
    this.status,
  });

  String name;
  bool status;

  factory Backoffice.fromJson(Map<String, dynamic> json) => Backoffice(
    name: json["name"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "status": status,
  };
}

class Outlet {
  Outlet({
    this.id,
    this.name,
    this.phone,
    this.address,
    this.provinceId,
    this.cityId,
    this.image,
    this.province,
    this.city,
  });

  int id;
  String name;
  String phone;
  String address;
  int provinceId;
  int cityId;
  String image;
  String province;
  String city;

  factory Outlet.fromJson(Map<String, dynamic> json) => Outlet(
    id: json["id"],
    name: json["name"],
    phone: json["phone"],
    address: json["address"],
    provinceId: json["province_id"],
    cityId: json["city_id"],
    image: json["image"],
    province: json["province"],
    city: json["city"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "phone": phone,
    "address": address,
    "province_id": provinceId,
    "city_id": cityId,
    "image": image,
    "province": province,
    "city": city,
  };
}
