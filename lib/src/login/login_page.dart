import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/const/const_image.dart';
import 'package:joss_laundry/src/home/home_screen.dart';
import 'package:joss_laundry/src/login/login_presenter.dart';
import 'package:joss_laundry/src/login/login_request_body.dart';
import 'package:joss_laundry/src/login/login_response.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs.dart';
import 'package:joss_laundry/src/sharedprefs/shared_prefs_presenters.dart';
import 'package:sizer/sizer.dart';
import 'package:sizer/sizer_util.dart';

class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}
class _State extends State<LoginPage> implements LoginContract,SharedPrefContract{
  final GlobalKey<FormState>  _formLogin = GlobalKey<FormState>();
  bool isLoading = false;
  LoginPresenter      _loginPresenter;
  SharedPrefPresenter _sharedPrefPresenter;
  TextEditingController email    = TextEditingController();
  TextEditingController password = TextEditingController();
  AuthResponse _authResponse;

  @override
  void initState(){
    super.initState();
    _sharedPrefPresenter = SharedPrefPresenter(this);
    _loginPresenter      = LoginPresenter(this);
    _sharedPrefPresenter.loadAuth();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print(SizerUtil.deviceType);
    return Scaffold(
      body: Container(
        child: ListView(
          children: [
            Image(image: AssetImage(ConstImages.IC_JOSS_LAUNDRY)),
            Container(
                padding: EdgeInsets.all(10.0.sp),
                child : formLogin()),
          ],
        ),
      ),
    );
  }
  
  Widget formLogin(){
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5,),
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 100, top: 50.0,),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [BoxShadow(
          color: Colors.grey,
       //   spreadRadius: 5,
          blurRadius: 5,
          // offset: Offset(0, 3),
        )]
      ),
      child: Form(
            key: _formLogin,
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width/1.2,
                  height:SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45.0,
                  padding: EdgeInsets.only(
                      top: 4.0.sp,left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(10.0)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 5
                        )
                      ]
                  ),
                  child: TextFormField(
                    controller: email,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(Icons.person,
                        color: Color(0xff6bceff),
                      ),
                      hintText: 'Email',
                    ),
                  ),
                ),

                Container(
                  width: MediaQuery.of(context).size.width/1.2,
                  height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                  margin: EdgeInsets.only(top: 32),
                  padding: EdgeInsets.only(
                      top: 4.0.sp,left: 16.0.sp, right: 16.0.sp, bottom: 4.0.sp
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(10.0)
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 5
                        )
                      ]
                  ),
                  child: TextFormField(
                    obscureText: true,
                    controller: password,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(Icons.vpn_key,
                        color: Color(0xff6bceff),
                      ),
                      hintText: 'Password',
                    ),
                  ),
                ),
                SizedBox(height: 50),
                InkWell(
                  onTap: this.isLoading ? null : (){
                    if(email.text.length == 0 && password.text.length == 0){
                    showAalert("Perhatian !","Email dan Password tidak boleh kosong !");
                    }
                    else if(email.text.length == 0){
                      showAalert("Perhatian !","Email tidak boleh kosong !");
                    }
                    else if(password.text.length == 0){
                      showAalert("Perhatian !","Password tidak boleh kosong !");
                    }
                    else {
                      var bytes = utf8.encode(password.text); // data being hashed
                      var digest = sha1.convert(bytes);

                      //encrypt username
                      var bytesUsername = utf8.encode(email.text); // data being hashed
                      var digestUsername = sha1.convert(bytes);
                      print("Digest username : "+digestUsername.toString());
                      _loginPresenter.doLogin(LoginRequestBody(email:email.text, password: digest.toString()), this._authResponse.data.token.toString());
                    }

                    // Navigator.pushReplacement(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => HomePage()),
                    // );
                  },
                  child: Container(
                    height: SizerUtil.deviceType == DeviceType.Tablet ? 70 : 45,
                    width: MediaQuery.of(context).size.width/1.2,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xff6bceff),
                            Color(0xFF00abff),
                          ],
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(50)
                        )
                    ),
                    child: Center(
                      child: this.isLoading ? CircularProgressIndicator(strokeWidth: 1,) :Text('Login'.toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          fontSize: 12.0.sp
                        ),
                      ),
                    ),
                  ),
                ),
              ],
          ),
      ),
    );
  }

  showAalert(String title,String message){
    return  // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: [
              FlatButton(onPressed: (){
                Navigator.pop(context);
              }, child: Text("Tutup"),
              ),
            ],
          );
        },
      );
  }

  @override
  void onFailureLogin(String error) {
    setState(() {
      showAalert("Login Gagal","message : "+error.replaceAll("Exception:", ""));
    });
  }

  @override
  void onLoginLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessLogin(LoginResponse loginResponse) {
    setState(() {
     SharedPref.save("login", loginResponse);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    });
  }

  @override
  void onFailure(String error) {
    // TODO: implement onFailure
  }

  @override
  void onLoadingFcmToken(bool isLoading) {
    // TODO: implement onLoadingFcmToken
  }

  @override
  void onLoadingShared(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void onSuccessAuth(AuthResponse authResponse) {
    setState(() {
      _authResponse = authResponse;
    });
  }

  @override
  void onSuccessDataLogin(LoginResponse loginResponse) {
    // TODO: implement onSuccessDataLogin
  }

  @override
  void onSuccessFCMToken(String token) {
    // TODO: implement onSuccessFCMToken
  }
}