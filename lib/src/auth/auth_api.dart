import 'package:joss_laundry/src/auth/auth_request_body.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/const/config.dart';

class AuthApi{
  final authUrl = "authenticate";
  Config config  = new Config();

  Future<AuthResponse> postAuth({AuthRequestBody authRequestBody}) async {
    return AuthResponse.fromJson(await config.doPostWithoutToken(endpoint:config.baseUrl + authUrl,dataBody: authRequestBody));
  }
}