// To parse this JSON data, do
//
//     final authResponse = authResponseFromJson(jsonString);

import 'dart:convert';

AuthResponse authResponseFromJson(String str) => AuthResponse.fromJson(json.decode(str));

String authResponseToJson(AuthResponse data) => json.encode(data.toJson());

class AuthResponse {
  AuthResponse({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory AuthResponse.fromJson(Map<String, dynamic> json) => AuthResponse(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.token,
    this.expireTime,
  });

  String token;
  DateTime expireTime;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    token: json["token"],
    expireTime: DateTime.parse(json["expire_time"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token,
    "expire_time": expireTime.toIso8601String(),
  };
}
