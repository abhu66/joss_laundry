// To parse this JSON data, do
//
//     final authRequestBody = authRequestBodyFromJson(jsonString);

import 'dart:convert';

AuthRequestBody authRequestBodyFromJson(String str) => AuthRequestBody.fromJson(json.decode(str));

String authRequestBodyToJson(AuthRequestBody data) => json.encode(data.toJson());

class AuthRequestBody {
  AuthRequestBody({
    this.appId,
    this.appSecret,
  });

  String appId;
  String appSecret;

  factory AuthRequestBody.fromJson(Map<String, dynamic> json) => AuthRequestBody(
    appId: json["app_id"],
    appSecret: json["app_secret"],
  );

  Map<String, dynamic> toJson() => {
    "app_id": appId,
    "app_secret": appSecret,
  };
}
