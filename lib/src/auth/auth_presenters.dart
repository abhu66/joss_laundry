import 'package:joss_laundry/src/auth/auth_api.dart';
import 'package:joss_laundry/src/auth/auth_request_body.dart';
import 'package:joss_laundry/src/auth/auth_response.dart';
import 'package:joss_laundry/src/login/login_api.dart';
import 'package:joss_laundry/src/login/login_request_body.dart';
import 'package:joss_laundry/src/login/login_response.dart';

abstract class AuthContract{
  void onAuthLoading(bool isLoading);
  void onSuccessAuth(AuthResponse authResponse);
  void onFailureAuth(String error);
}

class AuthPresenter{
  AuthApi api = AuthApi();
  AuthContract contract;
  AuthPresenter(this.contract);

  doGetAuth(AuthRequestBody authRequestBody) async {
    contract.onAuthLoading(true);
    api.postAuth(authRequestBody: authRequestBody).then((AuthResponse authResponse){
      contract.onAuthLoading(false);
      contract.onSuccessAuth(authResponse);
    }).catchError((error){
      contract.onAuthLoading(false);
      contract.onFailureAuth(error.toString());
    });
  }
}